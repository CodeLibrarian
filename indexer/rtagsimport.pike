#define TDIR  "/var/cvs-doc/tags"
string rtag_file_name( string directory )
{
    directory = replace(directory, "/", "_slash_");
    if( !strlen( directory ) )
	directory = "__empty__";
    return sprintf( "%s/rev/%s/%s", TDIR, directory[0..2], directory );
}

void save_tag_file( string d, mapping rt )
{
  werror( d+"\n");
    mkdir( dirname( rtag_file_name( d ) ) );
    Stdio.write_file( rtag_file_name( d ), encode_value( rt ) );
}

mapping get_rtag_file( string d )
{
    mapping t = ([]);
    catch( t = decode_value( Stdio.read_file( rtag_file_name(d) ) ) );
    return t;
}

void main( int argc, array argv )
{
    string otag;
    string tag, file, rev;
    mapping rt;
    int cc;
    foreach( Stdio.stdin.line_iterator(); int l; string x )
    {
	if( sscanf( x, "%s\0%s\0%s", tag, file, rev ) != 3 )
	    continue;
//	werror(tag+" "+file+" "+rev+"\n");
	if( tag != otag )
	{
	    if( otag && cc )
	    {
		save_tag_file( otag, rt );
		cc=0;
	    }
	    otag = tag;
	    rt = get_rtag_file( tag );
 	    werror("%O\n", tag );
	}
	if( rt[file] != rev )
	{
	  // 	    werror( "    "+file+" -> "+rev+"\n");
	    rt[file]=rev;
	    cc++;
	}
    }
    if( otag && cc )
	save_tag_file( otag, rt );
}
