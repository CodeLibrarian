#! /usr/bin/env pike
// $Id: indexer.pike,v 1.21 2009/03/25 16:23:16 zino Exp $
constant version = ("$Revision: 1.21 $"/" ")[1];

int sync_db = 0; // when set to 1, all files and revisions in the db that don't
		 // exist in the repository will be purged (reported on stderr)
int index_tags = 0; // when set to 1, tags are extracted and stored in the db.
int max_tag_time = 10;
int max_checkin_time = 30;
float poll_frequency = 10.0;
int verbosity = 0; // interactive
string mailhost, cvshost, mountpoint;
array(string) my_hostnames = ({});

mapping have_git_revision = ([]);

// int barwidth; - declared in compat.pike
#include "compat.pike"

//! Database with repository config and other librarian tables

class DBWRAPPER(Sql.Sql rdb)
{
    object master_sql;
    Thread.Mutex mt = Thread.Mutex();
    mixed big_query( mixed ... args )
    {
	if( !master_sql )
	    master_sql = rdb->master_sql;
	
	System.Timer t = System.Timer();
	object key = mt->lock();
	mixed res= rdb->big_query( @args );
	if( t->peek() > 0.2 )
	{
	    werror(" Query %s took %.1f seconds\n", args[0],t->get());
	}
	return res;
    }
    mixed query( mixed ... args )
    {
	if( !master_sql )
	    master_sql = rdb->master_sql;
	System.Timer t = System.Timer();
	object key = mt->lock();
	mixed res= rdb->query( @args );
	if( t->peek() > 1.0 )
	{
	    werror(" Query %s took %.1f seconds\n", args[0],t->get());
	}
	return res;
    }
    string quote( string what ) { return rdb->quote( what ); }
}

object db = DBWRAPPER(Sql.Sql(getenv("DBURL")));

#define FAIL(X, Y ...) do { werror(X + "\n", Y); exit(1); } while(0)
#define ASSERT_DIR(X) if(Stdio.Stat stat = file_stat(X))\
  {\
    if(!stat->isdir)\
      FAIL("%s is not a directory.", X);\
  }  \
  else\
    FAIL("Directory %s does not exist.", X)

#ifdef CVSVIEW_DEBUG
#define DEBUG(X, Y ...) werror("Importer: " + X, Y)
#else
#define DEBUG(X, Y ...)
#endif


#ifdef CVSVIEW_NOISE
#define NOISE(X, Y ...) werror("Importer: " + X, Y)
#else
#define NOISE(X, Y ...)
#endif


constant binary = (<
    "marm", "ref",  "rss",  "mmp",  "a",    "bak",  "bin",  "bld",  "bmp",
    "com",  "dat",  "dia",  "dj2",  "dlg",  "dll",  "doc",  "dsp",  "dsw",
    "exe",  "gif",  "gz",   "ico",  "jar",  "jpg",  "l",    "la",   "lib",
    "o",    "obj",  "png",  "tga",  "zip",  "mcp",  "rls",  "ra",
>);

int seen_dirs, seen_files, parsed_files;

mapping(string:int) users;

//! Returns a canonic representation of @[path] (without /./, /../, //
//! and similar path segments, trailing / removed).
string canonicize_path(string path)
{
  if(has_prefix(path, "/"))
    path = combine_path("/", path);
  else
    path = combine_path("/", path)[1..];
  if(has_suffix(path, "/"))
    return path[..sizeof(path)-2];
  return path;
}

string usage=#"index.pike v%s - gathers repository data for code librarian

This indexer is capable of indexing your repositories in two ways: in full
or incrementally. A full indexing will walk all directories, processing all of
their files as it moves along. The incremental indexer requires that you install
commitinfo and taginfo hooks that notify the indexer of changes in the
repository as they happen. The least resource intensive indexing approach is
running the indexer incrementally, but running the full indexer regularly works
too.

The list of repositories to index is stored in a database, and entries can be
added using the options -r, -p and (optionally) -n. When running incrementally,
the only mandatory parameter is -l.

Start the incremental indexing daemon:
  -l, --logdir=STRING           The directory the indexer should monitor
     [--poll-frequency=INT]     How many seconds to wait between logdir scans
     [--allow-tag-latency=INT]  Estimated max time of a tag operation
     [--allow-ci-latency=INT]   Estimated max time of a commit operation
     [--hostname=STRING]        Index repositories with cvsroot at this host

Add or modify one repository to index:
  -r, --cvsroot=STRING          The cvsroot directory of the repository
  -p, --repository-path=STRING  The path to the repository within cvsroot
  -g, --git-dir=STRING          The .git directory of the repository
 [-n, --repository-name=STRING] Give the repository a human-readable name
 [-u, --repository-uri=STRING]  Repository name in the server's uri namespace
 [-m, --mailhost=STRING]        Mailhost shown for commits to this repository
 [--index-mode=trigger|sweep]   How the repository is indexed (default: sweep)

Additional options:
  --with-tags (--without-tags)  Whether to index tags or not (default: not)
  --sync-with-db                Purge all manually moved/dropped files/revisions
  -q, --quiet                   Show neither progress nor end statistics
  -h, --help                    Display usage information and exit
  -V, --version                 Display version and exit\n";

int main(int argc, array(string) argv, array(string) env)
{
  string logdir, cvsscheme, cvsroot, path, name, index_mode;
  int add_repository, t = gethrtime();

  catch(have_git_revision = decode_value( Stdio.read_file( "/tmp/have_git_revision") ));
  catch(rep_refs_mtimes = decode_value( Stdio.read_file( "/tmp/rep_refs_mtimes") ));
  catch(timestamps = decode_value( Stdio.read_file( "/tmp/rep_refs_timestamps") ));
  new_timestamps = copy_value( timestamps );


  foreach(Getopt.find_all_options(argv, ({
    ({ "logdir",     Getopt.HAS_ARG,      "-l:--logdir:--log-dir"/":" }),
    ({ "sync-db",    Getopt.NO_ARG,       "--sync-with-db"/":" }),
    ({ "no-tags",    Getopt.NO_ARG,       "--without-tags:--ignore-tags"/":" }),
    ({ "with-tags",  Getopt.NO_ARG,       "--with-tags:--index-tags"/":" }),
    ({ "poll-wait",  Getopt.HAS_ARG,      ({ "--poll-frequency" }) }),
    ({ "tag-wait",   Getopt.HAS_ARG,      ({ "--allow-tag-latency" }) }),
    ({ "ci-wait",    Getopt.HAS_ARG,      ({ "--allow-ci-latency" }) }),
    ({ "hostname",   Getopt.HAS_ARG,      ({ "--hostname" }) }),
    ({ "cvsroot",    Getopt.HAS_ARG,      "-r:--cvsroot:--cvs-root"/":" }),
    ({ "git-dir",    Getopt.HAS_ARG,      "-g:--git-dir:--gitdir"/":" }),
    ({ "path",       Getopt.HAS_ARG,      "-p:--path:--repository-path"/":" }),
    ({ "name",       Getopt.HAS_ARG,      "-n:--name:--repository-name"/":" }),
    ({ "mailhost",   Getopt.HAS_ARG,      "-m:--mailhost:--mail-host"/":" }),
    ({ "index-mode", Getopt.HAS_ARG,      ({ "--index-mode" }) }),
    ({ "mountpoint", Getopt.HAS_ARG,      "-u:--repository-uri"/":" }),
    ({ "quiet",      Getopt.NO_ARG,       "-q:--quiet"/":" }),
    ({ "verbose",    Getopt.NO_ARG,       "-v:--verbose"/":" }),
    ({ "version",    Getopt.NO_ARG,       "-V:--version"/":" }),
    ({ "help",       Getopt.NO_ARG,       "-h:--help"/":" }) })), array opt)
    switch(opt[0])
    {
      case "logdir":
	logdir = canonicize_path(opt[1]);
	ASSERT_DIR(logdir);
	break;

      case "sync-db":	sync_db = 1; break;
      case "no-tags":	index_tags = 0; break;
      case "with-tags":	index_tags = 1; break;
      case "poll-wait": poll_frequency = (float)opt[1]; break;
      case "tag-wait":    max_tag_time = (int)opt[1]; break;
      case "ci-wait": max_checkin_time = (int)opt[1]; break;
      case "hostname":  my_hostnames += ({ opt[1] }); break;

      case "git-dir":
	opt[1] = canonicize_path(opt[1]);
	ASSERT_DIR(opt[1]);
	// Default to using the same name as git-web.
	name = name || Stdio.read_bytes(opt[1] + "/description");
	// Default the mountpoint to the repository name.
	if (!mountpoint &&
	    ((mountpoint = basename(opt[1])) == ".git")) {
	  mountpoint = basename(dirname(opt[1]));
	}
	// Default to same as the mountpoint.
	path = path || mountpoint;
	// We map git repositories onto the cvsscheme ":git:".
        opt[1] = ":git:" + opt[1];
	// FALL_THROUGH
      case "cvsroot":
	if(sscanf(cvsroot = opt[1], ":%s:%s@%s:%s",
		  cvsscheme, string user, cvshost, cvsroot) == 4)
	  cvsscheme = sprintf(":%s:%s@", cvsscheme, user);
	else if(sscanf(cvsroot, ":%s:%s", cvsscheme, cvsroot) == 2)
	  cvsscheme = sprintf(":%s:", cvsscheme);
	else
	  cvsscheme = ":local:";
	cvsroot = canonicize_path(cvsroot);
	ASSERT_DIR(cvsroot);
	if(!has_prefix(cvsroot, "/"))
	  FAIL("Cvsroot (-c) path component not absolute!");
	add_repository = 1;
	break;

      case "path":
	path = canonicize_path(opt[1]); // sanity checks below loop
	break;

      case "name":
	name = opt[1]; // ditto
	break;

      case "mailhost":
	sscanf(opt[1], "%*[@]%s", mailhost);
	break;

      case "index-mode":
	if(!(<"trigger", "sweep" >)[opt[1]])
	  FAIL("Illegal --index-mode parameter; use trigger or sweep.");
	index_mode = opt[1];
	break;

      case "mountpoint":
	mountpoint = opt[1];
	break;

      case "quiet":
	verbosity = 0;
	break;

      case "verbose":
	verbosity = 1;
	break;

      case "version":
	write(version + "\n");
	return 0;

      case "help":
	write(usage, version);
	return 0;
    }
  argv = Getopt.get_args(argv);

  array(mapping) reps;
  string get_repositories = "SELECT id,cvsroot_host,cvsroot_path,module,name,"
			    "uriname,mailhost,modified,cvsroot_scheme "
                            "FROM cl_repository";
  string where;
  if( name )
      where ="name like '"+db->quote(name)+"'";
  else
      where = "1";
  if(sizeof(my_hostnames = Array.uniq(my_hostnames)))
  {
    string cond;
    if(has_value(my_hostnames, ""))
      cond = "cvsroot_host IS NULL";
    if(sizeof(my_hostnames = my_hostnames - ({ "" })))
    {
      string hosts = sprintf("cvsroot_host IN ('%s')",
			     map(my_hostnames, db->quote) * "','");
      cond = cond ? cond + " OR " + hosts : hosts;
    }
    where += sprintf(" AND (%s)", cond);
  }
  create_needed_tables();
  reps = db->query(get_repositories + " WHERE " + where);
  signal(signum( "SIGQUIT" ), describe_all_threads);
  if(logdir)
    cd(logdir);


  if(add_repository)
  {
    if(!cvsroot)
      FAIL("No cvsroot (-c) specified!");
    if(!path)
      FAIL("No repository path (-r) specified!");
    if(has_prefix(path, cvsroot))
      sscanf(path, replace(cvsroot, "%", "%%") + "%*[/]%s", path);
    else if((< ".", "/" >)[path[0..0]])
      FAIL("The repository path (-r) must be a path below cvsroot!");
    if (cvsscheme != ":git:") {
      string fs_root = combine_path(cvsroot, path);
      ASSERT_DIR(fs_root);
    }

    mapping signified_by = ([ "cvsroot_path":cvsroot, "module":path ]);
    if(cvshost)
      signified_by->cvsroot_host = cvshost; // FIXME: possible clash
    else
      cvshost = "";
    string old_name = get("cl_repository", "name", signified_by) || path;

    if((name
     && name != old_name
     && has_value(reps->name, name))
    || (!name
     && !(name = old_name)
     && has_value(reps->name, name)))
      FAIL("Repository name %s already in use!", name);

    int rep_id = md5_56bit(cvshost + combine_path(cvsroot, path));
    mapping data = ([ "id":rep_id, "name":name, "cvsroot_scheme":cvsscheme ]);
    data->uriname = mountpoint || path; // FIXME: add uniquity check
    if(mailhost) data->mailhost = mailhost;
    if(index_mode) data->index_mode = index_mode;
    create_or_update("cl_repository", signified_by, data);
    traverse_and_update(rep_id, cvsroot, path, cvsscheme, 0, !!logdir);
  }
  else
  {
    if( !logdir )
      foreach(Array.shuffle(reps), mapping(string:string) r)
	{
	    if( !(int)r->id )
		r->id = (string)md5_56bit( r->name + r->cvsroot_path + r->cvsroot_host ); 
	    DEBUG("Checking repository: %O\n", r);
	    cvshost = lower_case(r->cvsroot_host || ""); // for calculating rep_id:s
	    traverse_and_update( (int)r->id, r->cvsroot_path, r->module,
				 r->cvsroot_scheme, r->modified);
	}
    if(!sizeof(reps))
      FAIL("No repositories set up. See --help for -r and -p options.");
  }

#define P(X, Y) (X ? X + " " Y + (X==1?"":"s") : "")
  if(verbosity > -1)
  {
    float seconds = (gethrtime() - t) / 1000000.0;
    string took = format_time(seconds), avg = "no";
    if( seen_files )
        avg =  format_time((seconds / (seen_files+0.01)));

    write("\nProcessed %s (%d updated) in %s.\nAverage time per file: %s.\n",
	  P(seen_files, "file"), parsed_files, took, avg);
    // For revisions without files.
    Stdio.write_file( "/tmp/have_git_revision", encode_value(have_git_revision));
    Stdio.write_file( "/tmp/rep_refs_mtimes", encode_value(rep_refs_mtimes));
    Stdio.write_file( "/tmp/rep_refs_timestamps", encode_value(new_timestamps));
  }

  if(logdir)
  {
    if(verbosity > 0)
      write("\nStarting incremental logger "
	    "(polling every %.1fs).\n", poll_frequency);
    call_out(meta_index_incrementally, 0, ([]));
    DEBUG("Activated backend.\n");
    return -1;
  }
}

void describe_all_threads()
{
  // Disable all threads to avoid potential locking problems while we
  // have the backtraces. It also gives an atomic view of the state.
  object threads_disabled = _disable_threads();

  Stdio.stderr.write("### Describing all pike threads:\n\n");

  array(Thread.Thread) threads = all_threads();
  array(string|int) thread_ids =
    map(threads,
	lambda (Thread.Thread t)
	{
	  string desc = sprintf ("%O", t);
	  if(sscanf(desc, "Thread.Thread(%d)", int i))
	    return i;
	  else
	    return desc;
	});
  sort(thread_ids, threads);

  int i;
  for(i=0; i < sizeof(threads); i++)
  {
    Stdio.stderr.write("### Thread %s%s:\n",(string)thread_ids[i],"");
    Stdio.stderr.write(describe_backtrace(threads[i]->backtrace()) + "\n");
  }

  Stdio.stderr.write("### Total %d pike threads\n", sizeof (threads));
  Stdio.stderr.write("### Pending call_outs:\n%O\n", call_out_info());
  threads = 0;
  threads_disabled = 0;
}

//! Returns a human-friendly string for @i{how long@} something took.
string format_time(int|float seconds)
{
  string time = "";
  seconds = (float)seconds;
  if(seconds < 0.0)
  {
    seconds = -seconds;
    time += "minus ";
  }
  int s = (int)round(seconds), d = s/3600/24, h = (s/3600)%24, m = (s/60)%60;
  s = s % 60;

  if(seconds >= 10.0)
    return time + String.implode_nicely(({ P(d, "day"), P(h, "hour"), 
					   P(m, "minute"), P(s, "second") })
					- ({ "" }));
  else if(seconds >= 1.0)
    return time + sprintf("%.1f seconds", seconds);
  foreach(({ "m", "u", "n", "p", "f", "a" }), string prefix)
    if((seconds *= 1000) >= 1.0 || prefix == "a")
      return time + sprintf("%.1f %ss", seconds, prefix);
}

void meta_index_incrementally(mapping(string:array(string)) state)
{
  string connection_lost = "Lost connection to MySQL server during query";
  mixed error = catch(index_incrementally(state));
  if(!error)
    return;
  if(has_value(error[0], connection_lost))
  {
    werror("%s; retrying.\n", connection_lost);
    meta_index_incrementally(state);
  }
  else
  {
      werror("Error caught in index_incrementally:\n%s\n", describe_backtrace(error) );
    werror("Retrying in %O seconds.\n", poll_frequency);
    call_out(meta_index_incrementally, poll_frequency, state);
  }
}

void create_needed_tables()
{
db->query(#"CREATE TABLE IF NOT EXISTS cl_repository (
  id             bigint unsigned primary key,
  name           varchar(128) character set 'utf8' not null,
  uriname        varchar(128) not null unique,
  mailhost       varchar(128) null,
  modified       datetime not null,
  cvsroot_scheme varchar(40) binary null,
  cvsroot_host   varchar(60) null,
  cvsroot_path   varchar(200) binary not null,
  module         varchar(200) binary not null,
  type           enum('core','misc','adjunct','platform') not null default 'core',
  index_mode     enum('sweep','trigger') not null default 'sweep',
  unique(cvsroot_host,cvsroot_path,module),
  index(name),
  index(module),
  index(id))");

#if 0
 db->query("DROP TABLE cl_dir");
 db->query("DROP TABLE cl_file");
 db->query("DROP TABLE cl_revision");
 db->query("DROP TABLE cl_checkin_message");
 db->query("DROP TABLE cl_user");
#endif

db->query(#"CREATE TABLE IF NOT EXISTS cl_dir (
  id             bigint unsigned not null,
  repository_id  bigint unsigned not null,
  parent_dir_id  bigint unsigned not null,
  path           varchar(240) character set 'utf8' not null,
  name           varchar(127) character set 'utf8' not null,
  modified       datetime not null,
  primary key(repository_id,parent_dir_id,name),
  unique(repository_id,path),
  index (id),
  index (modified),
  index (path))");

db->query(#"CREATE TABLE IF NOT EXISTS cl_file (
  id             bigint unsigned not null,
  repository_id  bigint unsigned not null,
  parent_dir_id  bigint unsigned not null,
  name           varchar(127) character set 'utf8' binary not null,
  modified       datetime not null,
  primary key(repository_id,parent_dir_id,name),
  index(id),
  index(modified))");

db->query(#"CREATE TABLE IF NOT EXISTS cl_revision (
  file_id        bigint unsigned not null,
  path		 varchar(255) character set 'utf8' not null,
  user_id        bigint unsigned not null,
  at             datetime not null,
  state          enum('changed', 'added', 'removed') not null,
  repository_id  bigint unsigned not null,
  ancestor	 varchar(64) binary null,
  revision       varchar(64) binary not null,
  branch         varchar(64) binary not null,
  l_total        int unsigned not null,
  l_added        int unsigned not null,
  l_removed      int unsigned not null,
  message_id     bigint unsigned not null,
  type           enum('normal','merge') not null,
  primary key(repository_id,file_id,revision),
  index          rep_rev_index(repository_id,revision),
  index(path(240)),
  index(branch),
  index(at),
  index(user_id),
  index(revision),
  index(file_id),
  index(message_id),
  index(repository_id),
  index(type),
  index(state))");

db->query(#"CREATE TABLE IF NOT EXISTS cl_checkin_message (
  id             bigint unsigned not null primary key,
  message        longtext character set 'utf8' not null,
  fulltext key   message (message))");

db->query(#"CREATE TABLE IF NOT EXISTS cl_user (
  id		 bigint unsigned not null primary key,
  login		 varchar(255) character set 'utf8' not null unique)");

db->query(#"CREATE TABLE IF NOT EXISTS cl_module_owners (
  module         varchar(100) NOT NULL default '',
  owner          varchar(100) NOT NULL default '',
  index(module),
  index(owner))");
}

void index_incrementally(mapping(string:array(string)) state,
			 int(0..1)|void synchronously,
			 int|void only_process_repository)
{
  int ctime;
  array(string) events = get_dir(".") - indices(state), files;
  string notify_file, file, command, cvsroot, dir;
  foreach((array)mkmapping(events, map(events, file_stat)),
	  [notify_file, Stdio.Stat stat])
  {
    state[notify_file] = ({}); // will contain file names later on for commits
    ctime = stat->ctime;
    file = Stdio.read_file(notify_file);
    sscanf(file, "%s\n%s\n%s", command, cvsroot, file);
    // werror("%s %s %s\n", command, notify_file, (file/"\n")[0]);
    switch(command)
    {
      case "commit": // dir, {file}+
	sscanf(file, "%s\n%s", dir, file);
	sscanf(dir, cvsroot + "%*[/]%s", dir);
	sscanf( dir, "%s/Attic", dir );
	files = (file / "\n") - ({ "" });

	perhaps_reindex_files(cvsroot, dir, files, ctime, notify_file, state,
			      synchronously, only_process_repository);
	break;

      default: werror("Log file %s records unknown cvs action %O!",
		    notify_file, command);
    }
  }

  DEBUG("Entries left on queue: %d\n", sizeof(state));
  call_out(meta_index_incrementally, poll_frequency, state);
}

void perhaps_create_cl_dir(int rep_id, string virt_path)
{
  if(virt_path == "") return;
  string parent_path = dirname(virt_path), name = basename(virt_path);
  int dir_id = md5_56bit(parent_path), id = md5_56bit(virt_path);
  if( !directories[dir_id+"|"+id] )
  {
      directories[dir_id+"|"+id]=1;
      db->query( "REPLACE INTO cl_dir (repository_id,parent_dir_id,name,path,id) "
		 "VALUES (%d,%d,N%s,N%s,%d)",
		 rep_id, dir_id, name, virt_path, id );
      perhaps_create_cl_dir(rep_id, parent_path);
  }
}

//! Check if the @[files] committed to the directory @[cvsroot]/@[dir]
//! belonged to one of the repositories in cl_repository that we keep
//! track of, and, if so, reindex those files.
void perhaps_reindex_files(string cvsroot, string dir, array(string) files,
			   int notify_time, string notify_file,
			   mapping(string:array(string)) state,
			   int(0..1)|void synchronously,
			   int|void only_process_repository)
{
  [dir, string module, int rep_id] = associate_to_repository(cvsroot, dir);
  if(!zero_type(only_process_repository) && rep_id != only_process_repository)
      return;
  if(!module)
  { // this commit did not happen in a directory we keep a record of:
    rm(notify_file);
    werror("Commit %s (-:%s): Module not handled.\n", notify_file, dir);
//    processed[notify_file]=1;
    m_delete(state, notify_file);
    return;
  }
  perhaps_create_cl_dir(rep_id, dir);
  string  fs_dir = combine_path(combine_path(cvsroot, module), dir),
      virt_dir = replace(replace(dir, "/Attic/", "/"), "/Attic", "");
  if( virt_dir == "Attic" || virt_dir == "Attic/" )
      virt_dir = "";
  int dir_id = md5_56bit(virt_dir), file_id;
  Stdio.Stat stat;
  foreach(files, string file)
  {
    string fs_path = combine_path(fs_dir, file + ",v"), save,
	 virt_path = combine_path(virt_dir, file);
    state[notify_file] += ({ virt_path });
    file_id = md5_56bit(virt_path);

    stat = file_stat(fs_path);
    if(!stat)
    {
      save = fs_path; // prepare for a possible error message below
      fs_path = combine_path(fs_dir, "Attic/" + file + ",v");
      stat = file_stat(fs_path);
    }
    if(!stat)
    {
      werror("Couldn't stat neither %s nor %s\n", save, fs_path);
      continue;
    }
    werror("   Indexing %s\n", fs_path);
     index_file_when_ready(rep_id, dir_id, file_id, fs_path, virt_path,
			    stat->mtime, notify_time, notify_file, state,
			    only_process_repository);
  }
}

//! Update directory mtimes upward in the dir hierarchy until we hit the root.
//! @param virt_path
//!   Path to some file in the directory where the bubbling starts
void bubble_mtime_to_root(int rep_id, string virt_path, string mtime)
{
    string virt_dir = dirname(virt_path);
    if(!strlen(virt_dir))
    {
//	werror("Updating mtime of "+rep_id+" to "+mtime+"\n");
	db->query("UPDATE cl_repository SET modified=GREATEST(modified,%s)"
		  " WHERE id=%d", mtime, rep_id);
    }
    else
    {
	db->query("UPDATE cl_dir SET modified=GREATEST(modified,%s)"
		  " WHERE repository_id=%d AND path=%s", mtime, rep_id, virt_dir);
//	if( db->master_sql->affected_rows() )
	    bubble_mtime_to_root( rep_id, virt_dir, mtime );
    }
}

Thread.Queue index_when_ready_queue = Thread.Queue();

void check_index_file_when_ready() 
{
    Stdio.Stat stat, stat2;
    while( 1 )
    {
	if( mixed err = catch {
		[int rep_id, int dir_id, int file_id,
		 string fs_path, string virt_path, int file_mtime,
		 int notify_time, string notify_file, mapping state,
		 int no_checkins] = index_when_ready_queue->read();
		do {
		    stat = file_stat(fs_path);
		    if( stat->mtime > time()-30 )
			sleep(4);
		    else
			sleep(2);
		    stat2 = file_stat(fs_path);
		} while( stat2->mtime != stat->mtime );
		call_out( _index_file_when_ready,0, 
			  rep_id,dir_id,file_id,fs_path,
			  virt_path,stat->mtime,notify_time,
			  notify_file,state,no_checkins);
	    } )
	    werror("Error while indexing file [file removed?]\n"+describe_backtrace(err));
    }
}

array index_when_ready_threads = ({});
//! Index @[fs_path] and remove the @[notify_file] from @[state] and disk.
void index_file_when_ready(int rep_id, int dir_id, int file_id,
			   string fs_path, string virt_path, int file_mtime,
			   int notify_time, string notify_file, mapping state,
			   int|void no_checkins)
{ 
    index_when_ready_queue->write( ({  rep_id,  dir_id,  file_id,
				       fs_path,  virt_path,  file_mtime,
				       notify_time,  notify_file,  state,
				       no_checkins }) );
    if( sizeof( index_when_ready_threads ) < 50 )
	index_when_ready_threads += ({ thread_create( check_index_file_when_ready ) });
}
	
void _index_file_when_ready(int rep_id, int dir_id, int file_id,
			   string fs_path, string virt_path, int file_mtime,
			   int notify_time, string notify_file, mapping state,
			   int|void no_checkins)
{
    int t = time();
    float f = time(t);
    werror("Really index file "+fs_path+"..\n");
    index_file(rep_id, dir_id, file_id, fs_path, virt_path,
	       file_mtime, no_checkins);
    werror( "%s %.2f\n", fs_path, (time(t)-f));
    bubble_mtime_to_root(rep_id, virt_path, mtime_to_string(file_mtime));
    if( state[notify_file] )
    {
	state[notify_file] -= ({ virt_path });
	if(!sizeof(state[notify_file])) // FIXME: racing with += in caller?
	{
	    if( !rm(notify_file) )
		werror("Failed to remove "+notify_file+"\n");
	    else
		werror("Removed "+notify_file+"\n");
	    m_delete(state, notify_file);
	}
    }
}

//! Shorthand for finding the name of repositories we've already associated.
static mapping(int:string) rep_names = ([]);
string get_repository_name(int rep_id)
{
  return rep_names[rep_id] ||
	(rep_names[rep_id] = get("cl_repository", "name", (["id":rep_id])));
}

//! If path @[path] (relative to @[cvsroot]) belonged to any monitored
//! repository, return the relevant data for that repository.
//! @returns
//!   @array
//!     @elem string path
//!       The remainder of the path, after a possible @[module] is removed.
//!       (Any resulting leading slashes (/) in the path are removed too.)
//!     @elem string|int(0..0) module
//!       The repository path to the module or @code{0@}, if unmonitored.
//!     @elem int repository_id
//!       The repository id or @code{0@}, if unmonitored.
//!   @endarray
array(string) associate_to_repository(string cvsroot, string path)
{
  catch(db->query("SELECT 1;")); // Make really sure the db is online.
  cvsroot = expand_symlinks(cvsroot);
  string module;
  array(mapping) matches;
  sscanf(path, "%[^/]", module);
  module = db->quote(module);
  matches = db->query("SELECT name,cvsroot_path,module,id FROM cl_repository"
		      " WHERE module=%s"
		      " AND (cvsroot_host=%s OR cvsroot_host IS NULL)"
		      " ORDER BY LENGTH(module) DESC", module, cvshost||"");

  matches = filter(matches, map(map(matches->cvsroot_path, expand_symlinks),
				`==, cvsroot));
  foreach(matches, mapping repository)
  {
    string module = repository->module,
	fs_module = combine_path(cvsroot, module);
    if(sscanf(path, replace(fs_module, "%", "%%") + "%*[/]%s", path)
       || sscanf(path, replace(module, "%", "%%") + "%*[/]%s", path))
    {
      rep_names[(int)repository->id] = repository->name;
      return ({ path, module, (int)repository->id });
    }
  }
  return ({ path, 0, 0 });
}

void traverse_and_update(int rep_id, string cvsroot, string module,
			 string scheme,
			 int|string mtime, int|void scrape_logdir)
{
  if(scheme == ":git:")
    traverse_and_update_git(rep_id, cvsroot);
  else
    traverse_and_update_cvs(rep_id, cvsroot, module, mtime, scrape_logdir);
}

 //! Perform a git command.
 //!
 //! @returns
 //!   Returns the stdout from the command.
 //!
 //! @throws
 //!   Throws @expr{0@} (zero) on git failure.
 //!   Note that this zero is catched by @[update_git()].
 string git_cmd(string root, string cmd, string ... args)
 {
     Stdio.File pipe = Stdio.File();
  
     args = ({ "git", "--git-dir=" + root, cmd }) +args;
     DEBUG(args*" "+"\n");
     Process.Process pid = Process.Process(args,([ "stdout": pipe->pipe()]));
     string res = pipe->read();
     if (pid->wait()) throw(0); // Failure!
     return res;
 }

static mapping(string:string) main_git_branches = ([]);
static mapping(string:int) have_git_rep_rev_ = ([]);

//! Check if we know of revision @[rev].
int have_git_rep_rev(int rep_id, string rev)
{
  if( have_git_revision[rev] ) return 1;
  string key = rep_id + "\0" + rev;
  int ret = have_git_rep_rev_[key];
  if (!zero_type(ret)) 
    return ret;


  return
    have_git_rep_rev_[key] = sizeof(db->query("SELECT file_id "
					      "  FROM cl_revision "
					      " FORCE INDEX (rep_rev_index) "
					      " WHERE repository_id = %d "
					      "   AND revision = %s "
					      " LIMIT 1",
					      rep_id, rev));
}

#define INIT_USERS() do {                                       \
        if( !users )                                                    \
        {                                                               \
            array tmp = db->query( "SELECT login,id FROM cl_user" );    \
            users = mkmapping(tmp->login,(array(int))tmp->id);          \
        }                                                               \
    } while(0)


#define INIT_MESSAGES()                                                \
    do {                                                               \
        if(!messages) {                                                     \
            array q = db->query("SELECT id FROM cl_checkin_message")->id;   \
            messages = mkmapping((array(int))q,q);                          \
        }                                                                   \
    } while(0)

//! Add a single git revision (aka commit) to the db.
//! @returns
//!   Returns the maximum commit timestamp.
int add_git_revision(int rep_id, string root, string branch,
                     string patch)
{
  string extra;
  parsed_files++;
  String.SplitIterator lines = String.SplitIterator(patch, '\n', 0,
						    lambda() {
						      string ret = extra;
						      extra = 0;
						      return ret;
						    });
  mapping(string:string|int|array(string)) rev = ([]);
  foreach(lines;;string line) {
    if (line == "") break;
    array(string) cmd = line/" ";
    switch(cmd[0]) {
    case "parent":
      if (rev->parent) {
	if (rev->merge)
	  rev->merge += ({ cmd[1] });
	else
	  rev->merge = ({ cmd[1] });
      } else {
	rev->parent = cmd[1];
      }
      break;
    case "commit": case "tree":
      // Note: Only keeps track of the first of each kind.
      rev[cmd[0]] = rev[cmd[0]] || cmd[1];
      break;
    case "author": case "committer":
      // Note: Only keeps track of the first of each kind.
      if (!rev[cmd[0]]) {
	rev[cmd[0]] = cmd[1..sizeof(cmd)-3]*" ";
	rev[cmd[0]+"_ts"] = (int)cmd[-2];
	rev[cmd[0]+"_tz"] = (int)cmd[-1];
      }
      break;
    default:
      werror("Unhandled git header line: %O\n", line);
      break;
    }
  }

  INIT_USERS();
  if(zero_type(rev->author_id = users[rev->author])) // (doc/sqltables.txt for more info)
  {
    rev->author_id = users[rev->author] = md5_56bit(rev->author);
    db->query( "REPLACE DELAYED INTO cl_user (id,login) VALUES (%d,N%s)",
	       rev->author_id, rev->author );
  }

  // FIXME: Ought to keep track of the Author timestamp as well.

  int mtime = rev->committer_ts;
  if (rev->merge) {
    // Append the diff.
    extra = "codelibrarian\n" + git_cmd(root, "diff", rev->parent, rev->commit);

    // Add the commits from the merged branch(es) to the db.
    // FIXME: Attempt to identify a name(s) of the branch(es)?
    // FIXME: The db ought to keep track of from where stuff was merged.
    foreach(rev->merge, string merge) {
      if (!have_git_rep_rev(rep_id, merge)) {
	int ts = update_git(rep_id, root, merge);
	if (ts > mtime) mtime = ts;
      }
    }
  }
  
  int merge_only;
  mapping(string:int) files = ([]);
  mapping(string:string|int) diff;
  foreach(lines;; string line) {
    array(string) cmd = line/" ";
    switch(cmd[0]) {
    case "":
      if (!diff) {
	rev->log = (rev->log || "") + cmd[3..]*" " + "\n";
      }
      break;
    default:
      if (diff->path) {
	if (merge_only) break;
	if (has_prefix(cmd[0], "+")) {
	  diff->lines++;
	  diff->added++;
	} else if (has_prefix(cmd[0], "-")) {
	  diff->lines++;
	  diff->removed++;
	}
      } else if (has_prefix(cmd[0], "+++") || has_prefix(cmd[0], "---")) {
	if (cmd[1] != "/dev/null") {
	  if (sizeof(cmd) > 2) {
	    diff->path = (cmd[1..] * " ");
	  } else {
	    diff->path = cmd[1];
	  }
	  if (has_suffix(diff->path, "\t")) {
	    // Git kludge for GNU patch compat.
	    // cf git.git:1a9eb3b9d50367bee8fe85022684d812816fe531
	    diff->path = diff->path[..sizeof(diff->path)-2];
	  }
	  if (has_prefix(diff->path, "\"") && has_suffix(diff->path, "\"")) {
	    // Quoted path. Decode escapes.
	    string path = "";
	    for (int i = 1; i < sizeof(diff->path)-1; i++) {
	      string c = diff->path[i..i];
	      if (c != "\\") path += c;
	      else {
		int octal;
		int delta;
		sscanf(diff->path[i+1..], "%3o%n", octal, delta);
		path += sprintf("%c", octal);
		i += delta;
	      }
	    }
	    diff->path = path;
	  }
	  diff->path = diff->path[2..];
	}
      } else {
	werror("Unrecognized diff line: %O\n", line);
      }
      break;
    case "deleted":
      diff->state = "removed";
      break;
    case "Binary":
      diff->lines = 1;
      diff->added = 1;
      diff->removed = 1;
      sscanf(line, "Binary files %*s and b/%s differ", diff->path);
      // werror("Binary diff:\n%O\n", patch);
      break;
    case "codelibrarian":	// Magic marker.
    case "diff":
      if (!rev->log_id) {
	rev->log_id = md5_56bit(rev->log);
    INIT_MESSAGES();
	if( !messages[rev->log_id] )
	{
	  db->query("REPLACE DELAYED INTO cl_checkin_message "
		    "(id, message) VALUES (%d,N%s)", rev->log_id, rev->log);
	  messages[rev->log_id]=1;
	}
      }
      string path;
      if (diff && (path = (diff->path || diff->diff_path)) && !files[path]) {
	// Make sure not to overwrite any real patches with
	// dummy patches from merges.
	files[path] = 1;
	int file_id = md5_56bit(path);
	string dir = dirname(path);
	int dir_id = md5_56bit(dir);
	perhaps_create_cl_dir(rep_id, dir);
	db->query( "REPLACE DELAYED INTO cl_file "
		   "(repository_id, parent_dir_id, name, id, modified) "
		   "VALUES (%d,%d,N%s,%d,%s)", 
		   rep_id, dir_id, basename(path),
		   file_id, mtime_to_string(rev->committer_ts));
	if (rev->parent && (diff->state != "added")) {
	  db->query("REPLACE DELAYED INTO cl_revision "
		    "(repository_id,file_id,revision,path,ancestor,"
		    "user_id,at,state,branch,"
		    "l_total,l_added,l_removed,message_id,type) "
		    "VALUES (%d,%d,%s,N%s,%s, %d,%s,"
		    "        %s,%s,%d,%d,%d,%d,%s)", 
		    rep_id, file_id, rev->commit, path, rev->parent,
		    rev->author_id, mtime_to_string(rev->committer_ts),
		    diff->state, branch,
		    diff->lines, diff->added, diff->removed, rev->log_id,
		    ((rev->merge || is_merge(rev->log))?"merge":"normal"));
	} else {
	  db->query("REPLACE DELAYED INTO cl_revision "
		    "(repository_id,file_id,revision,path,ancestor,"
		    "user_id,at,state,branch,"
		    "l_total,l_added,l_removed,message_id,type) "
		    "VALUES (%d,%d,%s,N%s,NULL, %d,%s,"
		    "        %s,%s,%d,%d,%d,%d,%s)", 
		    rep_id, file_id, rev->commit, path,
		    rev->author_id, mtime_to_string(rev->committer_ts),
		    diff->state, branch,
		    diff->lines, diff->added, diff->removed, rev->log_id,
		    ((rev->merge || is_merge(rev->log))?"merge":"normal"));
	}
      }
      diff = ([]);
      if (cmd[0] == "codelibrarian") merge_only = 1;
      diff->state = "changed";
      foreach(cmd[2..], string path) {
	if (path == "/dev/null") {
	  if (diff->diff_path)
	    diff->state = "removed";
	  else
	    diff->state = "added";
	} else if (has_prefix(path, "a/")) {
	  diff->diff_path = path[2..];
	} else if (has_prefix(path, "b/")) {
	  diff->diff_path = path[2..];
	  break;
	}
      }
      break;
    case "mode":
    case "old":
    case "new":
    case "@@":
    case "index":
      // Ignore.
      break;
    }
  }
  string path;
  if (diff && (path = (diff->path || diff->diff_path))) {
    int file_id = md5_56bit(path);
    string dir = dirname(path);
    int dir_id = md5_56bit(dir);
    perhaps_create_cl_dir(rep_id, dir);
    db->query( "REPLACE DELAYED INTO cl_file "
	       "(repository_id, parent_dir_id, name, id, modified) "
	       "VALUES (%d,%d,N%s,%d,%s)", 
	       rep_id, dir_id, basename(path),
	       file_id, mtime_to_string(rev->committer_ts));
    if (rev->parent && (diff->state != "added")) {
      db->query("REPLACE DELAYED INTO cl_revision "
		"(repository_id,file_id,revision,path,ancestor,"
		"user_id,at,state,branch,"
		"l_total,l_added,l_removed,message_id,type) "
		"VALUES (%d,%d,%s,N%s,%s, %d,%s,"
		"        %s,%s,%d,%d,%d,%d,%s)", 
		rep_id, file_id, rev->commit, path, rev->parent,
		rev->author_id, mtime_to_string(rev->committer_ts),
		diff->state, branch,
		diff->lines, diff->added, diff->removed, rev->log_id,
		((rev->merge || is_merge(rev->log))?"merge":"normal"));
    } else {
      db->query("REPLACE DELAYED INTO cl_revision "
		"(repository_id,file_id,revision,path,ancestor,"
		"user_id,at,state,branch,"
		"l_total,l_added,l_removed,message_id,type) "
		"VALUES (%d,%d,%s,N%s,NULL, %d,%s,"
		"        %s,%s,%d,%d,%d,%d,%s)", 
		rep_id, file_id, rev->commit, path,
		rev->author_id, mtime_to_string(rev->committer_ts),
		diff->state, branch,
		diff->lines, diff->added, diff->removed, rev->log_id,
		((rev->merge || is_merge(rev->log))?"merge":"normal"));
    }
  } else {
    werror("No files in revision %O!\n", rev->commit);
    have_git_revision[rev->commit]=1;
  }
  string key = rep_id + "\0" + rev->commit;
  have_git_rep_rev_[key] = 1;
  return mtime;
}

//! Add a list of git revisions (aka commit) to the db.
//! @returns
//!   Returns the maximum commit timestamp.
int add_git_revisions(int rep_id, string root, string branch, array(string) revision)
{
  int mtime;
  void process_checkins( array tmp )
  {
    if (!sizeof(tmp)) return;

    string patch = git_cmd(root, "show", "--encoding=UTF8", "--pretty=raw",
                           @tmp);
      
    String.SplitIterator lines = String.SplitIterator(patch, '\n', 0 );
    string one_checkin = "";
    foreach( lines;; string line )
    {
      if( has_prefix( line, "commit ") )
      {
        DEBUG( line+"\n");
        if( strlen( one_checkin ) )
          add_git_revision( rep_id,root,branch,one_checkin );
        one_checkin="";
      }
      one_checkin += line+"\n";
    }
    if( strlen( one_checkin ) )
      mtime = max(mtime,add_git_revision( rep_id,root,branch,one_checkin ));
  };
  array tmp = ({});
  foreach( revision;int i; string x )
  {
    if( have_git_revision[x] || have_git_rep_rev_[x] ) continue;
    tmp += ({ x });

    if(i == sizeof(revision)-1 || sizeof(tmp) > 200 )
    {
      process_checkins( tmp );
      tmp = ({});
    }
  }
  process_checkins( tmp );
  return mtime;
}

mapping(string:int) timestamps = ([]);
mapping(string:int) new_timestamps = ([]);
string get_last_index( int id, string branch )
{
  int ret = timestamps[id+"\0"+branch];
  new_timestamps[id+"\0"+branch] = time(1);
  return (string)ret;
}


//! Update a git branch identified by @[branch].
//! Set @[is_branch] to @expr{1@} if it is a true branch.
int update_git(int rep_id, string root, string branch, int|void is_branch)
{
  seen_files++;
  werror("Updating git branch %O...\n", branch);
  string rev_list = git_cmd(root, "rev-list", "--first-parent", branch);
  array(string) revisions = rev_list/"\n" - ({""});
  int min, max = sizeof(revisions), probe = max/2;
  int mtime = -0x80000000;
  if (!max) return mtime;
  do {
    // werror("probe: %d <= %d < %d\n", min, probe, max);
    if (have_git_rep_rev(rep_id, revisions[probe])) {
      max = probe;
    } else {
      min = probe;
    }
    probe = (min + max)/2;
  } while (max > min+1);

  werror("Found %d new revisions.\n", min+1);
  return
    add_git_revisions(rep_id, root,
                      main_git_branches[rep_id + "\0" + revisions[min]] ||
                      branch,
                      reverse(revisions[..min]));
}

mapping rep_refs_mtimes = ([]);
mapping(string:int) check_cached_refs_id( string rep_root )
{
  void rstat( string path, mapping res )
  {
    array q = get_dir( path );
    if( !q )
    {
      werror("Failed to read %O\n", path );
      return;
    }
    foreach( q, string p )
    {
      seen_files++;
      string pn = combine_path( path, p );
      Stdio.Stat x = file_stat( pn );
      if( !x ) continue;
      res[pn]=x->mtime;
      if( x->isdir )
        rstat( pn, res );
    }
  };

  mapping old_files = rep_refs_mtimes[rep_root];
  mapping new_files = ([]);

  rstat( combine_path( rep_root, "refs" ), new_files );
  seen_files++;
  if( Stdio.Stat x = file_stat(combine_path( rep_root, "packed-refs" )) )
    new_files[combine_path( rep_root, "packed-refs" )]=x->mtime;
  if( equal( old_files, new_files ) )
  {
    werror("%O unmodified\n", rep_root );
    return UNDEFINED;
  }
  werror("%O modified\n", rep_root );
  return new_files;
}


void traverse_and_update_git(int rep_id, string root)
{
  mapping(string:int) mtimes;
  if( !(mtimes = check_cached_refs_id( root )) )
    return;

  mixed err = catch {
      // Get the local branches and their heads.
      // FIXME: Import all branches?
      string branch_info = git_cmd(root, "branch", "-v", "-a", "--no-abbrev");
      array(string) branches = ({});
      foreach(branch_info/"\n" - ({""}), string line) {
	string star, branch, head, msg;
	if (sscanf(line, "%1s %s%*[ ]%[0-9a-z] %s", star, branch, head, msg)
	    == 5) {
	  // FIXME: Ought to check that the number of files matches
	  //        with the number of files in the git commit.
	  seen_files++;
	  if (have_git_rep_rev(rep_id, head)) continue;
	  if( star == "*" )
	    branches = ({ branch }) + branches;
	  else
	    branches += ({ branch });
	} else if (sscanf(line, "%1s %s%*[ ]-> %s", star, branch, head)
		   == 4) {
	  // Branch alias; typically origin/HEAD -> origin/master.
	  // Ignore for now, since the real branch will be added anyway.
	} else {
	  werror("Failed to parse branch information line %O\n", line);
	}
      }
      // Make sure merges don't rename any branches.
      foreach(branches, string branch) {
	array(string) revisions =
	  git_cmd(root, "rev-list", "--first-parent", branch)/"\n" - ({""});
	if( !sizeof( revisions ) )
	  continue;
	int min, max = sizeof(revisions), probe = max/2;
	do {
	  // werror("probe: %d <= %d < %d\n", min, probe, max);
	  if (main_git_branches[rep_id + "\0" + revisions[probe]]) {
	    max = probe;
	  } else {
	    min = probe;
	  }
	  probe = (min + max)/2;
	} while (max > min+1);
	if (!main_git_branches[rep_id + "\0" + revisions[min]]) {
	  for (;min >= 0;min--) {
	    main_git_branches[rep_id + "\0" + revisions[min]] = branch;
	  }
	}
      }
      foreach(branches, string branch) {
	int mtime = update_git(rep_id, root, branch, 1);
	bubble_mtime_to_root(rep_id, "", mtime_to_string(mtime));
      }

      // Everything ok. Propagate the new mtimes to the cache.
      rep_refs_mtimes[root] = mtimes;
    };
  // NB: Don't throw zero!
  if (err) throw(err);
}

void traverse_and_update_cvs(int rep_id, string cvsroot, string module,
			     int|string mtime, int|void scrape_logdir)
{
  mtime=0;
  if(!intp(mtime))
    if(mtime == "0000-00-00 00:00:00")
      mtime = 0;
    else
      mtime = parse_time(mtime)->unix_time();

  string cond = "";
//  if(verbosity > 0)
    write("\nScanning module %s (id %d):\n", module, rep_id);

  // By setting this to max(all mtimes for the repository) instead of
  // the time() we started traversing, we avoid dependencies on the
  // system clock on the machine running the crawler, which is good:
  //
  // However, it fails to detect checkins that occur while we are traversing!
  int ntime = time();
  mtime = min(traverse_repository(rep_id, cvsroot, module, mtime, "", 0, 0.0, 1.0),ntime);

  if(scrape_logdir) // we might have (partially) missed commits during crawling:
    index_incrementally(([]), 1, rep_id);

  db->query("UPDATE cl_repository SET modified=%s WHERE id=%d",
	    mtime_to_string(mtime), rep_id);
}

static function is_cvs_cruft = Regexp("^(CVS$|,.*,$|#cvs.(lock$|[rw]fl))")->match;

//!
int(0..1) is_repository_file(string filename)
{
  return !is_cvs_cruft(filename);
}

//! Used mostly for hashing data names to persistent unique id:s for various
//! cl_* tables - the idea is to get a unique id that will regenerate itself
//! even in the event of a restart from scratch, one or more tables wiped.
int md5_56bit(string data)
{
//  return data && array_sscanf(Crypto.MD5.hash(data), "%4c")[0];
    return data && (array_sscanf(Crypto.MD5.hash(data), "%7c")[0]);
}

//! Convenience method to get the column (or columns) @[field] from
//! the database table @[table], where the conditions @[where] are
//! satisfied. The result is never more than one row (in fact, a
//! @code{" LIMIT 1"@} is always appended to the query), and if no
//! rows match, the value @code{0@} is returned.
string|mapping get(string table, string|array field, array|mapping|string where)
{
  if(mappingp(where))
    where = format_idx_is_val(where);
  if(arrayp(where))
    where *= " AND ";
  string q = sprintf("SELECT %s FROM %s WHERE %s LIMIT 1",
		     (stringp(field) ? field : field * ","), table, where);
  array(mapping) rows = db->query(q);
  if(sizeof(rows))
      if(arrayp(field))
	  return rows[0];
      else
	  return rows[0][field];
}


array(string) format_idx_is_val(mapping(string:mixed) what)
{
  return Array.map((array)what,
		   lambda(array w)
		   {
		     return sprintf("%s='%s'", w[0],
				    db->quote((string)w[1]));
		   });
}

//! Updates the row matching @[where] in table @[table] with
//! the additional data @[extra_data], or creates that row if
//! it did not already exist.
//! @returns
//!   1 if the was created (didn't already exist), otherwise 0
int create_or_update(string table, array|mapping where,
		     mapping|void extra_data)
{
  if(mappingp(where))
    where = format_idx_is_val(where);
  string query, set;
  if(extra_data && sizeof(extra_data))
    set = format_idx_is_val(extra_data) * ",";
  if(get(table, (where[0]/"=")[0], where))
  {
    if(!set)
      return 0; // already done; no need for a NO-OP database roundtrip
    query = sprintf("UPDATE LOW_PRIORITY %s SET %s WHERE %s", table, set, where * " AND ");
  }
  else
    query = sprintf("INSERT LOW_PRIORITY INTO %s SET %s", table,
		    (set ? set + "," : "") + where * ",");
  //NOISE("db: %s\n", query);
  db->query(query);
  return has_prefix(query, "INSERT");
}

//! Recurse through repository @[rep_id], updating `db' with new findings.
//! @param tags_in_this_dir
//!   When supplied, only the given directory is traversed, and just the tags
//!   of the encountered files are processed; checkins are thus ignored.
//! @returns
//!   A time_t of the latest change found in the traversed directory.
//! @note
//!   exit(1)s if it fails to acquire a MySQL cooperative advisory lock on
//!   "Code Librarian indexing " + @[rep_id].
//! @note
//!   If @[sync_db] is 1 then any no longer existing files present in the db
//!   will be purged and warnings echoed to stderr accordingly.
mapping directories = ([]);
int traverse_repository(int rep_id, string cvs_root, string module,
			int|void last_mod, string|void dir,
			int|void tags_in_this_dir,
			float|void progress, float|void phase_size)
{
  if(!dir)
    dir = "";
  seen_dirs++;
  string rp_root = combine_path(cvs_root, module),// repository root in real fs
	  fs_dir = combine_path(rp_root, dir),    // current dir in real fs
	virt_dir = replace(dir, "/Attic/", "/");  // current dir in virtual fs

  virt_dir = replace(virt_dir, "/Attic", "");
  if( virt_dir == "Attic" || virt_dir == "Attic/" )  virt_dir = "";
  int dir_id = md5_56bit(virt_dir), id, mtime;
  array(string) dirlist = get_dir(fs_dir);
  if(!dirlist)
  {
    werror("Couldn't list directory %s\n", fs_dir);
    return 0;
  }
  DEBUG("Entering dir: %s\n", fs_dir[sizeof(cvs_root)+1..]);

  float increment;
  Tools.Install.ProgressBar pbar;
  dirlist = filter(dirlist, is_repository_file);
  if(progress && sizeof(dirlist))
  {
    pbar = Tools.Install.ProgressBar("Parsing files", 0, sizeof(dirlist),
				     progress, phase_size);
    progress -= increment = phase_size / sizeof(dirlist);
  }
  if(sync_db)
  {
    if(progress && pbar && verbosity > 0)
    {
      pbar->set_name("Syncing rm:s");
      UPDATE_PROGRESSBAR(pbar, 0);
    }
    string nl = "\n", names = "('" + map(dirlist, db->quote) * "','" + "')";
    nl = purge_deleted_dirs(rep_id, dir_id, dir, names, nl);
    nl = purge_deleted_files(rep_id, dir_id, dir, names, nl);
    if(nl == "\r")
      werror("\n");
  }
  foreach(reverse(sort(dirlist)), string filename)
  {
    progress += increment;
    string path = combine_path(dir, filename),   // file relative to repository
      virt_path = combine_path(virt_dir, filename), // path, as shown in cvsbrowser
	fs_path = combine_path(rp_root, path);   // file path in real fs

    Stdio.Stat stat = file_stat(fs_path); // FIXME: avoid inf. symlink recursion
    if(!stat)
    {
      werror("Couldn't stat %s\n", fs_path);
      continue;
    }
    if(stat->isdir && !tags_in_this_dir)
    {
      if(filename != "Attic")
      {
	  id = md5_56bit(virt_path);
	  if( !directories[rep_id+"|"+id] )
	  {
	      directories[rep_id+"|"+id]=1;
	      db->query( "REPLACE DELAYED INTO cl_dir (id,repository_id,parent_dir_id,path,name) "
			 "VALUES (%d,%d,%d,%s,%s)",
			 id, rep_id, dir_id, virt_path,filename );
	  }
      }
      if(verbosity > 0)
      {
	pbar->set_name("Dir " + (seen_dirs + 1));
	UPDATE_PROGRESSBAR(pbar, 1);
      }
      mtime = max(mtime, traverse_repository(rep_id, cvs_root, module,
					     last_mod, path, 0,
					     progress, increment));
    }
    else if(stat->isreg && has_suffix(filename, ",v"))
    {
      seen_files++;
      virt_path = virt_path[..sizeof(virt_path) - 3];
      id = md5_56bit(virt_path);
      mtime = max(mtime, stat->mtime);
      if(last_mod < stat->mtime)
      {
	index_file(rep_id, dir_id, id, fs_path, virt_path, stat->mtime,
		   tags_in_this_dir, progress, increment);
      }
      else if(verbosity > 0)
      {
	pbar->set_name("File " + seen_files);
	UPDATE_PROGRESSBAR(pbar, 1);
      }
    }
  }
//   if(!has_suffix(fs_dir, "Attic")) {
//       db->query( "UPDATE cl_dir SET modified=%s WHERE repository_id=%d AND path=%s ",
// 		 mtime_to_string(mtime),rep_id,virt_dir);
//   }
  return mtime;
}

//! Erases all of the cl_dir entries in the database that were not present in
//! the repository. (This only occurs when someone has tampered with the raw
//! repository, i e doing rm -rf somewhere.)
//! @param rep_id
//!   The repository we're looking at
//! @param dir_id
//!   Its directory id.
//! @param dir
//!   The path of the directory, relative to the repository root.
//! @param names
//!   Names of any entries in that directory that should not be purged, on the
//!   format "('foo','bar','baz',...)" (names quoted for the database).
//! @param nl
//!   The prefix to print for the next row of output to stderr, if any.
//! @returns
//!   A suitable nl for further iteration.
string purge_deleted_dirs(int rep_id, int dir_id, string dir,
			  string|void names, string|void nl)
{
  array dead_dirs = db->query("SELECT name,id FROM cl_dir "
			      "WHERE repository_id=%d AND parent_dir_id=%d" +
			      (names ? " AND name NOT IN " + names : ""),
			      rep_id, dir_id);
  if(sizeof(dead_dirs))
  {
    if(names && verbosity > 0)
    {
      string plural = sizeof(dead_dirs) == 1 ? "" : "s";
      werror("%s%s:%s, %s dir%s no longer exist%s; purging from db.",
	     nl, get_repository_name(rep_id), dir, plural,
	     String.implode_nicely(dead_dirs->name), plural);
    }
    db->query("DELETE FROM cl_dir WHERE repository_id=%d AND id IN (" +
	      dead_dirs->id * "," + ")", rep_id);
    foreach(dead_dirs, mapping(string:string) dead_dir)
    {
      dir_id = (int)dead_dir->id;
      string dir_name = dir + "/" + dead_dir->name;
      purge_deleted_dirs(rep_id, dir_id, dir_name);
      purge_deleted_files(rep_id, dir_id, dir_name);
    }
    return "\r";
  }
  return nl;
}

//! Like @[purge_deleted_dirs], but drops entries from cl_file.
string purge_deleted_files(int rep_id, int dir_id, string dir,
			   string|void names, string|void nl)
{
  array dead_files = db->query("SELECT id FROM cl_file "
			       "WHERE repository_id=%d AND parent_dir_id=%d" +
			       (names ? " AND name NOT IN " + names : ""),
			       rep_id, dir_id);
  if(sizeof(dead_files))
  {
    if(names && verbosity > 0)
    {
      string plural = sizeof(dead_files) == 1 ? "" : "s";
      werror("%s%s:%s, file%s %s no longer exist%s; purging from db.",
	     nl, get_repository_name(rep_id), dir, plural,
	     String.implode_nicely(dead_files->name), plural);
    }
    db->query("DELETE FROM cl_file WHERE repository_id=%d AND id IN (" +
	      dead_files->id * "," + ")", rep_id);
    return "\r";
  }
  return nl;
}

//! Index the RCS file @[fs_path].
//! @param no_checkins
//!   Don't update the cl_revisions table with the checkins of the
//!   file; just make sure the file exists in the cl_file table and
//!   that all of its tags are updated in cl_tag.
//! @returns
//!   When @[no_checkins] is 0: the number of revisions indexed, otherwise
//!   the number of tags present on the given file.
int index_file(int rep_id, int dir_id, int file_id, string fs_path,
	       string virt_path, int file_mtime, int|void no_checkins,
	       float|void progress, float|void phase_size)
{
  int t = gethrtime();
  string name = basename(fs_path);
//   if(!file)
//   {
//     werror("Couldn't read %s\n", fs_path);
//     return 0;
//   }
//  int qid = file_mtime;
//   string hid = "indexed/"+rep_id+"/"+dir_id+"/"+file_id;
//   if( file_stat( "/tmp/import_cache/"+hid ) )
//   {
//       if( qid == (int)Stdio.read_file( "/tmp/import_cache/"+hid ) )
// 	  return 0;
//   }
//   else
//   {
//       Stdio.mkdirhier( dirname("/tmp/import_cache/"+hid) );
//   }
  Parser.RCS rcs;
  mixed err = catch( rcs = Parser.RCS(fs_path) );
  if( err )
  {
      werror("*********** FAILED TO PARSE "+fs_path+"\n");
      describe_backtrace( err );
      return 0;
  }
//  Stdio.write_file( "/tmp/import_cache/"+hid, (string)qid );

//   file = rcs->parse_admin_section(file); // we now have all tags on this file
//   file = rcs->parse_delta_sections(file); // and needed data on its revisions

  if(no_checkins)
    return sizeof(rcs->tags);

  if(progress && verbosity > 0)
  {
    int n = sizeof(rcs->revisions);
    Tools.Install.ProgressBar pbar;
    string stats = sprintf("%4d %6.2fms: %s", n, (gethrtime() - t)/1e3, fs_path);
    write("\r%s%s\n", stats, " " * max(0, barwidth - sizeof(stats)));
     pbar = Tools.Install.ProgressBar(sprintf("File %d->db", parsed_files),
 				     0, 1, progress + phase_size);
    UPDATE_PROGRESSBAR(pbar, 0);
  }
//   file = 0;
  if( update_checkins(rcs, virt_path, rep_id, dir_id, file_id /*, incremental*/) )
  {
      db->query( "REPLACE DELAYED INTO cl_file (repository_id,parent_dir_id,name,id,modified) "
		 "VALUES (%d,%d,%s,%d,%s)", 
		 rep_id, dir_id, name[..sizeof(name)-3],file_id,mtime_to_string(file_mtime));
      parsed_files++;
  }
  if(!random(20)) gc();
  destruct(rcs);
//   werror("arrays: %O strings: %O objects: %O\n", 
// 	 _memory_usage()->array_bytes, 
// 	 _memory_usage()->string_bytes, 
// 	 _memory_usage()->object_bytes  );
  return 1;
}

int is_merge( string message )
{
  //    return 0;
     message = String.trim_all_whites(message);
     return has_prefix(lower_case(message), "merge from" ) || has_prefix(lower_case(message), "merged from" ) ||
       has_prefix(lower_case(message), "merge branch") ||  has_prefix(lower_case(message), "merged branch");
}

int is_binary( string file )
{
    return binary[lower_case((file/".")[-1])];
}

//! Update the cl_revision table with data from all revisions of @[file].
//! If the global variable @[sync_db] is 1 then all previously stored revisions
//! in the file that are not present any more are purged, with a warning echoed
//! to stderr.
mapping messages;
int update_checkins(Parser.RCS file, string path,
		     int rep_id, int dir_id, int file_id, int|void retry)
{
  int log_id, user_id;
  int num;
  string state, author;
  multiset have_revision = 
      (multiset)db->query( "SELECT revision FROM cl_revision WHERE file_id=%d AND repository_id=%d",file_id, rep_id)->revision;
  foreach(values(file->revisions), Parser.RCS.Revision rev)
  {
     if(!rev->log) 
     {
	 werror("%O is probably truncated\n", path );
	 if( retry < 10 )
	 {
	     werror("Retry in 2 secs\n");
	     call_out( update_checkins, 2.0, file, path, rep_id, dir_id, file_id, retry++ );
	 }
	 else
	 {
	     werror("Giving up\n");
	 }
	 return 0;
     }
      log_id = md5_56bit(rev->log);
      INIT_MESSAGES();
      if( !messages[log_id] )
      {
	  db->query("REPLACE DELAYED INTO cl_checkin_message "
		    "(id,message) VALUES (%d,%s)", log_id, rev->log);
	  messages[log_id]=1;
      }

      if( have_revision[ rev->revision ] )
      {
	  continue;
      }
      num++;
      werror("    "+(path/"/")[-1]+" "+rev->revision+" ["+(rev->branch||"HEAD")+"]\n");
      if(rev->state == "dead")
	  state = "removed";
      else if(!rev->ancestor ||
	      rev->revision == "1.1.1.1" || // is (added==removed==0 better?)
	      file->revisions[rev->ancestor]->state == "dead")
	  state = "added";
      else
	  state = "changed";

      INIT_USERS();
      author = rev->author;
      if(zero_type(user_id = users[author])) // (doc/sqltables.txt for more info)
      {
	  user_id = users[author] = md5_56bit(author);
	  db->query( "REPLACE DELAYED INTO cl_user (id,login) VALUES (%d,%s)", user_id, author );
      }

      if( is_binary( path ) )
      {
	  rev->added = 1;
	  rev->lines = 1;
	  rev->removed = 1;
      }

      if( rev->ancestor )
	  db->query( "REPLACE DELAYED INTO cl_revision "
		     "(repository_id,file_id,revision,path,ancestor,user_id,at,state,branch,"
		     "l_total,l_added,l_removed,message_id,type) "
		     "VALUES (%d,%d,%s,%s,%s,%d,%s,%s,%s,%d,%d,%d,%d,%s)", 
		     rep_id, file_id, rev->revision, path, rev->ancestor,
		     user_id, rev->time->format_time(), 
		     state, rev->branch||"", rev->lines, rev->added, rev->removed, log_id,
		     (is_merge(rev->log)?"merge":"normal"));
      else
	  db->query( "REPLACE DELAYED INTO cl_revision "
		     "(repository_id,file_id,revision,path,user_id,at,state,branch,"
		     "l_total,l_added,l_removed,message_id,type) "
		     "VALUES (%d,%d,%s,%s,%d,%s,%s,%s,%d,%d,%d,%d,%s)", 
		     rep_id, file_id, rev->revision, path,
		     user_id, rev->time->format_time(), 
		     state, rev->branch||"", rev->lines, rev->added, rev->removed, log_id, 
		     (is_merge(rev->log)?"merge":"normal"));
  }
  if(sync_db)
  {
    array dead_revs = db->query("SELECT revision FROM cl_revision "
				"WHERE repository_id=%d AND file_id=%d "
				"AND revision NOT IN ('" +
				map(indices(file->revisions), db->quote)*"','"
				+ "')", rep_id, file_id);
    if(sizeof(dead_revs))
    {
	num += sizeof(dead_revs);
      string plural = sizeof(dead_revs) == 1 ? "" : "s";
      werror("\n%s:%s, revision%s %s no longer exist%s; purging from db.",
	     get_repository_name(rep_id), path, plural,
	     String.implode_nicely(dead_revs), plural);
      db->query("DELETE FROM cl_revision WHERE repository_id=%d "
		"AND file_id=%d AND revision IN ('" +
		map(dead_revs->revision, db->quote) * "','" + "')",
		rep_id, file_id);
      werror("\n");
    }
  }
  return num;
}

static object calendar = Calendar.ISO.set_timezone("UTC");
Calendar.Second parse_time(string time)
{
  return calendar->parse("%Y-%M-%D %h:%m:%s", time);
}

string mtime_to_string(int time_t)
{
  return calendar->Second(time_t)->format_time();
}

