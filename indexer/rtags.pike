#define TDIR  "/var/cvs/web/pchangelog/tags"
object db = /*DBWRAPPER*/(Sql.Sql(getenv("DBURL") || Stdio.read_file(combine_path(__FILE__,"../mysql.url"))-"\n"));

int dirs, files, cutof;
int recursively_fix_tags( string dir, mapping old, string ivpath )
{
    int change;
    string out = "";
    array a = get_dir( dir );
    if( !a )
      {
	werror("**** FAILED TO READ %O\n", dir);
	return 0;
      }
    foreach( a, string x )
    {
	string path = combine_path( dir, x ), vpath=ivpath;

	Stdio.Stat st = file_stat( path );
	if( !st )
	    continue;
	if( st->isdir )
	{
	    dirs++;
	    if( x != "Attic" )
	    {
		vpath=combine_path( vpath, x );
		werror("%O\n", vpath );
	    }
	    change += recursively_fix_tags( combine_path( dir,x ), old, vpath );
	}
	else if( glob( "*,v", x ) )
	{
	    files++;
 	    if( cutof && st->mtime > cutof )
 		continue;
	    int syms;
	    string name, rev;
	    string vv = combine_path(ivpath,x);
	    foreach( Stdio.File( path, "r")->line_iterator(); int n; string l )
	    {
		if( !syms && !has_value( l, "symbols" ) )
		    continue;
		syms=1;
		if( sscanf( l, "\t%s:%s", name, rev )  == 2 )
		{
		    if( rev[-1] == ';' )
		    {
			syms=0;
			rev = rev[..strlen(rev)-2];
		    }
		    out += name+"\0"+vv+"\0"+rev+"\n";
		    if( strlen( out ) > 102400 )
		    {
			write( out );
			out="";
		    }
		    if( !syms )
			break;
		} else if( has_value( l, ";" ) )
		    break;
	    }
	    if( strlen( out ) )
	    {
		write(out);
		out="";
	    }
	}
    }
    return change;
}


void main( int argc, array argv )
{
    array rows = db->query( "SELECT cvsroot_host,cvsroot_path,module FROM cl_repository");
    Stdio.Stat stat = file_stat( "mark" );
    if( stat )
    {
	werror("All tags in files changed since "+stat->mtime+"..\n");
	cutof = stat->mtime;
    }
    foreach( rows, object row )
	if( row->cvsroot_host && row->cvsroot_host != gethostname() )
	    continue;
	else
	    recursively_fix_tags(combine_path( row->cvsroot_path, row->module ),0,row->module);
}
