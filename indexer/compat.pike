// Some compatibility definitions for running the indexer with a pike that
// lacks some of the features who were moved to instead of being local to
// this project. Updates to Parser.RCS are not fixed by this file, though.

#if __MAJOR__>7 || __MAJOR__==7 && (__MINOR__>3 || __MINOR__==3&&__BUILD__>=31)
int barwidth;
#define UPDATE_PROGRESSBAR(PBAR, N) barwidth = PBAR->update(N)
#else // compat; Tools.Install.ProgressBar->update used to be a void function
int barwidth = 74;
#define UPDATE_PROGRESSBAR(PBAR, N) PBAR->update(N)
#endif

#if constant(Stdio.expand_symlinks)
function expand_symlinks = Stdio.expand_symlinks;
#else
//! Unwinds all symlinks along the directory trail @[path], returning
//! a path with no symlink components or 0, in case @[path] does not
//! exist, for instance because one of its links pointed to a
//! nonexistent file or if there was a symlink loop. The returned path
//! is also canonicized/simplified, removing "//", "/./" and the like.
string|int(0..0) expand_symlinks(string path)
{
    return path;
  string unwound, root = has_prefix(path, "/") ? "/" : "";
  mapping(string:Stdio.Stat) seen = ([]);
  while(!seen[path = canonicize_path(path)])
  {
    if(!(seen[path] = file_stat(path, 1)))
      return 0;
    if(seen[path]->islnk)
      path = combine_path(path, System.readlink(path));
    else
    {
      if(unwound)
        unwound = basename(path) + "/" + unwound;
      else
        unwound = basename(path);
      path = dirname(path);
      if(path=="")
        return root + unwound;
    }
  }
  return 0;
}
#endif
