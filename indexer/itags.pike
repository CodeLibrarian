// Only index tags. Faster. The tag database is anything but, though.

#define TDIR  "/var/cvs-doc/tags"
object db = /*DBWRAPPER*/(Sql.Sql(getenv("DBURL") || Stdio.read_file("mysql.url")-"\n"));

mapping tagfiles=([]), tagfiles2=([]);

string rtag_file_name( string directory )
{
    directory = replace(directory, "/", "_slash_");
    if( !strlen( directory ) ) directory = "__empty__";
    return sprintf( "%s/rev/%s/%s", TDIR, directory[0..2], directory );
}

string rlog_file_name( string directory )
{
    directory = replace(directory, "/", "_slash_");
    if( !strlen( directory ) ) directory = "__empty__";
    return sprintf( "%s/log/%s/%s", TDIR, directory[0..2], directory );
}

void save_tag_file( string d )
{
    if( tagfiles[d] )
	tagfiles[d]["\0dirty"]=1;
}

void save_tag_files( mapping tz )
{
    foreach( tz; string tn; mapping z )
    {
	if( m_delete(z,"\0dirty") )
	{
	    werror("Writing files for tag %O\n",  tn );
	    mkdir( dirname(rtag_file_name(tn)) );
	    Stdio.write_file( rtag_file_name(tn), encode_value( z ) );
	}
    }
}

int hit, miss;


mapping get_rtag_file( string d )
{
    mapping t;
    string k = d;
    if( t=tagfiles[k] ) {
	hit++;
	return t;
    }
    if( tagfiles2[k] ) 
    {
	hit++;
	t = tagfiles[k] = m_delete( tagfiles2, k );
	return t;
    }
    miss++;
    if( sizeof( tagfiles ) > 100 )
    {
	save_tag_files( tagfiles2 );
	tagfiles2 = tagfiles;
	tagfiles = ([]);
    }

    t = ([ ]);
    catch( t = decode_value( Stdio.read_file( rtag_file_name(d) ) ) );
    tagfiles[k]=t;
    return t;
}

constant cvsroot = "/var/cvs/cvsroot";

string old_rlog;
Stdio.File rlog;
void append_rlog_file( string tag, string data )
{
    if( !tag ) return;
    if( !rlog || (tag != old_rlog) )
    {
	old_rlog = tag;
	mkdir( dirname(rlog_file_name(tag)) );
	rlog = Stdio.File( rlog_file_name(tag), "wca" );
    }
    rlog->write( data );
}

int do_tailf_file( string file )
{
    mixed err = catch 
    {
	Stdio.File fd = Stdio.File( file, "r" );
	int id = fd->stat()->ino;
	int offset;
	string buffer="";

	while(1)
	{
	    int change;
	    int sz = fd->stat()->size;
	    if( sz > offset )
	    {
		string read;
		read = fd->read( 1024*1024*10 );
		if( !read ) {
		    m_delete(threads,search(threads,this_thread()));
		    return 0;
		}
		werror("Read %d bytes\n", strlen(read));
		if( sz > 100*1024*1024 && file == "log")
		{
		    werror("Creating new tag-file since log is larger than 100Mb\n");
		    mv( file, file+"."+time()+"."+gethrtime()+"."+getpid() );
		    Stdio.File( file, "wc", 0777 );
		    chmod(file,0777);
		    file = 0;
		}
		buffer += read;
		offset += sizeof(read);
		array lines = buffer/"\n";
		buffer = lines[-1];
		lines = lines[..sizeof(lines)-2];
		foreach( lines, string l )
		{
		    string op, file, tag, rev;
		    int uid, timet;
		    if( sscanf( l, "%s\0%s\0%s\0%s\0%d\0%d", op, file, tag, rev, uid, timet) != 6 )
		    {
			uid = 0;
			timet = time(1);
			if( sscanf( l, "%s\0%s\0%s\0%s", op, file, tag, rev) != 4 )
			{
			    werror("Erronous entry in log: %O\n", l );
			    continue;
			}
		    }
		    append_rlog_file( tag,  op+"\0"+file+"\0"+rev+"\0"+uid+"\0"+timet+"\n"  );

		    string a, b;
		    if( sscanf( file, "%s/Attic/%s", a, b ) == 2 )
			file = a + "/" + b;
		    if( !sscanf( file, cvsroot+"/%s", file ) )
			werror("Path not in cvsroot: %O\n", file );
		    else
		    {
			mapping t = get_rtag_file(tag );
			if( op == "del" && t[file] )
			{
			    change++;
			    m_delete(t,file);
			    save_tag_file(tag);
			}
			else if( op == "tag" && t[file] != rev )
			{
			    change++;
			    t[file]=rev;
			    save_tag_file(tag);
			}
		    }
		}
	    }
	    if( change )
	    {
		save_tag_files( tagfiles2 );
		save_tag_files( tagfiles );
		if( !file ) 
		    sleep(2);
	    }
	    else
		sleep(1);
	    if( !file && !change )
		return 0;
	}
    };
    werror("Error in thread: %s\n", describe_backtrace(err));
    m_delete(threads,search(threads,this_thread()));
}

mapping(int:Thread.Thread) threads = ([]);
    

int main( int argc, array argv )
{
    werror("Tagdir: %O\n",argv[1]);
    cd(argv[1]);
    werror("Incremental tag-parser reporting for duty..\n");

    int first_pass=1;
    umask(0);
    while( 1 )
    {
	foreach( get_dir( "." ), string d )
	{
	    Stdio.Stat s = file_stat( d );
	    if( s && s->isreg )
	    {
		if( d=="log" && !threads[s->ino] )
		{
		    werror("New file: %d/%O\n", s->ino, d );
		    threads[s->ino] = thread_create(do_tailf_file, d );
		}
	    }
	}
	first_pass=0;
	sleep(10);
    }
}
