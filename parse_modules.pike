void main(  )
{
    string modules = Stdio.read_file( "/var/cvs/cvsroot/CVSROOT/modules" );
    string current_module_line = "";
    int continuation;
    mapping alias = ([]);
    Sql.Sql db = Sql.Sql( "mysql://root@localhost/code_librarian" );
    foreach( modules/"\n", string x)
    {
	if( !strlen( x ) || x[0] == '#' )
	    continue;
	if( continuation )
	{
	    current_module_line += (x-"&")-"\\";
	    continuation = 0;
	}
	else
	    current_module_line =  (x-"&")-"\\";

	if( x[-1] == '\\' )
	    continuation=1;
	if( !continuation )
	{
	    array tokens = replace(current_module_line,"\t"," ")/" "-({""});
	    if( sizeof(tokens) < 2 )
		continue;
	    if(tokens[1] == "-d")
	    {
		alias[tokens[0]] = tokens[-1];
		continue;
	    }
	    string module = tokens[0];
	    tokens = map( tokens[1..], lambda(string t){return alias[t]||t;});
	    tokens = Array.uniq(column(map( tokens, `/, "/" ),0));
	    int skip = sizeof(tokens)<3 || has_prefix( module, "js") || has_prefix(module,"lth");
	    if( !skip ) 
	    {
	      //		werror("Moduleset '"+module+"': "+tokens*", "+"\n\n");
		foreach( tokens, string token )
		{
 		    db->query( "REPLACE INTO cl_module_sets (modulename,setname) VALUES (%s,%s)",token,
			     module);
		}
	    }
	}
    }
}
