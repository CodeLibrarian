<if not variable='form.start' and sizeof='form.start > 0'><set variable='form.start'>-40 days</set></if>
<if not variable='form.end' and sizeof='form.end > 0'><set variable='form.end'>today</set></if>

<set variable=var.start type=text/plain>NOT SET</set>
<set variable=var.end type=text/plain>NOT SET</set>

<if not variable='form.starttag is yes'>
   <define preparse variable='var.start'><parse-date start=yes>&form.start;</parse-date></define>
   <define preparse variable='var.end'><parse-date>&form.end;</parse-date></define>

   <if not sizeof="var.start > 0">
     <if sizeof="var.end > 0">
        <set variable=var.datewarning>Failed to parse startdate. Setting to today</set> 
        <set variable=form.start>today</set>
        <define preparse variable=var.start><parse-date start=yes>today</parse-date></define>
     </if>
     <else>
        <set variable=var.datewarning>Failed to parse startdate and enddate. Assuming tags</set> 
        <set variable=form.starttag>yes</set>
     </else>
   </if>
   <else>
     <if not sizeof="var.end > 0">
        <set variable=var.datewarning>Failed to parse enddate. Setting to today</set> 
        <set variable=form.end>today</set>
        <define preparse variable=var.end><parse-date>today</parse-date></define>
     </if>
   </else>
</if>

<if variable='form.starttag is yes'>
  <set variable=var.start type=text/plain>fromtag='&form.start;'</set>
  <set variable=var.end type=text/plain>totag='&form.end;'</set>
</if>
<else>
  <set variable=var.start type=text/plain>from='&var.start;'</set>
  <set variable=var.end type=text/plain>to='&var.end;'</set>
<?comment  <set variable=var.datewarning type=text/plain>temp debug: &var.start; - &var.end;</set> ?>
  <define preparse variable='cookie.last_view'><parse-date start=1>now</parse-date></define>
</else>
