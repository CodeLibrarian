<optgroup label="Normal">
        <option value='long.inc'>normal</option>
        <option value='longrevs.inc'>with revisions</option>
        <option value='longjoin.inc'>with join-links</option>
        <option value='verbose.inc'>with diffs</option>
</optgroup><!--
<optgroup label="Compact">
        <option value='short.inc'>normal</option>
        <option value='shortmod.inc'>by module</option>
        <option value='shortfiles.inc'>with files</option>
        <option value='shortmodfiles.inc'>by module, with files</option>
</optgroup>-->
<optgroup label="Changelog">
        <option value='logonly.inc'>normal</option>
        <option value='logonlymod.inc'>by module</option>
        <option value='logonlyfiles.inc'>with files</option>
        <option value='logonlymodfiles.inc'>by module, with files</option>
</optgroup>
<optgroup label="Graphs">
        <option value='graph.inc'>over time</option>
        <option value='active.inc'>active users</option>
        <option value='activemodules.inc'>active modules</option>
        <option value='activebranches.inc'>active branches</option>
        <option value='activemodulebranches.inc'>active module x
        branches</option>
</optgroup>
<optgroup label="Misc">
        <option value='xml.inc'>XML changelog</option>
        <option value='rss.inc'>RSS feed</option>
        <option value='rss09.inc'>RSS 0.9 feed</option>
        <option value='bugsonly.inc'>Buglist only</option>
</optgroup>
