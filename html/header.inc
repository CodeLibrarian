<nooutput>
<set variable=var.mlist></set>
<emit source="values" split="|" values="&form.module;" rowinfo="var.n">
  <if variable="_.counter == 1"><set type=text/html variable='var.nmods'>&var.n:html; 
  <if variable="var.n == 1">module</if><else>modules</else></set></if>
  <set type=text/html variable='var.mlist'>&var.mlist; &_.value:html;</set>
</emit></nooutput>
<title><if expr="&var.n; > 4">Log <if variable="var.branch is ?"></if><else>&var.branch;/</else>&var.nmods;</if>
<else>Log <if variable="var.branch is ?"></if><else>&var.branch;/</else><emit source="values" split="|" values="&form.module;"
rowinfo="var.n"><if variable="_.counter == 1"></if><else><if
expr="&_.counter; == &var.n;"> and </if><else>,
</else></else>&_.value:html;</emit>
</else>
</title>
</head>
<body>
