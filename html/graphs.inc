<set type=text/plain variable='var.width'>&var.formwidth;</set>
<set type=text/plain variable='var.height'>220</set>
<if variable='form.template is graph.inc'>
  <set type=text/plain variable='var.width'>700</set>
  <set type=text/plain variable='var.height'>600</set>
</if>
<commit-graph font='roxen builtin' width='&var.width;' height="&var.height;" style="style/&cookie.cssstyle;">
  <emit branch="&var.branch;" source="commit-graph" ::="&var.filter;">
    <bar name="&_.name;" added="&_.plus;" removed="&_.minus;" normalize-ws="t" href="#&_.date;">
      &_.day;: &_.commits; checkin<if variable="_.commits != 1">s</if>
      (+&_.plus;/-&_.minus;) by <if variable="_.authors = 1">&_.login;</if>
      <else>&_.authors; people</else>
    </bar>
  </emit>
</commit-graph>
