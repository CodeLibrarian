inherit "tagevents.pike";
constant baseevent = "tag";


void find_checkins()
{
    //FIXME: Take URL from one of the global sources.
    Sql.Sql sql = Sql.Sql("mysql://rw@:/home/librarian/roxen/roxen/configurations/_mysql/socket/librarian");
    mapping old_rows = ([]);
    while( 1 )
    {
	array rows = sql->query( "SELECT * from cl_revision order by at desc limit 30" );
	mapping nold_rows = ([]);
	foreach( rows, mapping r )
	{
	    nold_rows[r->path+r->revision]=1;
	    if( !old_rows[r->path+r->revision] )
	    {
		array m = sql->query( "SELECT message from cl_checkin_message where id="+r->message_id );
		array u = sql->query( "SELECT login from cl_user where id="+r->user_id);

		array rep = sql->query( "SELECT name from cl_repository where id="+r->repository_id);
		string message = "";
		string user = "";
		string module = "";
		if( sizeof( m ) ) message = m[0]->message;
		if( sizeof( u ) ) user = u[0]->login;
		if( sizeof( rep ) ) module = rep[0]->name;
//		werror("%O", ({module+"/"+r->path,user,r->branch,r->revision, message}));
		event( module+"/"+r->path,user,r->branch,r->revision, message );
	    }
	}
	old_rows = nold_rows;
	sleep(1);
    }
}

void create()
{
    thread_create( find_checkins );
}

