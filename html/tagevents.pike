array requests = ({});

class Request( Stdio.File fd )
{
    string buffer = "";

    void got_data()  { }

    void write_more()
    {
	if( strlen( buffer ) )
	    buffer = buffer[fd->write(buffer)..];
    }

    System.Timer last_event = System.Timer();
    void event( string ... x )
    {
//  	if( last_event->peek() < 0.001 )
//  	{
//	    call_out(event,0.1, @x);
// 	    return;
// 	}
// 	last_event->get();
	buffer += "Event: message\ndata: "+((x*"\n")/"\n")*"\ndata: "+"\n\n";
	if(catch(write_more()))
	    closed();
    }

    void closed()
    {
	requests -= ({this});
	destruct(this);
    }

    void ping()
    {
	call_out( ping, 10 );
	buffer += "Event: ping\n";
	buffer += "data: "+ctime(time())+"\n";
	if(catch(write_more()))
	    closed();
    }

    void start()
    {
	requests += ({this});
	call_out( ping, 1 );
	fd->set_nonblocking( got_data, write_more, closed );
    }
}



mapping(int:Thread.Thread) threads = ([]);

int do_tailf_file( string file )
{
    mixed err = catch {
    Stdio.File fd = Stdio.File( file, "r" );
    
    int offset = fd->stat()->size;
//     if( Stdio.file_size( "/tmp/"+hash(file)+".mark" )>0 )
//     {
// 	offset = (int)Stdio.read_file( "/tmp/"+hash(file)+".mark" );
// 	werror("Resuming at offset %d\n", offset );
//     }
     fd->seek(offset-10000);
    string buffer="";

    while(1)
    {
	int change;
// 	int sz = fd->stat()->size;
	if( 1 /*sz > offset*/ )
	{
	    string read;
	    read = fd->read( 1024*1024*10 );
	    if( !read ) {
		m_delete(threads,search(threads,this_thread()));
		return 0;
	    }
	    if(!strlen(read))
	    {
		sleep(1);
		continue;
	    }
	    werror("XX Read %d bytes\n", strlen(read));
	    offset += sizeof(read);
	    if( strlen(read) == 1024*1024*10 )
	    {
		buffer = "";
		continue;
	    }
	    buffer += read;
	    array lines = buffer/"\n";
	    buffer = lines[-1];
	    lines = lines[..sizeof(lines)-2];
	    foreach( lines, string l )
	    {
		string op, file, tag, rev;
		if( sscanf( l, "%s\0%s\0%s\0%s", op, file, tag, rev) != 4 )
		    werror("Erronous entry in log: %O\n", l );
		else
		{
		    string a, b;
		    if( sscanf( file, "%s/Attic/%s", a, b ) == 2 )
			file = a + "/" + b;
		    event(op,Roxen.html_encode_string(file-"/import/cvs/taglog"),
			  Roxen.html_encode_string(tag), rev );
		}
	    }
	}
	sleep(0.5);
    }
	};
    werror("Error in thread: %s\n", describe_backtrace(err));
    catch {
	m_delete(threads,search(threads,this_thread()));
    };
}

int first_pass=1;
void find_tags()
{
    while( 1 )
    {
	foreach( get_dir( "/import/cvs/taglog" ), string d )
	{
	    Stdio.Stat s = file_stat( "/import/cvs/taglog/"+d );
	    if( s && s->isreg )
	    {
		if( d=="log" && !glob("*.gz",d) && !threads[s->ino] )
		{
		    werror("New file: %d/%O\n", s->ino, d );
		    threads[s->ino] = thread_create(do_tailf_file, "/import/cvs/taglog/"+d );
		}
	    }
	}
	first_pass=0;
	sleep(10);
    }
}


void create()
{
    werror("Tagevents starting\n");
    thread_create( find_tags );
}
array saved_events = ({});
void event(string ... x)
{
    saved_events = saved_events[sizeof(saved_events)-24..] + ({ x });
    requests->event(@x);
}

mapping parse(RequestID id)
{
    Request r = Request(id->my_fd);
    r->buffer = ( "HTTP/1.0 200 OK\r\n"
		  "Server: Roxen cvs-doc\r\n"
		  "Content-Type: application/x-dom-event-stream\r\n"
		  "\r\n");
    r->start();
    foreach(saved_events, array e )
	r->event( @e );
    return Roxen.http_pipe_in_progress();
}