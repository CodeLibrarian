<if variable='form.save'>
<?comment
 <set variable='cookie.viewcvs' from='form.viewcvs'/>
 <set variable='cookie.deflate' from='form.deflate'/>
?>
 <set variable='cookie.skipinitpage' from='form.skipinitpage'/>
 <set variable='cookie.autoselectdays' from='form.autoselectdays'/>
 <set variable='cookie.formwidth' from='form.formwidth'/>
 <set variable='cookie.formstyle' from='form.formstyle'/>
 <set variable='cookie.cssstyle' from='form.cssstyle'/>
 <set variable='cookie.diff-overflow' from='form.diff-overflow'/>
</if>
<if not variable="page.path is /">
<if not variable="page.path is /overview.html">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Changelog</title>
<if not variable=cookie.cssstyle>
   <set type=text/plain variable='cookie.cssstyle'>per.css</set>
</if>
<link href="style/&cookie.cssstyle;" rel="stylesheet" type="text/css" />
<link rel=icon href="pageicon.png"/>
<div id="content">
<img style='margin-top:-15px;' src=pageicon.png align=right>

<h2>User settings <i>(Note: Needs cookies)</i></h2>
</if>
</if>
<form action="&page.url;" method='POST'>
<table style='border: 1px solid black; padding: 10px; margin: 20px'>
<?comment
   <tr><td>Diff viewer: </td><td>
       <default name=viewcvs value='&cookie.viewcvs;'>
          <select name=viewcvs>
              <option value=0>Piff (pike diff)</option>
              <option value=1>ViewCVS</option>
           </select>
        </default>
</td></tr>?>
   <tr><td>Diff line overflow style: </td><td>
       <default name=diff-overflow value='&cookie.diff-overflow;'>
          <select name='diff-overflow'>
              <option value=scroll>Scrollbars</option>
              <option value=hidden>Hidden</option>
              <option value=visible>Visible</option>
           </select>
        </default>
</td></tr>
<tr><td>
   Show index page: </td><td>
          <default name=skipinitpage  value='&cookie.skipinitpage;'>
               <select name=skipinitpage>
                   <option value=0>Yes</option>
                   <option value=1>Nope</option>
                </select>
           </default>
</td></tr>
<?comment 
<tr><td>
   Compress pages using deflate: </td><td>
          <default name=deflate  value='&cookie.deflate;'>
               <select name=deflate>
                   <option value=1>Yes</option>
                   <option value=0>Nope</option>
                </select>
           </default>
</td></tr>?>
<tr><td>Formstyle:</td><td>
  <if not variable=cookie.formstyle><set variable=cookie.formstyle>right</set></if>
          <default name=formstyle  value='&cookie.formstyle;'>
               <select name=formstyle>
                   <option value=left>Left</option>
                   <option value=top>Top</option>
                   <option value=bottom>Bottom</option>
                   <option value=topbottom>Top and bottom</option>
                   <option value=right>Right</option>
                </select>
           </default> <br/>
</td></tr>
<tr><td>Formwidth:</td><td>
  <if not variable=cookie.formwidth><set variable=cookie.formwidth>175px</set></if>
          <default name=formwidth  value='&cookie.formwidth;'>
               <select name=formwidth>
                   <option value=155px>155px</option>
                   <option value=175px>175px</option>
                   <option value=200px>200px</option>
                   <option value=250px>250px</option>
                   <option value=auto>dynamic</option>
                </select>
           </default> 
           (only for right or left style)
</td></tr>
<tr><td>Stylesheet:</td><td>
  <if not variable='cookie.cssstyle'><set variable='cookie.cssstyle'>per.css</set></if>
          <default name=cssstyle  value='&cookie.cssstyle;'>
               <select name=cssstyle>
               <emit source=dir directory="style/" glob="*.css">
                   <if not variable="_.filename is *-*">
                   <option value='&_.filename;'>&_.filename;</option>
                   </if>
                </emit>
                </select>
           </default> 
</td></tr>

<tr><td>Automatically select modules:</td><td>
          <default name=autoselectdays  value='&cookie.autoselectdays;'>
               <select name=autoselectdays>
                   <option value=0>Don't automatically select modules</option>
                   <option value=1>Checkins last day</option>
                   <option value=5>Checkins last 5 days</option>
                   <option value=30>Checkins last month</option>
                </select>
           </default> 
</td></tr>
  
<tr><td colspan=2 align=right>
     <input type=submit name=save value=' Save settings '/><br />
</td></tr></table><br/>
</form>
<if not variable="page.path is /overview.html">
<if not variable="page.path is /">
<a href='index.html'>Go to changelog</a>
</if>
</if>
</div>
</body>
</html>
