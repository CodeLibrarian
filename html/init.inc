<if not variable='form.template'>
  <if variable='cookie.template'>
   <set variable='form.template' from='cookie.template'/>
  </if>  
</if>
<!-- Normalize the template name in case someone is doning something funny. -->
<set variable='form.template' expr='basename("&form.template:html;")'/>
<if variable='form.otemplate'>
  <set variable='form.template' expr='basename("&form.otemplate:html;")'/>
</if>
<if variable='form.template is xml.inc'><set type=text/plain variable=var.nohtml>1</set></if>
<if variable='form.template is logonly.inc'><set type=text/plain variable=var.nohtml>1</set></if>
<if variable='form.template is logonlyfiles.inc'><set type=text/plain variable=var.nohtml>1</set></if>
<if variable='form.template is logonlymod.inc'><set type=text/plain variable=var.nohtml>1</set></if>
<if variable='form.template is logonlymodfiles.inc'><set type=text/plain variable=var.nohtml>1</set></if>
<if variable='form.template is rss.inc'><set type=text/plain variable=var.nohtml>1</set></if>
<if variable='form.template is rss09.inc'><set type=text/plain variable=var.nohtml>1</set></if>

<if not variable='cookie.cssstyle'>
   <set type=text/plain variable='cookie.cssstyle'>per.css</set>
</if>

<if not variable='cookie.autoselectdays'>
   <set type=text/plain variable='cookie.autoselectdays'>0</set>
</if>

<if variable='cookie.viewcvs'>
  <set variable=form.viewcvs from=cookie.viewcvs/>
</if>
<?comment 
<if variable='form.variables'>
 <sscanf variables=cmodule scope=form format='%*scmodule=%[^|]'>&form.variables:none;</sscanf>
 <sscanf variables=branch scope=form format='%*sbranch=%[^|]'>&form.variables:none;</sscanf>
 <sscanf variables=start scope=form format='%*sstart=%[^|]'>&form.variables:none;</sscanf>
 <sscanf variables=end scope=form format='%*send=%[^|]'>&form.variables:none;</sscanf>
 <sscanf variables=user scope=form format='%*suser=%[^|]'>&form.variables:none;</sscanf>
 <sscanf variables=template scope=form format='%*stemplate=%[^|]'>&form.variables:none;</sscanf>
</if>
?>

<?comment    Clear all values ?>

<if variable="form.Clear is Clear">
  <unset variable="form.module"/>
  <unset variable="form.user"/>
  <unset variable="form.cmodule"/>
  <unset variable="form.m"/>
  <unset variable="form.start"/>
  <unset variable="form.end"/>
  <unset variable="form.starttag"/>
  <unset variable="form.merges"/>
  <unset variable="form.bugs"/>
  <set variable="form.branch" value="*"/>
  <set variable="form.cdelta" value="7"/>
</if>

<?comment    MERGES ?>
<if not variable="form.merges">
  <if variable='cookie.merges'>
    <set variable='form.merges' from='cookie.merges'/>
  </if><else>
    <set variable='form.merges'>on</set>
  </else>
</if>

<if not variable='cookie.merges'>
  <set variable='cookie.merges' from='form.merges'/>
</if>

<?comment    BRANCH ?>
<if not variable="form.branch">
<if variable='cookie.branch'>
   <set variable='form.branch' from='cookie.branch'/>
  </if><else>
   <set variable='form.branch'>*</set>
  </else>
</if>
<if variable='form.branch2'>
  <if not variable="form.branch2 is ">
     <set variable='form.branch'>&form.branch2;</set>
  </if>
</if>
<if variable="form.branch is ">
   <set variable='form.branch'>HEAD</set>
</if>
<if variable="form.branch is HEAD">
   <set variable='var.branch'></set>
</if>
<else>
   <set variable='var.branch'>&form.branch:http;</set>
</else>




<?comment    USER   ?>
<if sizeof="form.user > 1">
   <set type=text/html variable='var.user'>user=&form.user;</set>
</if>



<?comment    MODULE ?>
<if variable="form.cmodule">
  <set variable='form.m' from='form.cmodule'/>
</if>

<if variable="form.M">
   <set variable='form.module' from='form.M'/>
</if>
<else>
  <if variable="form.m">
     <define variable="form.module" preparse="1"><emit source='values' variable='form.m'>&_.value;|</emit></define>
     <sscanf variables="form.module" format="%s||">&form.module;|</sscanf>
  </if>
  <else>
    <if variable='cookie.module'>
      <set variable='form.module' from='cookie.module'/>
    </if>
    <else>
       <define variable=form.module preparse="1"><emit source="repository-list">&_.uriname;|</emit></define>
     </else>
  </else>
</else>

<if variable="form.Save">
  <set variable='cookie.module' from='form.module'/>
  <set variable='cookie.branch' from='form.branch'/>
  <set variable='cookie.template' from='form.template'/>
  <set variable='cookie.merges'   from='form.merges'/>
  <redirect to='index.xml'/>
</if>


<if sizeof="form.module">
  <set variable="var.module" value="module=&form.module;"/>
</if>

<?comment START ?>

<eval><insert file='initdate.inc'/></eval>

<?comment    SAVE ?>

<if not variable='form.template'>  
   <set variable=form.template>long.inc</set>
</if>
<if not variable='form.merges'><set variable='form.merges' value='0'/></if>
<set type=text/html variable='var.filter'>&var.user:none; &var.module:none; &var.start:none; &var.end:none; branch="&var.branch;" <if sizeof="form.message > 0">messageglob="&form.message;"</if> <if sizeof="form.bugs > 0">bugglob="&form.bugs;"</if> <if sizeof="form.path > 0">path="&form.path;"</if><if variable="form.merges is on"> merges=1</if></set>

<!-- explicitly select checkins. When this is done it's the only thing we care about. -->
<if variable='form.checkins'>
  <set type=text/html variable='var.filter'>checkins="&form.checkins;"</set>
</if>

  <if not variable='cookie.graph_unfolded'><set variable='cookie.graph_unfolded'>unfolded</set></if>
  <if not variable='cookie.form_unfolded'><set variable='cookie.form_unfolded'>unfolded</set></if>

  <if not variable=cookie.formwidth><set variable=cookie.formwidth>175px</set></if>
  <if not variable=cookie.formstyle><set variable=cookie.formstyle>right</set></if>
  <if variable="cookie.formwidth is auto">
     <set variable=var.formwidth from=cookie.formwidth/>
  </if>
  <else>
    <sscanf scope=var variables=formwidth format="%d">&cookie.formwidth;</sscanf>
    <set variable=var.formwidth expr='55+&var.formwidth;'/>
    <set variable='var.formwidth2' expr='6+&var.formwidth;'/>
    <set type='text/plain' variable='var.formwidth'>&var.formwidth;px</set>
    <set type='text/plain' variable='var.formwidth2'>&var.formwidth2;px</set>
  </else>

  <if variable="cookie.formstyle is left"><set variable='var.align' from='cookie.formstyle'/></if>
  <if variable="cookie.formstyle is right"><set variable='var.align' from='cookie.formstyle'/></if>
 
<if variable='var.align'>
  <set variable=var.graphtop from='cookie.formstyle'/>
 <unset variable=var.top/>
 <unset variable=var.bottom/>
</if>
<else>
    <if variable="cookie.formstyle is top">
         <set variable=var.graphtop from='cookie.formstyle'/>
         <set variable=var.top from='cookie.formstyle'/>
    </if>
    <if variable="cookie.formstyle is bottom"><set variable=var.bottom from='cookie.formstyle'/></if>
    <if variable="cookie.formstyle is topbottom">
            <set variable=var.graphtop from='cookie.formstyle'/>
            <set variable=var.top from='cookie.formstyle'/>
            <set variable=var.bottom from='cookie.formstyle'/>
    </if>
</else>
<if not variable='form.template is graph.inc'>
   <if not sizeof='form.starttag > 1'>
     <gauge variable='var.graphgauge' resultonly>
        <define preparse variable='var.graph' type='text/html'>
           <eval>
              <insert file='graphs.inc'/>
           </eval>
        </define>
     </gauge>
   </if>
   <else>
     <set variable=var.graph>Graphs not supported for tag logs</set>
     <set variable=var.graphgauge>0.0</set>
   </else>
</if>
