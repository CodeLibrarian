<define variable='var.starttip'>
This specified the start date or tag.
For times you can use relative arguments, such as '-1 day' for  yesterday, or '-1 year', '-1 month' etc for other time ranges (only  day, week, month and year are supported)
Tags can be either branch tags or normal tags. Use 'HEAD' to select  the head branch. The start and end tags must recide on the same  branch, or be direct descendants of eachother.
If the start-tag is a branch the first revision on that branch is  selected.
</define>

<define variable='var.endtip'>
This specified the start date or tag.
For times you can use relative arguments, such as '-1 day' for yesterday, or '-1 year', '-1 month' etc for other time ranges (only day, week, month and year are supported)
Tags can be either branch tags or normal tags. Use 'HEAD' to select the head branch. The start and end tags must recide on the same branch, or be direct descendants of eachother.
If the end-tag is a branch the last revision on that branch is selected.
</define>


<define variable='var.branchtip'>
The branch you want to view. Globs can be used anywhere (only * and ? are supported). As an example, *release* selects all branches with the word 'release'.
You can specify several branches by separating them with ','.
Branchnames are case-sensitive.
</define>

<define variable='var.branchseltip'>This field only includes branches that have checkins in the current log.</define>

<define variable='var.messagetip'>
Only show checkins whose message match the specified glob. As an example, 'google' will search for all messages including google.
Globs can be used anywhere (only * and ? are supported).
Messages are case-insensitive.
</define>

<define variable='var.usertip'>
Select a user to see checkins by the specified user only
</define>

<define variable='var.pathtip'>
Limit the query to only certain paths.  Globs can be used anywhere (only * and ? are supported). An implicit '*' is appended to the path.
Paths are case-sensitive.
</define>

<define variable='var.formattip'>
Select the desired logformat.
</define>

<define variable='var.savetip'>
Saves the currently selected branch and module set as the default for you.
</define>

<define variable='var.cleartip'>
Reset the form to it's default
</define>

<define variable='var.showtip'>
Get a new log matching the selected values
</define>

<define variable='var.permanenttip'>
Create a permanent URL that will always return the results currently visible to the left
</define>