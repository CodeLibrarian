#!/bin/sh

INDEXER_PATH=/var/code_librarian/indexer/indexer.pike
echo "Content-type: text/html"
echo ""
echo "<h1>Indexing $QUERY_module</h1><br />"
cd $(dirname $INDEXER_PATH)
/usr/local/bin/pike $INDEXER_PATH -v  -n $QUERY_module 2>&1  | sed -e s',&,&amp;,g' -e 's/</&lt;/g' -e 's,\(.*\),<tt>\1</tt><br>,'
