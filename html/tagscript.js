document.getElementById("tags").addEventListener("message", tagEventHandler, false);
document.getElementById("checkins").addEventListener("message", checkinEventHandler, false);


document.getElementById("tags").addEventListener("ping", pingTagEventHandler, false);
document.getElementById("checkins").addEventListener("ping", pingCheckinEventHandler, false);


function pingTagEventHandler(event)
{
    document.getElementById( "tagwhen" ).innerText = "Last message "+event.data;
}

function pingCheckinEventHandler(event)
{
    document.getElementById( "checkinwhen" ).innerText = "Last message "+event.data;
}

function tagEventHandler(event)
{
    var data = event.data.split("\n");
    var op = data[0];
    var file = data[1];
    var tag = data[2];
    var rev = data[3];

    var op_ele = document.createElement("td");
    op_ele.appendChild( document.createTextNode(op) );
    op_ele.className = "op";

    var path_ele = document.createElement("td");
    file = file.substring(0,file.length-2);
    file = file.substring("/var/cvs/cvsroot/".length,file.length);
    path_ele.appendChild( document.createTextNode(file) );
    path_ele.className = "path";

    var tag_ele = document.createElement("td");
    tag_ele.appendChild( document.createTextNode(tag) );
    tag_ele.className = "tag";


    var rev_ele = document.createElement("td");
    rev_ele.appendChild( document.createTextNode(rev ? rev : "") );
    rev_ele.className = "rev";

    var line_ele = document.createElement("tr");
     line_ele.appendChild(op_ele);
     line_ele.appendChild(path_ele);
     line_ele.appendChild(tag_ele);
     line_ele.appendChild(rev_ele);
     var cc = document.getElementById("tag-area");
	
     if (cc.childNodes.length > 24) {
	cc.removeChild(cc.firstChild);
     }

     cc.appendChild(line_ele);
     document.body.id = document.body.id; // forces reflow. workaround for reflow bugs
}

function checkinEventHandler(event)
{
    var data = event.data.split("\n");
    var file = data[0];
    var user = data[1];
    var branch = data[2];
    var rev = data[3];
//    alert("Checkin");
    var path_ele = document.createElement("td");
    path_ele.appendChild( document.createTextNode(file) );
    path_ele.className = "path";

    var user_ele = document.createElement("td");
    user_ele.appendChild( document.createTextNode(user) );
    user_ele.className = "user";

    var branch_ele = document.createElement("branch");
    branch_ele.appendChild( document.createTextNode(branch) );
    branch_ele.className = "branch";

    var rev_ele = document.createElement("td");
    rev_ele.appendChild( document.createTextNode(rev ? rev : "") );
    rev_ele.className = "rev";

    var line_ele = document.createElement("tr");
    line_ele.appendChild(user_ele);
    line_ele.appendChild(path_ele);
    line_ele.appendChild(branch_ele);
    line_ele.appendChild(rev_ele);

    var cc = document.getElementById("checkin-area");
    cc.appendChild(line_ele);

    for( var i=4; i<data.length; i++ )
    {	
	line_ele = document.createElement("tr");
	path_ele = document.createElement("td");
        path_ele.className = "message";
	path_ele.colSpan = 4;
	path_ele.appendChild( document.createTextNode( data[i] ) );
	line_ele.appendChild( path_ele );
	cc.appendChild( line_ele );
    }
    while(cc.childNodes.length > 40)
	cc.removeChild(cc.firstChild);

     document.body.id = document.body.id; // forces reflow. workaround for reflow bugs
}
