<h2>Rows changed per user</h2>
<diagram format=png type=sumbars font='franklin gothic demi' fontsize='12'
         bgcolor='#ffffff' textcolor='black' gridcolor='#bbbbbb' vertgrid=1
         horgrid=1 linewidth=3.2 neng=neng height=600 width=600
         border=0>
<colors>#0768B2	#033A80</colors>
<legend>Added	Removed</legend>
<xaxis quantity='User'/>
<yaxis quantity='Lines'/>
<data form='column' xnames xnamesvert><emit  limit=50 source=committers ::="&var.filter;">
&_.login;	&_.added;	&_.removed;</emit>
</data>
</diagram>
<p>
<h2>Percentages</h2>
<diagram bgcolor=white height=600 width=600 format=png scale=0.5
type=pie>
<legend separator=","><emit source=committers ::="&var.filter;" limit='20'>
&_.name; +&_.added;/-&_.removed;,</emit></legend>
<data form='column' xnames><emit source=committers ::="&var.filter;" limit='20'>
&_.login;	&_.total;</emit>
</data>
</p>
