<h2>Warning: Query limited to 100 hits</h2>
<style>
tt { white-space:pre; padding: 10px; margin: 10px;display:block;border: 1px solid black; }
span.change_added {
   font-weight:bold;background: #dfffdf;
}
span.change_removed {
    font-weight:bold;background: #fff2f2;
}
</style>

<h2>Changes with diffs</h2>
<emit maxrows=100 scope=t source="checkins" ::="&var.filter;">
  <sscanf variables="d,t" scope="var" format="%s %s">&_.ended;</sscanf>
  <if variable="var.d != &var.day;"><set variable="var.day" from="var.d"/><a name="&var.d;"><h2>&var.day;</h2></a></if>
  <emit source="checked-in-files" scope="ci">
  <h2>&var.t; (&t.age;) by &t.user; (&t.login;@mail.com)</h2>
   <h2>&_.message:none;</h2>
  <set variable='form.module' value='&ci.module;'/>
  <set variable='form.file' value='&ci.pathraw;'/>
  <set variable='form.from' value='&ci.ancestor;'/>
  <set variable='form.to' value='&ci.revision;'/>
  <set variable='form.inline' value='1'/>
  <insert file='piff.xml'/>
  </emit>
</emit>