<emit format=default source="checkins" ::="&var.filter;" maxrows=500000>&_.alles:none;
<set variable='var.allbugs' from='_.allbugs'/></emit><if sizeof='var.allbugs > 1'>
<h2>Bugs mentioned</h2>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td> &nbsp; </td><td><a href="&_.url;"><img src='&_.icon;'> &_.no;</a></td><td><small>&_.status;</small></td><td>&_.os;</td><td>&_.summary:html;</td></tr>
</emit>
</table>
</if>
