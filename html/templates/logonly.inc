<header name='Content-Type' value='text/plain;charset=utf-8'/><emit source="checkins" ::="&var.filter;" releasenotes='*'>&_.message:none;<set type='text/html' variable='var.allbugs'>&var.allbugs:none;&_.bugfixes:none;</set></emit>

<if sizeof='var.allbugs > 1'>
Bugs mentioned:
==============
<emit source=bugs bugs='&var.allbugs:none;'>
<sprintf format="%-15s %-15s %s" split='
'>&_.no;
&_.status;
&_.summary:none;</sprintf></emit>
</if>
