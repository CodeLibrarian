<pre><emit source="checkins" ::="&var.filter;" sortmodule='yes' releasenotes='*'><if 
    variable='_.module != &var.currentmodule;'><set variable='var.currentmodule' from='_.module'/><if 
       sizeof='var.allbugs > 1'>
  <h3>  Bugs mentioned</h3>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td> &nbsp; </td><td><a href="&_.url;">&_.no;</a></td><td><small>&_.status;</small></td><td>&_.summary:html;</td></tr>
</emit>
</table>

<set variable='var.allbugs' value=""/></if>
<h2>&var.currentmodule;</h2>
</if><emit source="checked-in-files" scope="ci">   &ci.path; (+&ci.added:none;/-&ci.removed:none;)
</emit>     &_.message:html;
<set type='text/html' variable='var.allbugs'>&var.allbugs:none;&_.bugfixes:none;</set></emit>
<if sizeof='var.allbugs > 1'>
  <h3>  Bugs mentioned</h3>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td> &nbsp; </td><td><a href=&_.url;>&_.no;</a></td><td><small>&_.status;</small></td><td>&_.summary:html;</td></tr>
</emit>
</table>
