<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="style/changelog-xml.css" type="text/css"?>
<header name='Content-Type' value='text/xml'/>

<changelog xmlns="http://www.red-bean.com/xmlns/cvs2cl/">
<emit source="checkins" ::="&var.filter;" scope=top>
<sscanf variables="d,t" scope="var" format="%s %s">&top.ended;</sscanf>
<entry>
    <maketag type=container name='date'>&var.d;</maketag>
    <time>&var.t;</time>
    <isoDate>&var.d;T&var.t;</isoDate>
    <author>&top.login;</author> <?comment FIXME: Use userinfo?>
<emit source="checked-in-files" scope="ci">    <file>
       <name>&ci.path;</name>
       <revision>&ci.revision;</revision>
       <if not variable="ci.branch is "><branch>&ci.branch;</branch></if>
<?comment <emit revision='&ci.revision;' source="tags" scope="tag" path='&ci.path;' module='&ci.module;'>
	<tag>&tag.name;</tag></emit>?>    </file>
    </emit>    <msg>&top.raw_message:html;</msg>
</entry>
</emit>

</changelog>