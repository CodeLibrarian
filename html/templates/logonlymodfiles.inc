<header name='Content-Type' value='text/plain;charset=utf-8'/><emit source="checkins" ::="&var.filter;" sortmodule='yes' releasenotes='*'><if 
    variable='_.module != &var.currentmodule;'><set variable='var.currentmodule' from='_.module'/><if 
       sizeof='var.allbugs > 1'>
  Bugs mentioned
  ==============<emit source='bugs' bugs='&var.allbugs:none;'>
  &_.no;: &_.status; &_.summary:none;</emit>

<set variable='var.allbugs' value=""/></if>
&var.currentmodule;
<emit source=values variable=var.currentmodule split="">=</emit>
</if><emit source="checked-in-files" scope="ci">   &ci.path; (+&ci.added:none;/-&ci.removed:none;)
</emit>     &_.message:none;
<set type='text/html' variable='var.allbugs'>&var.allbugs:none;&_.bugfixes:none;</set></emit>
<if sizeof='var.allbugs > 1'>
  Bugs mentioned:
  ==============
  <emit source=bugs bugs='&var.allbugs:none;'>
  &_.no;: &_.status; &_.summary:none;</emit></if>
