<h2>Changelog</h2>
<pre><emit source="checkins" ::="&var.filter;" releasenotes='*'><emit source="checked-in-files" scope="ci">&ci.module;/&ci.path; (+&ci.added:none;/-&ci.removed:none;)
</emit>   &_.message:html;
<set type='text/html' variable='var.allbugs'>&var.allbugs:none;&_.bugfixes:none;</set></emit>

<if sizeof='var.allbugs > 1'>
<h2>Bugs mentioned:</h2>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td><a href=&_.url;>&_.no;</a></td><td><small>&_.status;</small></td><td>&_.summary:html;</td></tr>
</emit>
</table>
</if>
