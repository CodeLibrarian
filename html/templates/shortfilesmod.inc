<pre><emit source="checkins" ::="&var.filter;" sortmodule='yes' releasenotes='*'><if 
    variable='_.module != &var.currentmodule;'><set variable='var.currentmodule' from='_.module'/><if 
       sizeof='var.allbugs > 1'>
  <h2>Bugs mentioned</h2>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td><a href=&_.url;>&_.no;</a></td><td><small>&_.status;</small></td><td>&_.summary;</td></tr>
</emit>
</table>

<set variable='var.allbugs' value=""/></if>
<h1>&var.currentmodule;</h1>
</if><emit source="checked-in-files" scope="ci">   &ci.path; (+&ci.added:none;/-&ci.removed:none;)
</emit>     &_.message:none;
<set type='text/html' variable='var.allbugs'>&var.allbugs:none;&_.bugfixes:none;</set></emit>
<if sizeof='var.allbugs > 1'>
  <h2>Bugs mentioned</h2>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td><a href=&_.url;>&_.no;</a></td><td><small>&_.status;</small></td><td>&_.summary;</td></tr>
</emit>
</table>
