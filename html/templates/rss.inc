<?xml version="1.0"?><header name='Content-Type' value='application/xml;charset=utf-8'/><define variable="var.mlist" preparse="1"><replace from="|" to=" ">&form.module;</replace></define>
<rdf:RDF
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns="http://purl.org/rss/1.0/"
 xml:lang="en">
<channel rdf:about="https://cvs/cvs/index.xml?template=rss.inc">
  <title>ChangeLog - &var.mlist; - &form.branch;</title>
  <link>https://cvs/changelog/</link>
  <description>ChangeLog - &var.mlist; - &form.branch;</description>
  <dc:publisher></dc:publisher>
  <dc:creator>Per Hedbor &lt;ph@mail.com&gt;</dc:creator>
  <dc:date><date type='iso'/></dc:date>
  <image rdf:resource="https://cvs/changelog/logo.gif"  />

  <items>
   <rdf:Seq>
<emit scope=tl source="checkins" ::="&var.filter;">
  <emit source="checked-in-files" scope="ci">
    <if variable='cookie.viewcvs'>
      <rdf:li resource='https://cvs/viewcvs/viewvc.cgi/&ci.module;/&ci.pathraw;.diff?r1=&ci.ancestor;&amp;r2=&ci.revision;' />
    </if>
    <else>
      <rdf:li resource='https://cvs/cvs/piff.xml?module=&ci.module;&amp;file=&ci.pathraw;&amp;from=&ci.ancestor;&amp;to=&ci.revision;' />
    </else>
  </emit>
</emit>
   </rdf:Seq>
  </items>
</channel>
<emit scope=tl source="checkins" ::="&var.filter;">
  <emit source="checked-in-files" scope="ci">
  <if variable='cookie.viewcvs'>
    <item rdf:about='https://cvs/viewcvs/viewvc.cgi/&ci.module;/&ci.pathraw;.diff?r1=&ci.ancestor;&amp;r2=&ci.revision;'>
  </if>
  <else>
    <item rdf:about='https://cvs/cvs/piff.xml?module=&ci.module;&amp;file=&ci.pathraw;&amp;from=&ci.ancestor;&amp;to=&ci.revision;'>
  </else>
  <title>&tl.user; &lt;&tl.login;@mail.com&gt;</title>
  <if variable='cookie.viewcvs'>
    <link>https://cvs/viewcvs/viewvc.cgi/&ci.module;/&ci.pathraw;.diff?r1=&ci.ancestor;&amp;r2=&ci.revision;</link>
  </if>
  <else>
    <link>https://cvs/cvs/piff.xml?module=&ci.module;&amp;file=&ci.pathraw;&amp;from=&ci.ancestor;&amp;to=&ci.revision;</link>
  </else>
  <description>&ci.module;/&ci.path; (+&ci.added:none;/-&ci.removed:none;)
<if sizeof='&ci.branch; > 0'>[&ci.branch;]</if>

&tl.raw_message:html;
</description>
  <dc:creator>&tl.user; &lt;&tl.login;@mail.com&gt;</dc:creator>
  <dc:date>&tl.ended;</dc:date>
 </item>
</emit>
</emit>


</rdf:RDF>
