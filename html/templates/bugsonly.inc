<emit source="checkins" ::="&var.filter;" releasenotes='*'><set type='text/html' variable='var.allbugs'>&var.allbugs:none;&_.bugfixes:none;</set></emit><if sizeof='var.allbugs > 1'>
<h2>Bugs mentioned</h2>
<table>
<emit source=bugs bugs='&var.allbugs:none;'>
<tr><td> &nbsp; </td><td><a href="&_.url;"><img src='&_.icon;'> &_.no;</a></td><td><small>&_.status;</small></td><td>&_.os;</td><td>&_.summary:html;</td></tr>
</emit>
</table>
</if>
