<?xml version="1.0"?><header name='Content-Type' value='application/xml;charset=iso-8859-1'/><define variable="var.mlist" preparse="1"><replace from="|" to=" ">&form.module;</replace></define>
<rss version="0.91">
  <channel>
    <title>ChangeLog - &var.mlist; - &form.branch;</title>
    <link>https://cvs/changelog/</link>
    <description>ChangeLog - &var.mlist; - &form.branch;</description>
    <pubDate><date type='iso'/></pubDate>
    <language>en-US</language>
    <image>
       <url>https://cvs/changelog/logo.gif</url>
       <link>https://cvs/changelog/</link>
    </image>

   <emit scope=tl source="checkins" ::="&var.filter;">
      <emit source="checked-in-files" scope="ci">
       <item>
           <title>&tl.user; &lt;&tl.login;@mail.com&gt;</title>
           <link>piff.xml?module=&ci.module;&amp;file=&ci.pathraw;&amp;from=&ci.ancestor;&amp;to=&ci.revision;</link>
         <description>&ci.module;/&ci.path; (+&ci.added:none;/-&ci.removed:none;)
<if sizeof='&ci.branch; > 0'>[&ci.branch;]</if>

&tl.raw_message:html;
</description>
  <creator>&tl.user; &lt;&tl.login;@mail.com&gt;</creator>
  <maketag type='container' name='date'>&tl.ended;</maketag>
     </item>
  </emit>
</emit>
</channel>
</rss>
