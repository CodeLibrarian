#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_PROVIDER;
constant module_name = "CVS Viewer: JIRA issue fetcher";

#define JIRA_USER query("jira user")
#define JIRA_PASSWORD query("jira password")
//FIXME: Unsafe on multi-user installations
#define JIRA_CACHE "/tmp/cached_bugs_jira.dat"

string|array(string) query_provides()
{
    return "bugs";
}

Thread.Queue update_queue = Thread.Queue();

string status() 
{
    string res = sprintf( "<b>Bugs:</b> %d<br>", sizeof(cached_bugs) );
}

mapping(string:function) query_action_buttons() 
{
    return ([ "Delete bug cache":delete_bug_cache ]);
}

void delete_bug_cache()
{
    cached_bugs = ([]);
    save_bugs();
}

void update_bugs(  )
{
    catch
    {
        while( string x = update_queue->read() )
        {
            if( !(<"Resolved","Accepted","Verified","Inactive">)[cached_bugs[x]->status] )
            {
                werror("Checking if %O is fixed .. ", x );
                cached_bugs[x] = get_issue( x );
                if( !(<"Resolved","Accepted","Verified","Inactive">)[cached_bugs[x]->status] )
                {
                    werror("still not fixed.\n");
                    update_queue->write( x );
                }
                else
                    werror("now fixed.\n");
                sleep( 10 + random(5) );
            }
        }
    };
}

void save_bugs()
{
    Stdio.write_file( JIRA_CACHE, encode_value( cached_bugs ) );
}

mapping cached_bugs = ([]);

mapping status_cache;
mapping get_statuses()
{
    if( status_cache )
        return status_cache;

    call_out( lambda(){status_cache=0;},100);
    Protocols.XMLRPC.Client remote = Protocols.XMLRPC.Client( query("xmlrpc url") );
    string token;
    catch(token = remote["jira1.login"]( JIRA_USER, JIRA_PASSWORD )[0]);
    array status =  remote["jira1.getStatuses"]( token )[0];
    if( objectp( status ) )
        return ([]);
    return status_cache = mkmapping( status->id, status );
}

multiset cached_project_list;
multiset get_list_of_projects()
{
    if( cached_project_list ) return cached_project_list;
    Protocols.XMLRPC.Client remote = Protocols.XMLRPC.Client( query("xmlrpc url") );
    string token;
    catch(token = remote["jira1.login"]( JIRA_USER, JIRA_PASSWORD )[0]);
    if( !token ) {
        werror("no login\n");
        return 0;
    }
    array status = Array.flatten(remote["jira1.getProjectsNoSchemes"]( token ));

    if( !sizeof( status ) )
    {
        werror("no status\n");
        return 0;
    }
    remote["jira1.logout"]( token );
    call_out( lambda() { cached_project_list = 0; }, 1200 );
    return cached_project_list = (multiset)status->key;
}

void create()
{
    defvar("xmlrpc url", "", "Jira XMLRPC URL", 
           TYPE_URL|VAR_INITIAL, "Jira XMLRPC URL" );
    defvar("jira user", "**", "JIRA username", 
           TYPE_STRING|VAR_INITIAL, "" );
    defvar("jira password", "**", "JIRA password", 
           TYPE_STRING|VAR_INITIAL, "" );
    defvar("numerical bugs", 1, "Search for numerical bugid's", 
           TYPE_FLAG, "" );
    defvar("bugs", "https://bugs.internal/show_bug.cgi?id=%s",
           "Bugdatabase URL", TYPE_STRING,
           "%s will be replaced with the bug id" );

}

// Provider API. Returns the bug-id for a given word, if any.
string is_bug( string word )
{
    string pre, post;

    if( query("numerical bugs") )
    {
        int id;
        if( sscanf( word, "#%d",id ) ) return (string)id;
        if( (int)word && (int)word > 10000 && (int)word < 20000000)
            return (string)(int)word;
    }

    if( sscanf( word, "%[A-z0-9]-%[0-9]", pre, post) == 2 && strlen(post))
    {
        while( strlen(word) && 
               (word[-1] == '.' || word[-1] == ']' || word[-1] == ')') )
            word = word[..strlen(word)-2];
        // some common cases..
        while( strlen(pre) && pre[0] == '#' )
            pre = pre[1..];
      
        if( upper_case( pre ) != pre && lower_case(pre) != pre )
            return 0;


        if( multiset match = get_list_of_projects() )
            return match[upper_case(pre)] ? pre+"-"+post: 0;

        // --BEGIN-OPERA-INTERNAL--
        if( strlen( word ) > strlen(pre)+strlen(post)+2 )//  block core-2.2 etc
            return 0;
        if( pre == "core" ) 
            return 0;
        if( (<"CORE-1","CORE-2","CORE-3","CORE-4">)[pre + "-" + post] ) 
            return 0;
        // --END-OPERA-INTERNAL--

        return pre+"-"+post; // fallback when jira is down.   
    }
    return 0;
}

mapping get_bug( string x )
{
    if( cached_bugs[x] )
        return cached_bugs[x];
    System.Timer t = System.Timer();
    werror("[fetching "+x+" ..");
    cached_bugs[x] = get_issue( x );
    werror(" %.1fs]", t->get());
    update_queue->write( x );
    remove_call_out( save_bugs );
    call_out( save_bugs, 10 );
    return cached_bugs[x];
}

mapping get_issue( string id )
{
    if( !id || !strlen(id))
    {
        return ([
            "icon":0,
            "status":"?",
            "summary":0,
        ]);
    }

    Protocols.XMLRPC.Client remote = Protocols.XMLRPC.Client( query("xmlrpc url") );
    string token;
    catch(token = remote["jira1.login"]( JIRA_USER, JIRA_PASSWORD )[0]);
    if( !token )
    {
        return ([
            "no":id,
            "icon":0,
            "status":"?",
            "summary":"Login to JIRA failed",
        ]);
    }
    mapping status = get_statuses();
    array(mapping) _bug =  remote["jira1.getIssue"]( token, id );
    remote["jira1.logout"](token);

    mapping bug;
    if( objectp(_bug)) 
    {
        return ([
            "no":id,
            "url":sprintf(query("bugs"),id),
            "icon":"",
            "status":"?",
            "summary":0,//sprintf("Failed to get issue %O (%O)",id,_bug?_bug:"Failed to read bugdata"),
        ]);
    }

    bug = _bug[-1];
      
    mapping st = status[bug->status];
    if( !st )
        st = (["name":"unknown","icon":"/roxen-internal-unit"]);

    return ([
        "no":id,
        "url":sprintf(query("bugs"),id),
        "icon":st->icon,
        "status":st->name,
        "summary":String.trim_all_whites(bug->summary)
    ]);
}


void start( void|int num, void|Configuration conf )
{
    destruct( update_queue );
    update_queue = Thread.Queue();

    if( conf )
    {
        thread_create( update_bugs );
        catch(cached_bugs = decode_value(Stdio.read_file( JIRA_CACHE )));
        foreach( cached_bugs; string key; mapping bug )
            update_queue->write( key );
        werror("%O cached bugs\n", sizeof(cached_bugs));
    }
}
