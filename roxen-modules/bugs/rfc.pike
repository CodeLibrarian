#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_PROVIDER;
constant module_name = "CVS Viewer: RFC reference fetcher";

//FIXME: RFC lookup is blocking (AFAIC). That doesn't really matter
//       when the cache is hot, but it's going to act up sooner or later.

//FIXME: Unsafe on multi-user installations
#define RFC_CACHE "/tmp/cached_bugs_rfc.dat"

string|array(string) query_provides()
{
    return "bugs";
}

string status()
{
    string res = sprintf( "<b>RFCs:</b> %d<br>", sizeof(cached_rfcs) );
}

mapping(string:function) query_action_buttons()
{
    return ([ "Delete rfc cache":delete_rfc_cache ]);
}

void delete_rfc_cache()
{
    cached_rfcs = ([]);
    save_rfcs();
}

void save_rfcs()
{
    Stdio.write_file( RFC_CACHE, encode_value( cached_rfcs ) );
}

mapping cached_rfcs = ([]);

void create()
{
}

// Provider API. Returns the bug-id for a given word, if any.
string is_bug( string word, string|void prev_word )
{
  word = upper_case(word);
  if (!has_prefix(word, "RFC")) {
    if (upper_case(prev_word||"") == "RFC") {
      word = "RFC" + word;
    } else {
      return 0;
    }
  }

  word = replace(word, ({ "-", "#" }), ({ "", "" }));
  int no;
  if (sscanf(word, "RFC%d", no) && no) {
    return sprintf("RFC%04d", no);
  }
  return 0;
}

mapping get_bug( string x )
{
    if( cached_rfcs[x] )
        return cached_rfcs[x];
    System.Timer t = System.Timer();
    werror("[fetching "+x+" ..");
    cached_rfcs[x] = get_rfc( x );
    werror(" %.1fs]", t->get());
    remove_call_out( save_rfcs );
    call_out( save_rfcs, 10 );
    return cached_rfcs[x];
}

string get_rfc_url(string rfc)
{
  int no = (int)rfc[3..];
  return sprintf("http://pike.lysator.liu.se/docs/ietf/rfc/%02d/rfc%04d.xml",
		 no/100, no);
}

string raw_get_url(string url)
{
    object req = Protocols.HTTP.get_url(url);
    if( req->status != 200 )
    {
        werror("CL Rfc: Failed (%O) to load URL %O.\n", req->status, url);
        return 0;
    }
    return req->unicode_data();
}

protected constant SimpleNode = Parser.XML.Tree.SimpleNode;

protected SimpleNode find_node(SimpleNode node, string path)
{
  //  Just get first child that matches the given path
  foreach((path / "/") - ({ "" }), string segment) {
    array(string) tags = node->get_children()->get_tag_name();
    int pos = search(tags, segment);
    if (pos >= 0)
      node = node[pos];
    else
      return 0;
  }
  return node;
}

array(SimpleNode) find_nodes(SimpleNode node, string|array path)
{
  //  Find all children at each level that match each path segment. When
  //  we are called recursively the path will already be in array form.
  array(string) segments = arrayp(path) ? path : (path / "/") - ({ "" });

  //  Always return an array even if no matches are found
  array(SimpleNode) res = ({ });

  if (sizeof(segments)) {
    res = filter(node->get_children(),
                 lambda(SimpleNode n) {
                   return
                     (n->get_node_type() == Parser.XML.Tree.XML_ELEMENT) &&
                     (< n->get_tag_name(), "*" >)[segments[0]];
                 });

    //  If we've got any result and there are additional path segments
    //  left to process we run them all in parallel.
    if (sizeof(res) && sizeof(segments) > 1)
      res = Array.flatten(map(res, find_nodes, segments[1..]));
  }
  return res;
}

protected string find_string_val(SimpleNode node, string path)
{
  //  If path ends with attribute we stop one level above
  if (has_value(path, "@")) {
    [path, string attr] = path / "@";
    node = find_node(node, path);
    return node && node->get_attributes()[attr];
  } else {
    node = find_node(node, path);
    return node && node->value_of_node();
  }
}

mapping get_rfc( string id )
{
  mapping(string:string) rfc = ([
    "no":id,
    "icon":"img/IETF_Logo-32x17.png",
    "status":"INFORMATIONAL",
    "url":get_rfc_url(id),
    "summary":id,
  ]);
  if( !id || !strlen(id) || lower_case(id) > "rfc99999" ) {
    return rfc;
  }

  string rfc_html = raw_get_url(rfc->url);
  if (!rfc_html) return rfc;

  string title;
  string status;
  array(string) frags = rfc_html/"<";

  ADT.Stack stack = ADT.Stack();
  foreach(frags; int i; string frag) {
    if (has_prefix(frag, "/")) {
      if (sizeof(stack)) stack->pop();
      continue;
    }
    array(string) a = frag/">";
    string tag = a[0];
    string content = a[1..] * ">";
    a = replace(tag, ({ "\t", "\n" }), ({ " ", " " })) / " ";
    string name = a[0];
    mapping(string:string) entry = ([
      "<name>":a[0],
    ]);
    foreach(a[1..], string val) {
      array(string) b = val/"=";
      if (sizeof(b) == 2) {
	entry[b[0]] = b[1];
      }
    }
    switch(name) {
    case "title":
      // html/head/title
      if (!title) {
	// Strip the " - Pike Programming Language" suffix.
	array(string) segments = content/" - ";
	if (sizeof(segments) > 1) {
	  content = segments[..sizeof(segments)-2] * " - ";
	}
	title = content;
      }
      break;
    case "strong":
      // html/body/div/div/div/div/div/aside/div/div[class='status']/strong
      if (status) break;
      if ((stack->top()["<name>"] == "div") &&
	  (<"\"status\"", "'status'", "status">)[stack->top()["class"]]) {
	status = content;
      }
      break;
    }
    stack->push(entry);
  }

  if (title && sizeof(title)) rfc->summary = title;
  if (status && sizeof(status)) rfc->status = status;

  return rfc;
}


void start( void|int num, void|Configuration conf )
{
    if( conf )
    {
        catch(cached_rfcs = decode_value(Stdio.read_file( RFC_CACHE )));
        werror("%O cached rfcs\n", sizeof(cached_rfcs));
    }
}
