#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_PROVIDER;
constant module_name = "CVS Viewer: GitLab issue fetcher";

#define GITLAB_TOKEN query("gitlab_token")
//FIXME: Unsafe on multi-user installations
#define GITLAB_CACHE "/tmp/cached_bugs_gitlab.dat"

string|array(string) query_provides()
{
    return "bugs";
}

Thread.Queue update_queue = Thread.Queue();

Standards.URI get_api_url(string path)
{
  return Standards.URI(sprintf("/api/v4/projects/%d/%s",
			       query("project_id"),
			       path),
		       query("gitlab_url"));
}

string status()
{
    string res = sprintf( "<b>Bugs:</b> %d<br>", sizeof(cached_bugs) );
}

mapping(string:function) query_action_buttons()
{
    return ([ "Delete bug cache":delete_bug_cache ]);
}

void delete_bug_cache()
{
    cached_bugs = ([]);
    save_bugs();
}

void update_bugs(  )
{
    catch
    {
        while( string x = update_queue->read() )
        {
            if( !(<"CLOSED">)[cached_bugs[x]->status] )
            {
                werror("Checking if %O is fixed .. ", x );
                cached_bugs[x] = get_issue( x );
                if( !(<"CLOSED">)[cached_bugs[x]->status] )
                {
                    werror("still not fixed.\n");
                    update_queue->write( x );
                }
                else
                    werror("now fixed.\n");
                sleep( 10 + random(5) );
            }
        }
    };
}

void save_bugs()
{
    Stdio.write_file( GITLAB_CACHE, encode_value( cached_bugs ) );
}

mapping cached_bugs = ([]);

void create()
{
  defvar("gitlab_url", "https://git.lysator.liu.se/", "GitLab URL",
	 TYPE_URL|VAR_INITIAL, "" );
  defvar("gitlab_token", "", "GitLab access token",
	 TYPE_STRING|VAR_INITIAL, "" );
  defvar("numerical_bugs", 1, "Search for numerical bugids",
	 TYPE_FLAG, "" );
  defvar("project_id", 636, "GitLab project id.", TYPE_INT, "");
}

// Provider API. Returns the bug-id for a given word, if any.
string is_bug( string word, string|void prev_word )
{
    string pre, post;
    int id;
    if( sscanf( word, "#%d", id ) ) return "#" + id;

    id = (int)word;

    if (id) {
      if( query("numerical_bugs") )
      {
	if (id > 10000 && id < 20000000)
	  return "#" + id;
      }

      do {
	if (prev_word) {
	  if (upper_case(prev_word) == "PIKE") {
	    // Many false positives for stuff like
	    // Pike 8.0 and Pike 7.8.866.
	    return 0;
	    word = "PIKE-" + word;
	    break;
	  } else if (lower_case(prev_word) == "bug") {
	    // Bugzilla bug.
	    return "#" + id;
	  } else if (lower_case(prev_word) == "pikelangpike") {
	    return "#" + id;
	  }
	}
	return 0;
      } while(0);
    }

    if( sscanf( word, "PIKE-%[0-9]", post) && strlen(post))
    {
	// YouTrack PIKE-XXXX issues have been offset by 8000 in GitLab.
	return "#" + (8000 + (int)post);
    }

    if( sscanf( word, "pike#%[0-9]", post) && strlen(post))
    {
      // pike#XXXX
      return "#" + (int)post;
    }

    if( sscanf( word, "pikelangpike#%[0-9]", post) && strlen(post))
    {
      // pikelang/pike#XXXX
      return "#" + (int)post;
    }

    return 0;
}

mapping get_bug( string x )
{
    if( cached_bugs[x] )
        return cached_bugs[x];
    System.Timer t = System.Timer();
    werror("[fetching "+x+" ..");
    cached_bugs[x] = get_issue( x );
    werror(" %.1fs]", t->get());
    update_queue->write( x );
    remove_call_out( save_bugs );
    call_out( save_bugs, 10 );
    return cached_bugs[x];
}

mapping get_issue( string id )
{
  if (has_prefix(id || "", "#")) id = id[1..];

  if( !id || !strlen(id))
  {
    return ([
      "icon":0,
      "status":"?",
      "summary":0,
    ]);
  }

  do {
    // FIXME: Cache the connection and use keep-alive.
    Protocols.HTTP.Query q;

    mixed err;
    if (err = catch {
	q=
	  Protocols.HTTP.get_url(get_api_url("issues"),
				 ([ "iids[]": id ]),
				 ([ "PRIVATE-TOKEN": query("gitlab_token") ]));
      }) {
      werror("Failed to fetch GitLab issue #%s:\n"
	     "%s\n",
	     id,
	     describe_backtrace(err));
      break;
    }
    if (!q() || (q->status >= 300)) {
      werror("Bad status for GitLab issue #%s: %O\n",
	     id, q->status);
      break;
    }

    array(mapping(string:mixed)) issues =
      Standards.JSON.decode(q->unicode_data());

    // NB: For non-existing issues the empty mapping is received.
    if (!arrayp(issues) || !sizeof(issues)) {
      werror("No information for GitLab issue #%s: %O\n",
	     id, issues);
      break;
    }

    mapping(string:mixed) issue = issues[0];

    string state = "OPEN";
    if (issue->state == "closed") {
      state = "CLOSED";
    } else {
      // Look at the labels.
      multiset(string) labels = (multiset)(issue->labels || ({}));
      foreach(({
		({ "To Do: Triage", "TRIAGE" }),
		({ "Doing", "INPROGRESS" }),
		({ "To Do: QA", "QA" }),
		({ "To Do: Change Log", "CHANGES" }),
	      }), [ string l, string s ]) {
	if (labels[l]) {
	  state = s;
	  break;
	}
      }
    }

    return ([
      "no":"#" + id,
      "url":issue->web_url ||
      (string)Standards.URI(sprintf("/pikelang/pike/issues/%d", (int)id),
			    query("gitlab_url")),
      "icon":"bugzillastatus/" + state + ".gif",
      "status":state,
      "summary":String.trim_all_whites(issue->title),
    ]);
  } while(0);

  return ([
    "no":"#" + id,
    "url": (string)Standards.URI(sprintf("/pikelang/pike/issues/%d", (int)id),
				 query("gitlab_url")),
    "icon":0,
    "status":"?",
    "summary":"Failed to fetch issue from GitLab.",
  ]);
}

void start( void|int num, void|Configuration conf )
{
    destruct( update_queue );
    update_queue = Thread.Queue();

    if( conf )
    {
        thread_create( update_bugs );
        catch(cached_bugs = decode_value(Stdio.read_file( GITLAB_CACHE )));
        foreach( cached_bugs; string key; mapping bug )
            update_queue->write( key );
        werror("%O cached bugs\n", sizeof(cached_bugs));
    }
}
