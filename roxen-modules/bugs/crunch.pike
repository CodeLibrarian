#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_PROVIDER;
constant module_name = "CVS Viewer: Crunch issue fetcher";

//FIXME: bug lookup is blocking (AFAIC). That doesn't really matter
//       when the cache is hot, but it's going to act up sooner or later.

#define CRUNCH_USER query("crunch user")
#define CRUNCH_PASSWORD query("crunch password")
//FIXME: Unsafe on multi-user installations
#define CRUNCH_CACHE "/tmp/cached_bugs_crunch.dat"

string|array(string) query_provides()
{
    return "bugs";
}

Thread.Queue update_queue = Thread.Queue();

string status() 
{
    string res = sprintf( "<b>Bugs:</b> %d<br>", sizeof(cached_bugs) );
}

mapping(string:function) query_action_buttons() 
{
    return ([ "Delete bug cache":delete_bug_cache ]);
}

void delete_bug_cache()
{
    cached_bugs = ([]);
    save_bugs();
}

//FIXME: Needs updating for Bugzilla
void update_bugs(  )
{
    werror("CL Crunch: update\n");

    catch
    {
        while( string x = update_queue->read() )
        {
            if( !(<"RESOLVED", "VERIFIED", "CLOSED">)[cached_bugs[x]->status] )
            {
                werror("Checking if %O is fixed .. ", x );
                cached_bugs[x] = get_issue( x );
                if( !(<"RESOLVED", "VERIFIED", "CLOSED">)[cached_bugs[x]->status] )
                {
                    werror("still not fixed.\n");
                    update_queue->write( x );
                }
                else
                    werror("now fixed.\n");
	        //FIXME: A some
                sleep( 60 + random(5) );
            }
        }
    };
}

void save_bugs()
{
    Stdio.write_file( CRUNCH_CACHE, encode_value( cached_bugs ) );
}

mapping cached_bugs = ([]);

mapping status_cache;

//FIXME: No XMLRPC interface to crunch, but it should be resonably
// easy to just parse the XML version of the bug.
mapping get_statuses()
{
    //!FIXME: Needs non-Jira version
    werror("CL Crunch: get_statuses\n");
    return ([]);

    if( status_cache )
        return status_cache;

    call_out( lambda(){status_cache=0;},100);
    Protocols.XMLRPC.Client remote = Protocols.XMLRPC.Client( query("xmlrpc_url") );
    string token;
    catch(token = remote["jira1.login"]( CRUNCH_USER, CRUNCH_PASSWORD )[0]);
    array status =  remote["jira1.getStatuses"]( token )[0];
    if( objectp( status ) )
        return ([]);
    return status_cache = mkmapping( status->id, status );
}

multiset cached_project_list;
multiset get_list_of_projects()
{
    //!FIXME: Needs non-Jira version (Or maybe not?)
    werror("CL Crunch: get_list_of_projects\n");
    return 0;

    if( cached_project_list ) return cached_project_list;
    Protocols.XMLRPC.Client remote = Protocols.XMLRPC.Client( query("xmlrpc_url") );
    string token;
    catch(token = remote["jira1.login"]( CRUNCH_USER, CRUNCH_PASSWORD )[0]);
    if( !token ) {
        werror("no login\n");
        return 0;
    }
    array status = Array.flatten(remote["jira1.getProjectsNoSchemes"]( token ));

    if( !sizeof( status ) )
    {
        werror("no status\n");
        return 0;
    }
    remote["jira1.logout"]( token );
    call_out( lambda() { cached_project_list = 0; }, 1200 );
    return cached_project_list = (multiset)status->key;
}

void create()
{
    defvar("crunch url", "http://bugzilla.roxen.com/bugzilla/", 
           "Crunch base URL", TYPE_URL|VAR_INITIAL, "Crunch base URL" );
    defvar("crunch user", "**", "Crunch username", 
           TYPE_STRING|VAR_INITIAL, "" );
    defvar("crunch password", "**", "Crunch password", 
           TYPE_STRING|VAR_INITIAL, "" );
    defvar("min bugno", 1000, "Lowest allowed bug number", 
           TYPE_INT|VAR_INITIAL, "Any number is considered a bug. This limits the false positives a bit." ); }

// Provider API. Returns the bug-id for a given word, if any.
string is_bug( string word )
{
    int id;
    if( sscanf( word, "#%d",id ) ) 
	return (string)id;
    if( (int)word && (int)word > query("min bugno") )
        return (string)(int)word;

    return 0;
}

mapping get_bug( string x )
{
    if( cached_bugs[x] )
        return cached_bugs[x];
    System.Timer t = System.Timer();
    werror("[fetching "+x+" ..");
    cached_bugs[x] = get_issue( x );
    werror(" %.1fs]", t->get());
    update_queue->write( x );
    remove_call_out( save_bugs );
    call_out( save_bugs, 10 );
    return cached_bugs[x];
}

string raw_get_bug(string id)
{
    string url = query("crunch url") +"show_bug.cgi?ctype=xml&id="+ id;
    object req = Protocols.HTTP.get_url(url);
    if( req->status != 200 )
    {
        werror("CL Crunch: Failed (%O) to load URL %O.\n", req->status, url);
        return 0;
    }
    return req->data();
}

object xml_element(object node, string name)
{
    array nodes = node->get_elements(name);
    if( sizeof(nodes)!=1 )
    {
        werror("CL Crunch: Error in xml format.\n%s\n", (string)node);
        return 0;
    }
    return nodes[0];
}

mapping process_bug(object bug)
{
    mapping bugdata = ([]);

    foreach( ({"bug_id", "bug_status", "short_desc"}), string name )
       bugdata[name] = xml_element(bug, name)->value_of_node();

    if( (<"RESOLVED", "VERIFIED", "CLOSED">)[bugdata->bug_status] )
       bugdata->resolution = xml_element(bug, "resolution")->value_of_node();

    return ([ 
        "no":bugdata->bug_id,
        "url":"fill_in_later",
        "icon":"bugzillastatus/"+bugdata->bug_status+".gif",
        "status":bugdata->bug_status, 
        "summary":String.trim_all_whites(bugdata->short_desc) 
    ]); 
}

mapping get_issue( string id )
{
    werror("CL Crunch: get_issue(%O)\n", id);
    mapping dummy_bug = ([
            "no":id,
            "icon":0,
            "status":"?",
            "summary":0,
        ]);
    if( !id || !strlen(id) || (int)id > 10000 )
        return dummy_bug;

    object node = Parser.XML.Tree.simple_parse_input( raw_get_bug(id) );
    node = xml_element(node, "bugzilla");
    node = xml_element(node, "bug");
    if (!node || node->get_attributes()->error)
        return dummy_bug;
    mapping buginfo = process_bug(node);
    if(buginfo->no)
        buginfo->url = query("crunch url") +"show_bug.cgi?id="+ buginfo->no;

    werror("issue %O: %O\n", id, buginfo);
    return buginfo;
}


void start( void|int num, void|Configuration conf )
{
    destruct( update_queue );
    update_queue = Thread.Queue();

    if( conf )
    {
        thread_create( update_bugs );
        catch(cached_bugs = decode_value(Stdio.read_file( CRUNCH_CACHE )));
        foreach( cached_bugs; string key; mapping bug )
            update_queue->write( key );
        werror("%O cached bugs\n", sizeof(cached_bugs));
    }
}
