inherit "sh.pike";
constant module_name = "CVS Viewer: configure.in file highlighter";


int can_highlight( string ext, string filename, string header )
{
    return ext == "in" && has_value( filename, "configure");
}

constant rules =
  "(,),`,case,do,done,elif,else,esac,case,fi,if,for,function,in,select,then,until,while,||,&&: 1\n"
  "test,exit,{,},$: 3\n"
  "[:7->][\n"
  "\":7->\"\n"
  "':7->'\n"
  "#:5->nl\n"
  "dnl:5->nl\n"
  "AC:1->(\n"
  "PIKE:1->(\n"
;

