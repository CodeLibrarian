inherit "c";
constant module_name = "CVS Viewer: XML syntax highlighter";

int can_highlight( string ext, string filename, string header )
{
    return (has_suffix(filename, ".svg") ||
            has_suffix(filename, ".xml" ) ||
            String.count(header, "<") > 3 );
}

string highlight( string file )
{
    string res = "";
    object p = Parser.HTML();
    void got_entity( object p, string entity )
    {
        res += "\0""1"+entity+"\0""1";
    };

    void got_comment( object p, string comment )
    {
        res += "\0""6<!--"+comment+"-->\0""6";
    };

    void got_data( object p, string data )
    {
        res += data;
    };

    void got_tag( object p, string tag )
    {
        tag = tag[1..strlen(tag)-2];
        string attributes, name;
        name = (tag/" ")[0];
        if( name == "!--" )
        {
            res += "<\0""6"+tag+">\1""6";
            return;
        }
        attributes = "\0""1 ";
        int state, in = 1;
        foreach( (array)tag[strlen(name)+1..], int i )
        {
            switch( state )
            {
                case 0:
                    switch( i )
                    {
                        case '=': 
                            attributes += "\1""1\0""7";
                            in = 7;
                            break;
                        case ' ': 
                            if( attributes[-1] != ' ' )
                            {
                                attributes += "\1""7\0""1"; 
                                in = 1;
                            }
                            break;
                        case '"': state = 1; break;
                        case '\'': state = 2; break;
                    }
                    break;
                case 1:
                    if( i == '"' )
                        state = 0;
                    break;
                case 2:
                    if( i == '\'' )
                        state = 0;
                    break;
            }
            attributes += sprintf("%c", i );
        }
        if( strlen( attributes ) == 3 )
            attributes="";
        else
            attributes += "\1"+in;

        res += "<"+"\0""3"+name+"\1""3"+attributes+">";
    };

    p->add_quote_tag( "!--", got_comment, "--" );
    p->_set_tag_callback( got_tag );
    p->_set_entity_callback( got_entity );
    p->_set_data_callback( got_data );
    p->finish(file);
    return res;
}

