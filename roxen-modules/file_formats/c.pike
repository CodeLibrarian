#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_PROVIDER;
constant module_name = "CVS Viewer: C/C++/Java syntax highlighter";

string|array(string) query_provides()
{
    return "syntax-highlight";
}

int can_highlight( string ext, string filename, string header )
{
    if( (<"c","h","cpp","cp","java",
          "hh","cc">)[ ext ] )
        return 1;
    if( String.count(header, "{") > 3 ||
        has_value(header, "-*- c -*-") ||
        has_value(header, "-*- c++ -*-"))
        return 1;
}


// Should return HTML for the given C-like code.
string highlight( string file )
{
    string result = "", last_token;
    array(Parser.C.Token) tokenized = Parser.C.tokenize(Parser.C.split(file));
    foreach(Parser.C.hide_whitespaces(tokenized), Parser.C.Token token)
    {
        string color = 0;
        string token_text = (string)token;
        switch(token_text)
        {
            case "char":	case "long":	case "union":
            case "double":	case "short":	case "unsigned":
            case "enum":	case "signed":	case "void":
            case "float":	case "struct":  case "class":
            case "int":	case "typedef": case "uni_char":
            case "wchar_t": case "UNI_L": case "bool": case "boolean":
            case "BOOL":
            case "INT32": case "UINT32": case "INT": case "UINT":
            case "WORD": case "BYTE": case "byte":
                color = "1";
                break;

// 	case "{": case "}": case "(": case ")": case "[": case "]":
// 	  color = "2"; 
// 	  break;

            case "break":	case "default":	case "for":	case "return":
            case "try":         case "finally": case "catch":   case "this":
            case "case":	case "do":	case "goto":	case "switch":
            case "continue":    case "else":	case "if":	case "while":
            case "new":         case "delete":  case "implements": case "extends": 
                color = "3";
                break;

            case "auto":	case "extern":	case "static":
            case "const":	case "register":case "volatile":
            case "inline":	case "private":	case "protected":
            case "nomask":	case "public": case "namespace":
            case "final":       case "import": case "package":
                color = "4"; break;
        }
        if(token_text[0]=='#')
            color = "5";
        else if((< "//", "/*" >)[token_text[0..1]])
            color = "6";
        else if(token_text[0]=='"' && token_text[-1]=='"')
            color = "7";
        else if( token_text[0] <= 'Z' && token_text[0] >= 'A' &&
                 strlen(token_text) > 1 &&
                 token_text[1] <= 'z' && token_text[1] >= 'a' &&
                 last_token != "->" && last_token != ".")
            color = "1";
        last_token = token_text;
        if(color)
            result += sprintf("\0%s%s\1%s", color, token->text, color);
        else
            result += token->text;
        result += token->trailing_whitespaces;
    }
    return result;
}
