inherit "c";
constant module_name = "CVS Viewer: SH file highlighter";


int can_highlight( string ext, string filename, string header )
{
  return ext == "sh"  ||
    ((header-=" ") &&
     has_value( header, "#!/bin/sh") ||
     has_value( header, "#!/bin/ksh")||
     has_value( header, "#!/bin/bash"));
}

constant com = "6";
constant str = "7";
constant key = "1";

constant rules =
  "`,case,do,done,elif,else,esac,case,fi,if,for,function,in,select,then,until,while,||,&&: 1\n"
  "test,exit,[,],{,},(,),$: 3\n"
  "\":7->\"\n"
  "':7->'\n"
  "#:5->nl\n"
;

mapping keywords = ([]);
mapping(int:array(string)) keywords_sort = ([]);

void parse_rules()
{
  foreach( rules/"\n", string rule )
  {
    string a;
    string|array b;
    if( sscanf( rule, "%s:%s", a, b ) == 2)
    {
      b = String.trim_all_whites(b);
      if( has_value( b, "-" ) )
	b = replace(map(b/"->",String.trim_all_whites)[*],([ "nl":"\n","spc":" "]));
      foreach( a/",", string key )
      {
	keywords[String.trim_all_whites(key)] = b;
      }
    }
  }
  foreach( reverse(indices(keywords)), string key )
    keywords_sort[key[0]] += ({ key });
}

void start(){
  parse_rules();
}

string highlight(string file)
{
    string res = "";
    string start(string x){if(strlen(x))return "\0"+x;return "";};
    string end(string x){if(strlen(x))return "\1"+x;return "";};


    for( int i; i<sizeof(file); i++ )
    {
      void skip_to(int char, int|void inc)
      {
	int n = 1;
	res += file[i..i];
	for( ++i; i<sizeof(file) && (file[i] != char || --n); i++ )
	{
	  if( file[i] == inc )
	    n++;
	  res += file[i..i];
	  if( file[i] == '\\' ){
	    i++;
	    res += file[i..i];
	  }
	}
	res += file[i..i];
      };
      bool if_looking_at( string x, array|string col )
      {
	string look = file[i..i+strlen(x)];
	if( has_prefix(look,x) && ((strlen(x) == 1) || (<'(','_',' ','\t','\n','\r',';',':'>)[look[-1]]) )
	{
	  if( arrayp(col))
	  {
	    res += start(col[0]);
	    skip_to( col[1][0],strlen(col[1])>1?col[1][-1]:-1 );
	    res += end(col[0]);
	    return true;
	  }
	  res += start(col);
	  res += x;
	  res += end(col);
	  i += strlen(x)-1;
	  return true;
	};
      };

      bool done;
      if( array keys = keywords_sort[file[i]] )
	foreach( keys, string key )
	  if( if_looking_at( key,keywords[key] ) )
	  {
	    done=true;
	    break;
	  }
      if( !done )
	res += file[i..i];
    }
    return res;
}
