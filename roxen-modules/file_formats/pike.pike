inherit "c";
constant module_name = "CVS Viewer: Pike syntax highlighter";

int can_highlight( string ext, string filename, string header )
{
    if(ext=="pike" || ext=="pmod" )
        return 1;
    if( has_value(header, "-*- pike -*-") )
        return 1;
}


string highlight(string file)
{
    string result = "";
    array(Parser.Pike.Token) tokenized;
    tokenized = Parser.Pike.tokenize(Parser.Pike.split(file));
    string last_token;
    foreach(Parser.Pike.hide_whitespaces(tokenized), Parser.Pike.Token token)
    {
        string color = 0;
        string token_text = (string)token;
        switch(token_text)
        {
            case "array":	case "function":case "multiset":case "this_program":
            case "constant":case "int":	case "object":	case "typedef":
            case "enum":	case "mapping":	case "program":	case "void":
            case "float":	case "mixed":	case "string":
                color = "1"; break;

            case "{": case "}": case "(": case ")": case "[": case "]":
                color = "2"; break;

            case "break":	case "do":	case "import":	case "sscanf":
            case "case":	case "else":	case "if":	case "switch":
            case "catch":	case "for":	case "inherit":	case "this":
            case "class":	case "foreach":	case "lambda":	case "typeof":
            case "continue":case "gauge":	case "predef":	case "while":
            case "default":	case "global":	case "return":  case "delete":
            case "new":
                color = "3"; break;

            case "inline":	case "private":	case "protected":
            case "nomask":	case "public":	case "static":
                color = "4"; break;
        }
        if(token_text[0]=='#')
            color = "5";
        else if((< "//", "/*" >)[token_text[0..1]])
            color = "6";
        else if(token_text[0]=='"' && token_text[-1]=='"')
            color = "7";
        else if( token_text[0] <= 'Z' && token_text[0] >= 'A' &&
                 strlen(token_text) > 1 &&
                 token_text[1] <= 'z' && token_text[1] >= 'a' &&
                 last_token != "->" )
            color = "1";
        if(color)
            result += sprintf("\0%s%s\1%s", color, token->text, color);
        else
            result += token->text;
        last_token = token_text;
        result += token->trailing_whitespaces;
    }
    return result;
}

