inherit "c";
constant module_name = "CVS Viewer: INI file highlighter";


int can_highlight( string ext, string filename, string header )
{
    return ext == "ini" || has_value( filename, ".ini.");
}


string highlight(string file)
{
    string res = "";
    string string1, string2, post, pre;

    foreach( file/"\n", string x )
    {
        string tmp;
        if( strlen( x ) )
        {
            if( sscanf( x, "[%s]%s", x, tmp ) )
                x = sprintf("\0""3[%s]\1""3%s", x, tmp );
            string tmp = "";
            while( sscanf( x, "%s\"%s%[^\\]\"%s", pre, string1, string2, x ) == 4 )
                tmp += sprintf( "%s\0""7\"%s%s\"\1""7", pre, string1, string2 );
            if( (sscanf( x, "%s#%s", pre, post  ) == 2) ||(sscanf( x, "%s;%s", pre, post  ) == 2) )
                x = sprintf("%s\0""6#%s\1""6", pre, post );
            x = tmp+x;
            if( sscanf( x, "%[^\"=]=%s", pre, post ) == 2 )
                x = sprintf("\0""1%s\1""1=%s", pre, post );
        }
        res += x+"\n";
    }
    return res;
}
