#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_PROVIDER;
constant module_name = "CVS Viewer: LISP syntax highlighter";

string|array(string) query_provides()
{
    return "syntax-highlight";
}

int can_highlight( string ext, string filename, string header )
{
    if( (<"lisp","el","cl">)[ ext ] )
        return 1;
    if( (String.count(header, "(") > 6 &&String.count(header, ")") > 5) ||
        has_value(header, "-*- elisp -*-") ||
        has_value(header, "-*- lisp -*-"))
        return 1;
}


// Should return HTML for the given C-like code.
string highlight( string file )
{
    string result = "", last_token;
    int next_is_var, next_is_fun, comment_to_eol;

    array(Parser.C.Token) tokenized = Parser.C.tokenize(Parser.C.split(replace(file,"-","\xffee")));

    foreach(Parser.C.hide_whitespaces(tokenized), Parser.C.Token token)
    {
        token->text = replace(token->text, "\xffee", "-");
        string color = 0;
        string token_text = (string)token;
        if( !comment_to_eol )
        switch(token_text)
        {
            case "let":
                color = "1";
                break;
            case "defvar":
            case "defconst":
                next_is_var = 1;
            case "defun":
            case "defmacro":
                next_is_fun = 1;
                color = "3";
                break;
        }
        if(!color)
        {
            if( next_is_var )
                color = "1";
            else if( next_is_fun )
                color = "4";
            next_is_var = 0;
            next_is_fun = 0;
        }
        if( token_text == ";" )
            comment_to_eol = 1;

        if( comment_to_eol )
        {
            color = "6";
            if( has_value( token->trailing_whitespaces, "\n" ) )
                comment_to_eol = 0;
        }

        if( !comment_to_eol )
            if((token_text[0]=='"' && token_text[-1]=='"') || (token_text[0]=='\'' && token_text[-1]=='\''))
                color = "7";
        last_token = token_text;
        if(color)
            result += sprintf("\0%s%s\1%s", color, token->text, color);
        else
            result += token->text;
        result += token->trailing_whitespaces;
    }
    
    return result;
}