inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_FIRST;
constant module_name = "Foce SSL";
constant module_doc  = "This module redirects all non-ssl requests to the SSL port";

mapping first_try(RequestID id) 
{
    if(id->my_fd && 
       (!id->my_fd->session || !id->my_fd->session->cipher_spec 
	|| id->my_fd->session->cipher_spec->key_bits < 128))
  {
    string url = id->conf->query("MyWorldLocation")+id->raw_url[1..];
    if( has_prefix( url, "http:" ) ) url = "https:"+url[5..];
    return Roxen.http_redirect(url);
  }
}
