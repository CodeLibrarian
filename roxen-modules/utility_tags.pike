#include <module.h>
inherit "module";
constant thread_safe = 1;
constant module_type = MODULE_TAG;
constant module_name = "CVS Viewer: Utility tags";
constant module_doc =  "Some tags used by the default RXML interface to the cvsview module";

class TagPermanentURL
{
    inherit RXML.Tag;
    constant name = "permanent-url";

    class Frame
    {
        inherit RXML.Frame;
        array do_return(RequestID id)
        {
	  
        }
    }
}


class TagParseDate
{
    inherit RXML.Tag;
    constant name = "parse-date";

    constant relative = ([
        "last":1,
        "second":2,
        "third":3,
        "fourth":4,
        "fifth":5,
        "sixth":6,
        "seventh":7,
        "eight":8,
        "ninth":9,
        "tenth":10,
        "eleventh":11,
        "one":1,
        "two":2,
        "three":3,
        "four":4,
        "five":5,
        "six":6,
        "seven":7,
        "eight":8,
        "nine":9,
        "ten":10,
        "eleven":11,
        "förra":1,
        "förrförra":2,
        "föregående":1,
        "preceding":1,
        "before":1,
    ]);


    string parse_time( string x, int start )
    {
        int parsed;
        int h, m, s;
        string pre, post;
        object t = Calendar.Second();

        sscanf( x, "%s ago", x );
        sscanf( x, "sedan %s", x );
        sscanf( x, "since %s", x );
        sscanf( x, "i %s", x );

        if( strlen(x) && x[0] == '@' )
        {
            t = Calendar.Second( (int)x[1..] );
            parsed = 2;
        }
        else
        {
            if( !catch(t=Calendar.dwim_time(x)) )
                return t->set_timezone("UTC")->format_time();
            if( sscanf( x, "%s%d:%d%s", pre, h, m, post ) != 4 )
            {
                if( start )
                    h = m = s = 0;
                else
                {
                    if( String.trim_all_whites(pre) == "today" )
                    {
                        mapping m2 = localtime(time());
                        h = m2->hour; 
                        m = m2->min;
                        s = m2->sec;
                    }
                    else
                    {
                        h = 23;
                        m = 59;
                        s = 59;
                    }
                }
            }
            else
            {
                sscanf( post, ":%d%s", s, post );
                x = String.trim_all_whites(pre)+post;
                parsed = 1;
            }
        }

        if( !catch(t=Calendar.dwim_day(x)) )
            parsed=2;
        while( catch( t = t->day()->hour(h)->minute(m)->second(s) ) && h > 0 )
            h--;

        if( parsed == 2 )
            return t->format_time();

        switch( String.trim_all_whites(x) )
        {
            case "forever":
                t = Calendar.Year(100);
                parsed = 1;
                break;
            case "monday":case "mon":case "måndag":case "mån":
                t = t->week()->day(1)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "tuesday":case "thuesday":case "tue":case "tisday":case "tis":
                t = t->week()->day(2)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "wednesday":case "wed":case "onsdag":case "ons":
                t = t->week()->day(3)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "thursday":case "thu":case "torsdag":case "tor":
                t = t->week()->day(4)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "friday":case "fri":case "fredag":case "fre":
                t = t->week()->day(5)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "saturday":case "sat":case "lördag":case "lör":
                t = t->week()->day(6)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "sunday":case "sun":case "söndag":case "sön":
                t = t->week()->day(7)->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;
            case "today": case "idag":
                parsed = 1;
		
                break;

            case "förrgår": case "day before yesterday":
                t-=Calendar.Day();
            case "yesterday": case "igår":case "gårdagen":case "gårdagens":
                t-=Calendar.Day();
                parsed = 1;
                break;
            case "easter": case "påskas": case "påsk":
                t = Calendar.Event.Easter()->scan(Calendar.Year())[0];
                if( start && t > Calendar.Day() )
                    t = Calendar.Event.Easter()->scan(Calendar.Year()-Calendar.Year())[0];
                t=t->day()->hour(h)->minute(m)->second(s);
                parsed = 1;
                break;

            default:
                int delta;
                if( (sscanf( x, "%d%s", delta, x )==2) || (delta=relative[(x/" ")[0]]) && (x=(x/" ")[1..]*" ") )
                {
//		    werror("delta: %d; unit: %s\n", delta, x );
                    sscanf( x, "%s ago", x );
                    sscanf( x, "sedan %s", x );
                    sscanf( x, "to last %s", x );
                    sscanf( x, "since %s", x );
                    sscanf( x, "i %s", x );
                    delta = abs(delta);
                    switch( String.trim_all_whites( x ) )
                    {
                        case "seconds": case "second": case "sekund":case "sekunder":
                            parsed = 1;
                            t = Calendar.Second()-Calendar.Second()*delta;
                            break;
                        case "minutes": case "minute": case "minut": case "minuter":
                            parsed = 1;
                            t = Calendar.Second()-Calendar.Minute()*delta;
                            break;
                        case "hours": case "hour": case "timmar": case "timme":
                            parsed = 1;
                            t = Calendar.Second()-Calendar.Hour()*delta;
                            break;
                        case "day": case "days": case "dagar": case "dag":
                            parsed = 1;
                            t -= Calendar.Day()*delta;
                            break;
                        case "month": case "months": case "månader": case "månad":
                            parsed = 1;
                            t -= Calendar.Month()*delta;
                            break;
                        case "week": case "weeks": case "veckor": case "vecka":
                            parsed = 1;
                            t -= Calendar.Week()*delta;
                            break;
                        case "year": case "years": case "år": 
                            parsed = 1;
                            t -= Calendar.Year()*delta;
                            break;
                        case "easter": case "påsk": case "easters": case "påskar":
                            parsed = 1;
                            t = Calendar.Event.Easter()->scan(Calendar.Year())[0];
                            if( t < Calendar.Day() )
                                delta--;
                            t = Calendar.Event.Easter()->scan(Calendar.Year()-Calendar.Year()*delta)[0];
                            t = t->day()->hour(h)->minute(m)->second(s);
                            break;
                        case "monday":case "mon":case "måndag":case "mån":
                            t = t->week()->day(1)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                        case "tuesday":case "thuesday":case "tue":case "tisday":case "tis":
                            t = t->week()->day(2)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                        case "wednesday":case "wed":case "onsdag":case "ons":
                            t = t->week()->day(3)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                        case "thursday":case "thu":case "torsdag":case "tor":
                            t = t->week()->day(4)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                        case "friday":case "fri":case "fredag":case "fre":
                            t = t->week()->day(5)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                        case "saturday":case "sat":case "lördag":case "lör":
                            t = t->week()->day(6)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                        case "sunday":case "sun":case "söndag":case "sön":
                            t = t->week()->day(7)->hour(h)->minute(m)->second(s);
                            t -= Calendar.Week()*delta;
                            parsed = 1;
                            break;
                    }
                }
        }
        if( !parsed )
        {
//	    werror("%s not parsed\n", x);
            return 0;
        }
        return t->set_timezone("UTC")->format_time();
    };

    class Frame
    {
        inherit RXML.Frame;
        array do_return(RequestID id)
        {
            switch( String.trim_all_whites(content) )
            {
                case "now": 
                    if( args->start )
                        return ({ (Calendar.now()->hour()-1)->set_timezone("UTC")->format_time() });
                    return ({ Calendar.now()->set_timezone("UTC")->format_time() });
	     
                case "last time": case "last visit": case "last": case "förra": case "senast": case "sist":
                    return ({ id->cookies["last_view"]||parse_time("today",0) });
            }
            string res = parse_time( content, args->start ? 1 : 0 );
            content="";
            if( res ) 
                return ({res});
            else
                return ({""});
        }
    }
}
