#include <module.h>
inherit "module";

constant thread_safe = 1;
constant module_type = MODULE_TAG | MODULE_LOCATION;
constant module_name = "CVS Viewer: Main module";
constant module_doc = #"A clean rewrite of cvsview.pike, though still
a whole lot less than Code Librarian - in all, a pragmatic approach.";

string explain(array(mapping) what )
{
    string zs(string x){ return x||""; };
    constant columns =
        ({ "table", "type", "possible_keys", "key", 
           "key_len", "ref", "rows", "Extra" });

    array len = map(columns,strlen)[*]+1;

    // find max len
    foreach( what, mapping m )
    {
        array a = map(map(rows(m,columns),zs),sizeof);
        for( int i = 0; i<sizeof(columns); i++ )
            if( len[i] < a[i]+1 )
                len[i] = a[i]+1;
    }
    // print header.
    string format = map(len,lambda(int i){ return "%-"+i+"s";})*""+"\n";
    werror( format, @columns );
    werror("="*`+(@len)+"\n");
    // print rows.
    foreach( what, mapping m )
        werror( format, @map(rows(m,columns),zs) );
}

mapping(object:Sql.Sql) dbs = ([]);
static array(mapping) debug_query( string query, mixed...args )
{
    int delay = gethrtime();
    Sql.Sql db =
      DBManager.get( ::query( "dburl" ), UNDEFINED, UNDEFINED, true,
		     "broken-unicode" );

    if(!db)
	werror("Failed to connect to database %O, bad things will happend\n",
	       ::query("dburl"));

    array res = db->query( query, @args );
    
    delay = gethrtime()-delay;
    if( delay > 1000000 )
    {
        werror( "\n\n** Slow query **\n%s\nTook %.1fs to execute\n", query, delay/1000000.0 );
        explain(db->query( "EXPLAIN "+query, @args ));
        werror("\n\n");
    }
    return res;
}

string dbquote( string x ) { return replace(x,(["'":"\\'","\0":"\\0"])); }
static void add_error( RequestID id, string x, mixed ... args )
{
    if( sizeof( args ) )
        x = sprintf( x, @args );
    id->misc->_cvsview_error += x;
}
#define ERROR(X...) add_error(id, X)

roxen.ImageCache the_cache;
mapping repositories = ([]), users = ([]);

mapping find_internal( string f, RequestID id )
{
    return the_cache->http_file_answer( f, id );
}

string status() 
{
    array s=the_cache->status();
    string res = sprintf("<b>Images in cache:</b> %O images<br />\n"
                         "<b>Cache size:</b> %s<br>",
                         s[0], Roxen.sizetostring(s[1]));
    return res;
}

mapping(string:function) query_action_buttons() 
{
    return ([ "Clear cache":flush_cache ]);
}

void flush_cache() {
    the_cache->flush();
}


// mapping(git_dir:mapping(tag:revision))
static mapping(string:mapping(string:string)) git_forward_tags = ([]);
// mapping(git_dir:mapping(revision:array(tag)))
static mapping(string:mapping(string:array(string))) git_reverse_tags = ([]);

static mapping(string:int) tagdir_mtime = ([]);
static void low_check_update_git_tags(string tagdir, string prefix,
				      mapping(string:string) forward,
				      mapping(string:array(string)) reverse)
{
  string dir = tagdir + prefix;
  Stdio.Stat st = file_stat(dir);
  if (st->mtime == tagdir_mtime[dir]) return;
  foreach(get_dir(dir), string tag) {
    tag = prefix + tag;
    if (forward[tag]) continue;
    if (Stdio.is_dir(tagdir + tag)) {
      low_check_update_git_tags(tagdir, tag + "/", forward, reverse);
    } else {
      string rev = Stdio.read_bytes(tagdir + tag)[..39];
      forward[tag] = rev;
      reverse[rev] = (reverse[rev] || ({})) + ({ tag });
    }
  }
}

static void check_update_git_tags(string git_dir)
{
  mapping(string:string) forward = git_forward_tags[git_dir] || ([]);
  mapping(string:array(string)) reverse = git_reverse_tags[git_dir] || ([]);
  git_forward_tags[git_dir] = forward;
  git_reverse_tags[git_dir] = reverse;
  low_check_update_git_tags(git_dir + "/refs/tags/", "", forward, reverse);
}

string okey;
mapping otags;
//! @returns
//!   Returns a mapping from revision to the tag names
//!   for that revision.
mapping(string:array(string)) get_rev_tags( string module, string path )
{
    if( module+path == okey )
        return otags;
    okey = module+path;

    string cvsroot =  get_cvsroot(module);
    if (has_prefix(cvsroot, ":git::")) {
      string git_dir = cvsroot[6..];
      check_update_git_tags(git_dir);
      // Note: We're cheating, and don't join tags for revisions
      //       where @[path] wasn't changed.
      return otags = git_reverse_tags[git_dir];
    }
    mapping res = ([]);
    string f = basename(path);
    foreach( ({combine_path( cvsroot, module, dirname(path) ), 
               combine_path( cvsroot, module, dirname(path), "Attic" ) }), 
             string dir )
    {
        int syms;
        if( file_stat( combine_path( dir, f ) ) )
            foreach( Stdio.File( combine_path( dir, f ), "r")->line_iterator(); 
                     int n; string l )
            {
                string name, rev;
                if( !syms && !has_value( l, "symbols" ) )
                    continue;
                syms=1;
                if( sscanf( l, "\t%s:%s", name, rev )  == 2 )
                {
                    if( rev[-1] == ';' ) {
                        rev = rev[..strlen(rev)-2];
                        syms=0;
                    }
                    if( res[rev] )
                        res[rev] += ({name});
                    else
                        res[rev] = ({name});
                    if( !syms )
                        break;
                } else if( has_value( l, ";" ) )
                    break;
            }
    }
    return otags = res;
}

mapping admin_users = ([]);
void start(void|int num, void|Configuration conf)
{
    dbs = ([]);

    if( conf )
    {
        the_cache = roxen.ImageCache( "cvsview", generate_image );
        get_repository_id( 0 );
        admin_users = mkmapping(query( "admin_users" ),query( "admin_users" ));
        if(conf)
            module_dependencies(conf, ({ "business", "utility_tags",
                                         "pikescript", "additional_rxml" }));
    }
}

void create()
{
    defvar( "pike", "/usr/local/bin/pike", "Pike binary", TYPE_FILE,
            "The pike binary to use for the external scripts");

    defvar( "parse_modules",
            "/home/ph/src/code_librarian/parse_modules.pike",
            "Module list parser",
            TYPE_FILE,
            "Module list (CVSROOT/modules) parser" );

    defvar( "tag_file_directory",
            "/var/cvs/web/pchangelog/tags",
            "Tag file directory", TYPE_DIR,
            "Directory with tag-to-version maps. "
            "Created by the 'tags.pike' script" );

    defvar( "admin_users",
            Variable.StringList( ({}), 0, "Admin users",
                                 "Users that will see extra options "
                                 "(mark as merge et.al.)"));

    defvar( "build_tags",
            Variable.StringList( ({"*build*"}), 0, "Build tags",
                                 "Tags that will be filtered when no build tags are wanted"));

    defvar("location", Variable.Location("/cvs/", 0, "Mount point",
                                         "This is where the module will be inserted in the "
                                         "namespace of your server."));

    defvar("repository_regexp", Variable.String(".*", 0, "Repository regexp",
                                                "What repositories to list commits / generate commit graphs for,"
                                                "unless otherwise specified in tag arguments. This is a MySQL "
                                                "regexp that should match the uriname of all wanted repositories."));

class DatabaseVar
{
  inherit Variable.StringChoice;
  array get_choice_list( )
  {
    return ({ " none" })
           + sort(DBManager.list( my_configuration() ));
  }
};

    //FIXME: "url" is for historical reasons. refactor.
    defvar( "dburl",
           DatabaseVar( "librarian",({}),0,
                        "SQL database",
		        "The Roxen identifier for your database." ) );


    defvar("gitbin", "/usr/bin/git",
	   "GIT binary", TYPE_FILE,
	   "Path to the git binary.");

    defvar("cvsbin", "/usr/bin/cvs",
           "CVS binary", TYPE_FILE,
           "Path to the cvs binary.");

    defvar("default_mailhost", "example.com", "Default domain",
           TYPE_STRING,
           "The default domain shown for users");

    defvar("person", "&lt;%s@%s&gt;", "User name template",
	   TYPE_STRING,
	   "The first %s will be replaced with the login name, "
	   "the second with the mail domain.");

    defvar("viewcvs",
           "https://viewcvs.internal/viewcvs/viewvc.cgi/%s/%s.diff?r1=%s&r2=%s",
           "The alternative CVS viewer",
           TYPE_STRING,
           "The strings are module, path, ancestor, revision");

/* The URL is now controlled by the bug plugins
    defvar("bugs", "https://bugs.internal/show_bug.cgi?id=%s",
           "Bugdatabase URL", TYPE_STRING,
           "%s will be replaced with the bug id" );
*/
}

int rev_is_greater( array(int) r1, array(int) r2 )
{
    for( int i=0; i<sizeof(r2); i++ )
        if( r1[i] > r2[i] )
            return 1;
}

array(string) branchify( string x )
{
    array res = ({x});
    return res;
    string base = "";
    array z = x / ".";
    for( int i = 1; i<sizeof(z)-1; i++)
        res += ({z[..i]*"."+".0."+z[i+1..]*"."});
    return res;
}

int rev_is_ancestor( string a, string b )
{
    if( !b || !a ) return 0;
    if( String.count( b, "." ) < String.count(a, "." ) )
        return 0;
    array aa = (array(int))(a/".");
    array ba = (array(int))(b/".");
    if( sizeof(aa) > sizeof(ba) )
        ba += ({0}) * (sizeof(aa)-sizeof(ba));
    else if( sizeof(ba) > sizeof(aa) )
        aa += ({0}) * (sizeof(ba)-sizeof(aa));
    return rev_is_greater( aa, ba );
}

array(string) treewalk_a( array(int) rev1, array(int) rev2, mapping msg )
{
    array(string) res = ({});
    string base = ""+rev1[0]+".";

    if( rev_is_greater( rev1, rev2 ) ) {
        [rev1,rev2] = ({rev2,rev1});
    }

    if( rev1[0] != rev2[0] )
    {
        msg->error = sprintf("The revisions %s and %s (for %s) recide on two different toplevel branches.\n"
                             "Can not currently produce a log for this.\n",
                             ((array(string))rev1)*".",((array(string))rev2)*".",msg->file);
        return ({});
    }

    for( int i = 1; i<sizeof(rev2); i+=2 )
    {
        for( int rn=rev1[i]+1; rn<=rev2[i]; rn++ )
        {
            res += branchify( base+rn );
        }
        if( i+1 >= sizeof(rev2) )
        {
            return res;
        }
        if( rev1[i+1] && (rev2[i+1] != rev1[i+1]) )
        {
            // Diff from start of branch instead.
            msg->error = 
                "Some revisions recide on two different branches.\n"
                "Those will contain the diff from the branchpoint of the first branch to the end of the last one.\n";
            return treewalk_a( rev1[..i] + ({ 0 })*(sizeof(rev1)-i-1), rev2, msg);
        }
        base += rev2[i]+"."+rev2[i+1]+".";
    }
    return res;
}

array(string) treewalk( string rev1, string rev2, mapping msg )
{
    if( !rev2 || rev1==rev2 )
        return ({});

    array a1 = rev1?(array(int))(rev1/"."):({1});
    array a2 = (array(int))(rev2/".");
    if( a1[0] == 0 )
        a1[0] = a2[0];
    if( sizeof( a1 ) > sizeof( a2 ) )
        [a2,a1] = ({ a1,a2 });
    a1 += ({ 0 }) * (sizeof(a2)-sizeof(a1));
    return treewalk_a( a1, a2, msg );
}

Calendar.TimeRange utc_second(string timestamp)
{
    if( `-( timestamp, "00", "-", " ", ":") == "" )
        return Calendar.ISO.Year( 0 )->second( 0 );
    return Calendar.ISO.parse("%y-%M-%D %h:%m:%s", timestamp);
}

class TagEmitRepositoryList
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "repository-list";

    mapping polish_entities(mapping entry)
    {
        return entry - filter(indices(entry), has_value, ".");
    }

    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        NOCACHE(); // dynamic results, for now
        array(mapping) dir;
        if(arg->modules)
            dir = debug_query("SELECT * FROM cl_repository "
                              "WHERE module REGEXP %s ORDER BY name", arg->modules);
        else if( arg->qa )
            dir = debug_query("SELECT * FROM cl_repository WHERE qa=1 ORDER BY name ");
        else
            dir = debug_query("SELECT * FROM cl_repository ORDER BY LOWER(name)",
                              time(1)-(3600*24*90));
        return map(dir, polish_entities);
    }
}

constant units = ([ 60 : "minute",
                    3600 : "hour",
                    86400 : "day",
                    604800 : "week",
                    2628000 : "month",
                    31557600 : "year",
                    315576000 : "decade",
                    3155760000 : "centrury",
                    31557600000 : "millenia",
                    315576000000 : "eon",
                 ]);

string how_old(int|string utc_time, int|void exact)
{
    int seconds;
    if( stringp( utc_time ) )
    {
        //object utc = Calendar.set_timezone("UTC");
        Calendar.TimeRange age = utc_second(utc_time)->range(Calendar.now()->second());
        seconds = age->how_many( Calendar.Second );
    }
    else
        seconds = utc_time;

    if(seconds == 0)
        return "now";
    if(seconds < 60)
        return sprintf("%d second%s", seconds, (seconds==1 ? "" : "s"));

    array result = ({ });
    foreach(reverse(sort(indices(units))), int unit)
        if(seconds > unit)
        {
            int n;
            if( exact )
                n = seconds/unit;
            else
                n = (int)round((float)seconds/(float)unit);
            string part = sprintf("%d %s%s", n, units[unit], (n==1?"":"s"));
            if(!exact)
                return part + " ago";
            seconds -= n * unit;
            result += ({ part });
        }
    return String.implode_nicely(result);
}


mapping image_urls = ([]);
mixed do_image( string cvsroot, string path, string version, int icon, RequestID id )
{
    mapping args = ([ "cvsroot":cvsroot,"format":"png","path":path,"r1":version]);
    if( icon )
    {
        args->nochecks=1;
        args["max-width"] = args["max-height"] = 20;
        args["true-alpha"] = 1;
        return "../.."+query_internal_location()+the_cache->store( args, id );
    }
    return 
        Roxen.make_tag( "img", ([ "src":query_internal_location()+
                                  the_cache->store( args, id ) ]), 1);
}

int is_image( string x )
{
    return (<"tga","xpm","ttf","xbm","bmp",/*"ico",*/"png","jpg","xcf","jpeg","gif">)
        [lower_case((x/".")[-1])];
}

class TagEmitRepositoryDir
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "repository-dir";


    int deleted_flag_dir( int module, int id  )
    {
        int del = (int)debug_query("SELECT COUNT(id) as c FROM cl_file WHERE parent_dir_id=%d AND repository_id=%d",id,module)[0]->c;
        if( del )
            return 0;
        // FIXME: Check for sub-directories with files.
        return 1;
    }


    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        NOCACHE(); // dynamic results, for now
        string dirname = array_sscanf(arg->dir||"", "%*[/]%s")[0];
        array(mapping) dirs, files;
        int parent_dir_id = md5_56bit(dirname),
            parent_dir_id2 = md5_56bit(strlen(dirname)?dirname+"/Attic":"Attic"),
            repository_id = (int)get_repository_id( arg->module );
        dirs = debug_query("SELECT HIGH_PRIORITY id,name,modified,parent_dir_id FROM cl_dir "
                           "WHERE repository_id=%d AND (parent_dir_id=%d OR parent_dir_id=%d) "
                           "ORDER BY name", repository_id, parent_dir_id,parent_dir_id2);
        dirs = map(dirs, dir_entities, dirname, parent_dir_id, repository_id);

        files = debug_query("SELECT HIGH_PRIORITY id AS file_id,parent_dir_id,name,modified "
                            "FROM cl_file "
                            "WHERE repository_id=%d AND (parent_dir_id=%d OR parent_dir_id=%d) "
                            "ORDER BY id", repository_id, parent_dir_id,parent_dir_id2);
        files = get_file_entities(files, repository_id, dirname, arg->branch, parent_dir_id,
                                  arg->module, id);
        if(zero_type(arg->sort))
        {
            sort(map(dirs->name,lower_case), dirs);
            sort(map(files->name,lower_case), files);
        }
        return dirs + files;
    }

    //! returns emit#repository-dir entities for a directory entry
    mapping dir_entities(mapping entry, string dir, int id, int rip)
    {
        return ([ "name":entry->name, "icon":"dir.gif",
                  "deleted":deleted_flag_dir( rip, (int)entry->id)?"deleted":"normal",
                  "path":combine_path(dir, entry->name), "dirname":dir,
                  "modified":entry->modified, "age" : how_old(entry->modified),
                  "age-exact" : how_old(entry->modified, 1) ]);
    }

    array get_file_entities(array info, int rep_id, string dir, string branch, int id, string mod, RequestID rid )
    {
        if(!sizeof(info))
            return ({});
        string where = "repository_id="+rep_id+" AND file_id IN ("+
            info->file_id*","+")";
        where += " AND state!='removed' ";
        if(branch) where += sprintf(" AND branch='%s'", dbquote(branch));
        array last =
            debug_query("SELECT HIGH_PRIORITY concat(MAX(at),'/',l_total,'/',"
                        "l_added,'/',l_removed,'/',state,'/',"
                        "'ancestor/',revision,'/',user_id,'/',message_id,'/')"
                        " AS ugh,file_id "
                        "FROM cl_revision WHERE " + where + " GROUP BY file_id");
        mapping metadata = mkmapping(last->file_id, last->ugh);

        // set most of the entities
        array(mapping) res = allocate(sizeof(info));
        for(int i=0; i<sizeof(info); i++)
        {
            mapping file = info[i];
            res[i] =  ([ "name":file->name,
                         "icon" : "file.gif",
                         "deleted":(((int)file->parent_dir_id)!=id ?
                                    "deleted" : "normal"),
                         "path":combine_path(dir/*, (((int)file->parent_dir_id)!=id?"Attic":"")*/, file->name),
                         "dirname":dir
                      ]);
            string data = metadata[file->file_id];
            if(data)
                res[i] |= mkmapping("modified/lines/added/removed/state/"
                                    "ancestor/revision/userid/msgid" / "/",
                                    array_sscanf(data, 
                                                 "%s/%d/%d/%d/%s/%s/%s/%d/%d"));

            if( is_image( file->name ) && res[i]->revision )
            {
                string cvsroot = get_cvsroot( mod );
                res[i]->icon = do_image(cvsroot, 
                                        combine_path(mod,res[i]->path), 
                                        res[i]->revision || "HEAD",1,rid);
            }
        }


        return res;
    }
}

class TagEmitCommitters
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "committers";

    mapping process_user( mapping x, mapping a )
    {
        fix_user_info( x );
        return x;
    }

    array(mapping) get_dataset( mapping args, RequestID id )
    {
        string narrow="";
        args->pathmod_available="CONCAT(cl_repository.module,'/',path)";
        mapping con = constraints( args,id );
        string group="user_id", uriname="repository_id";
        switch( args->axis )
        {
            case "modulebranch":
                group="concat(repository_id,branch)";
                uriname = "CONCAT(uriname,'-',branch)";
                break;
            case "module":
                group="repository_id";
                uriname="uriname";
                break;
            case "branch":
                group="branch";
                uriname="branch";
                break;
            case "user":
                break;
        }
        array rows = map(debug_query( 
                             #"SELECT HIGH_PRIORITY
	    login,"+uriname+#" as uriname,COUNT(l_added) as committs,
	    SUM(l_added) as added,SUM(l_removed) as removed
         FROM " +(con->tables*"") + #"
            cl_revision, cl_user, cl_repository
         WHERE
            "+con->limit+#"
            AND cl_user.id = cl_revision.user_id 
            AND cl_repository.id = cl_revision.repository_id
         GROUP BY
            "+group),process_user,args);
        foreach( rows, mapping q )
        {
            if( !strlen(q->uriname) || q->uriname[-1] == '-' ) 
                q->uriname += "HEAD";
            q->total = (string)((int)q->added+(int)q->removed);
        }
        sort( (array(int))rows->total, rows );
        rows = reverse(rows);
        if( args->limit )
        {
            int ll = ((int)args->limit)-1;
            if( ll < sizeof( rows )  )
            {
                rows[ll]->name="Other";
                rows[ll]->uriname="Other";
                rows[ll]->login="Other";
                rows[ll]->total = (int)rows[ll]->total;
                rows[ll]->added = (int)rows[ll]->added;
                rows[ll]->removed = (int)rows[ll]->removed;
                for( int i = (int)args->limit; i<sizeof(rows); i++ )
                {
                    rows[ll]->total += (int)rows[i]->total;
                    rows[ll]->added += (int)rows[i]->added;
                    rows[ll]->removed += (int)rows[i]->removed;
                }
                rows[ll]->total = (string)rows[ll]->total;
                rows[ll]->added = (string)rows[ll]->added;
                rows[ll]->removed = (string)rows[ll]->removed;
                rows = rows[..ll];
            }
        }
        return rows;
    }
}

class TagEmitClUsers
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "cl-users";

    mapping process_user( mapping x )
    {
        fix_user_info( x );
        return x;
    }

    array(mapping) group_by_email( array(mapping) x )
    {
        sort( x->rev_email, x );
        foreach( x, mapping m )
            if( has_value( m->mailhost, "(none)" ) )
                x-=({m});
        return x;
    }

    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        string limit="";
        int not;
        if( arg->email )
        {
            if( arg->email[0] == '!' )
            {
                not = 1;
                arg->email = arg->email[1..];
            }
            limit = " WHERE login "+(not?"NOT ":"")+
                "LIKE '%@"+replace(arg->email,"'","''")+">' ";
        }
        return 
            group_by_email( map(
                                debug_query( "SELECT HIGH_PRIORITY id,login FROM cl_user "+
                                             limit),
                                process_user));
    }
}

class TagEmitClBranches
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "cl-branches";

    mapping process_user( mapping x )
    {
        fix_user_info( x );
        return x;
    }

    mapping fix_branch( mapping x )
    {
        if( x->branch == "" )
            x->branch= "HEAD";
        return x;
    }

    array(mapping) get_dataset(mapping args, RequestID id)
    {
        array repositories;
        string narrow="";
        get_repository_id(0);
        if( args->module )
            repositories = map(args->module/"|",get_repository_id);
	
        if( repositories && sizeof( repositories ) )
            if( sizeof( repositories ) > sizeof(irep_cache)/2 )
                if( !sizeof(indices(irep_cache)-repositories) )
                    narrow = " 1 ";
                else
                    narrow += " repository_id NOT IN ("+(indices(irep_cache)-repositories)*","+") ";
            else
                narrow += " repository_id IN ("+repositories*","+") ";
        else
            narrow = " 1 ";
        [int from,int to] = get_date_bounds( args->from, args->to );
        if( args->from )
        {
            narrow += " AND at>'"+time_to_string(from)+"'  ";
            if( args->to && to < time(1) )
                narrow += " AND at<'"+time_to_string(to)+"' ";
        }
        if( !args->merges )
            narrow += " AND cl_revision.type!='merge' ";
        sscanf( narrow, "%*[ ]AND%s", narrow );
        return map(
            debug_query( "SELECT HIGH_PRIORITY branch FROM cl_revision WHERE "+narrow+
                         " GROUP BY branch ORDER BY branch" ), fix_branch);
    }
}

array(int) get_date_bounds(string from_arg, string to_arg)
{
    int tz = 0;// -localtime(time(1))->timezone;
    int to = time(1), from = to - 86400;
    if(from_arg && catch(from = Calendar.dwim_time(from_arg)->unix_time()-tz))
        catch(from = Calendar.dwim_day(from_arg)->unix_time()-tz);
    if(to_arg && catch(to = Calendar.dwim_time(to_arg)->unix_time()-tz))
        catch(to = Calendar.dwim_day(to_arg)->unix_time()-tz);
    if(from_arg && !to_arg) to = from + 86400;
    if(!from_arg && to_arg) from = to - 86400;
    if( from > to )
        return ({ to, from });
    return ({ from, to });
}

mapping process_committer(mapping info)
{
    string mail = info->mailhost;
    info = ([ "login" : info->login ]);
    if(mail)
        info->mail = info->login + "@" + mail;
    return info;
}

mixed simpletag_reload_cvsviewer(mapping args, RequestID id  )
{
    return "";
}


string checkin_state( mapping r )
{
    switch(r->state)
    {
        case "changed":
            return ("(+<b class='added'>"+(r->added=r->l_added)+"</b>"
                    "/-<b class='removed'>"+(r->removed=r->l_removed)+"</b>)");

        case "added":
            r->removed = "0";
            return "<b class='added'>Added&nbsp;(+"+(r->added=r->l_total)+")</b>";

        case "removed":
            r->added = "0";
            return "<b class='removed'>Removed&nbsp;(-"+(r->removed=r->l_total)+")</b>";
    }
    return "(unknown)";
}

array filter_build_tags( array x )
{
    foreach( query( "build_tags"), string q )
        x -= glob( q, x );
    return x;
}

class TagEmitTags
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "tags";

    array(mapping) get_dataset( mapping arg, RequestID id )
    {
        array tags = get_rev_tags( arg->module, arg->path+",v" )
            [arg->revision]||({});
        if( arg->nobuild )
            tags = filter_build_tags( tags );
        return map( tags, lambda( string x ) { return ([ "name":x ]); } );
    }
}

class TagEmitRevisions
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "revisions";

    mapping process_checkin_entry(mapping checkin, mapping options)
    {
        int when = (int)checkin->time;
        mapping result = checkin;
        result->raw_message = checkin->message;
        result->message = fix_bugno(replace(qh(String.trim_all_whites(checkin->message)),
                                            "\n","<br />\n"),0);
        mapping l = localtime(when);
        result->ended = sprintf("%4d-%02d-%02d %02d:%02d:%02d", l->year+1900,
                                l->mon+1, l->mday, l->hour, l->min, l->sec );
        result->age = how_old( time(1)-when );

        string status;
        result->pathraw = checkin->path;
        result->path = replace(checkin->path,"Attic/","");


        int nobuild;
        switch( (int)options->tags )
        {
            case 1:
                nobuild=1;
            case 2:
                array tags = get_rev_tags( result->module, result->path+",v" )
                    [result->revision]||({});
                if( nobuild )
                    tags = filter_build_tags( tags );
                result->tags = String.implode_nicely( tags );
            case 0:
        }
        result->lines = checkin->l_total;
        status = checkin_state( checkin );
        result->status = status;
        return result;
    }


    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        NOCACHE(); // dynamic results, for now
        array(mapping) res;
        [mapping ba,string bc] = branch_constraint( ({arg->module}),map((arg->branch||"*")/",",String.trim_whites),id );
        if( strlen( bc ) )
            bc = "AND "+bc;
        res = debug_query("SELECT HIGH_PRIORITY UNIX_TIMESTAMP(at) as time,"
                          "state,repository_id,ancestor,revision,branch,"
                          "at,l_total,l_added,l_removed," // from cl_revision
                          "login,message," // from cl_user and cl_checkin_message
                          "name,module,mailhost " // from cl_repository
                          "FROM cl_revision LEFT JOIN cl_user"
                          " ON user_id=cl_user.id "
                          "LEFT JOIN cl_checkin_message"
                          " ON message_id=cl_checkin_message.id "
                          "LEFT JOIN cl_repository"
                          " ON repository_id=cl_repository.id "
                          "WHERE file_id=%d "
                          +bc+" "+
                          "ORDER BY at DESC", md5_56bit(arg->file));

	// DEBUG: Funny "/log.xml?file=src/peep.c&branch=*&module=pike.git"
        res=filter( res, lambda( mapping x ) { 
			     if( !x->message ) {
				 return 0;
			     }
                             return !has_value( x->message, 
                                                "was initially added on");
                         } );
        res->module = arg->module; // no need to ask for these above
        res->path = arg->file; 
        return map(res, process_checkin_entry, arg, ba);
    }
}

#ifdef DEBUG_TIME
# define TIME(Y)   werror(#Y+" %.3f\n", __timer->get() )
#else
# define TIME(Y)
#endif

#if constant(System.Timer)
#define TIMER System.Timer
#else
class TIMER
{
    float f;
    int t;

    float get()
    {
        int q = time();
        float qf = time(q);
        float res = q-t + qf-f;
        [t,f]=({q,qf});
        return res;
    }

    void create()
    {
        t = time();
        f = time(t);
    }
}
#endif

array(string) split_to_words( string x )
{
    // ignore html
    string res="";
    Parser.HTML p = Parser.HTML();
    p->_set_data_callback(lambda( Parser.HTML p, string data )
                          {
                              res += data + " ";
                          });
    p->finish( x );
    constant from = "/$()[]{}.,:;"/"";
    res = replace(res, from, ({" "})*sizeof(from));
    return replace(res,(["\t":" ", "\n":" "])) / " " - ({""});
}

string bug_id( string key )
{
    mapping b = get_bug( key );
    if( b ) return b->no || b->id;
    return key;
}

string fix_bugno( string x, int bugs_only )
{
    string pre, keyword;
    string fixed="";
    string bugs="";
    string ox = x;

    pre = "";
    mapping replaces = ([]);
    while( sscanf( ox, "%s[%[^] ]]%s", pre, keyword, ox) == 3 )
        replaces["["+keyword+"]"] = 
            "<a href='index.xml?start=forever&message=["+keyword+"]'><span id='keyword'>["+keyword+"]</span></a>";

    string prev_word;
    foreach( split_to_words(x), string word )
    {
        if( string key = is_bugid( word, prev_word ) )
        {
            string bugid = bug_id(key);
	    if( !bugid )
	    {
		werror("WARNING: %O returned key %O, but there was no bugid for for that key.\n", word, key);
		prev_word = word;
		continue;
	    }
            if( !strlen( bugid ) ) {
                prev_word = word;
                continue; // not really a bug.
	    }
            mapping bug = get_bug( bugid );
            if( !bug || !bug->summary ) {
                prev_word = word;
	        continue;
	    }

            bugs+=bugid+",";
	    // NB: Override the bracket notation added for the keyword
	    //     above (if any).
	    replaces["[" + word + "]"] = replaces[word] =
	      sprintf("<a href=%O id=%O title=\"%s\" class=bug><nobr>%s</nobr></a>",
		      bug->url,
		      lower_case(bug->status),
		      qh(bug->summary+" ("+bug->status+")"),
		      (word != bugid && ((int)bugid || (int)bugid[1..]) &&
		       !has_value(lower_case(x),lower_case(bugid))) ?
		      word + " ("+bugid+")" : word);
	    // NB: The following entry does not seem to be used.
            replaces["$"+key] = 
                sprintf("<a class=bug href=%O id=%O>%s</a>", 
                        bug->url, 
                        lower_case(bug->status),
                        sprintf("<nobr><img height=12 src=%O>%s:</nobr> %s",
                                bug->icon,
                                bugid,
                                qh(bug->summary)));
        }
	prev_word = word;
    }
    if( bugs_only ) return bugs;
    return replace( x, replaces );
}


string my_trim_whites( string x)
{
    string res = "";
    foreach( x/"\n", string y )
    {
        y = reverse(y);
        sscanf(y, "%*[ ]%s", y );
        res += reverse(y)+"\n";
    }
    return res;
}

string format_release_msg_entry( string bullet, string login, string msg )
{
    return my_trim_whites(sprintf( bullet+" ("+login+") %=-"+(72-strlen(login)-4)+"s",
                                   replace(replace(replace(msg,"\n\n","\1"),"\n"," "),
                                           "\1","\n\n")));
}
string indent( string x, string ind )
{
    string res = "";
    foreach( x /"\n", string z )
        res += ind+z+"\n";
    return res;
}
string format_release_msg( string bullet, string msg, string login )
{
    string a, b, c;
    if( sscanf( msg, "%s* (%s) %s", a, b, c ) == 3 && 
        (!strlen(a) || (<'\n',' ','\t','\r'>)[a[-1]]) )
    {
        return format_release_msg_entry(bullet,login,String.trim_all_whites(a)) 
            + indent("* ("+b+") "+String.trim_all_whites(c), "     ")+"\n";
    }
    if( sscanf( msg, "(%s)%s", login, msg ) == 2 )
        msg = String.trim_whites( msg );

    return format_release_msg_entry(bullet,login,msg);
}
mapping rep_cache = ([]);
mapping irep_cache = ([]);
string get_repository_id( string x )
{
    if( rep_cache[x] )
        return rep_cache[x];
    array q;
    if( !x )
    {
        q = debug_query( "SELECT HIGH_PRIORITY uriname,id FROM cl_repository", x );
        rep_cache = mkmapping( q->uriname, q->id );
        irep_cache = mkmapping( q->id, q->uriname );
    }
    else
    {
        q = debug_query( "SELECT HIGH_PRIORITY id FROM cl_repository WHERE uriname=%s", x )->id;
        if( !sizeof( q ) )
            return "0";
	irep_cache[q[0]] = x;
        return rep_cache[x] = q[0];
    }
}

array branch_cache;

string module_branch_constraint( array(string) modules, array(string) branches )
{
    string rep_lim, br_lim;

    if( !modules || !sizeof(modules) )
        rep_lim="";
    else if( sizeof( modules ) == 1 )
        rep_lim="repository_id="+modules[0];
    else
    {
        if( sizeof( modules ) > sizeof(irep_cache)/2 )
        {
            modules = (indices(irep_cache)-modules);
            if( !sizeof( modules ) )
                rep_lim = "1";
            else
                rep_lim = "repository_id NOT IN ("+modules*","+")";
        }
        else
            rep_lim="repository_id IN ("+modules*","+")";
    }
    if( !branches || !sizeof(branches) )
        br_lim="";
    else if( sizeof( branches ) == 1 )
        br_lim="branch='"+branches[0]+"'";
    else
        br_lim="branch IN ("+map(map(branches,dbquote),
                                 lambda(string f){return "'"+f+"'";})*","+")";


    if( strlen( rep_lim ) && strlen(br_lim) )
        return "("+rep_lim+" AND "+br_lim+")";
    if( strlen( rep_lim ) )
        return rep_lim;
    return br_lim;
}

array(mapping|string) branch_constraint( array(string) modules, 
                                         array(string) branches, 
                                         RequestID id )
{
    array res = ({ ([]), ({}) });
    mapping module_branches = ([]);
    mapping rev = ([]);

    foreach( branches, string branch )
    {
        if( branch == "*" ) {
            return ({ ([]), module_branch_constraint( map(modules,get_repository_id),0) }); // All
        }
        if( !branch_cache )
            branch_cache=debug_query( "SELECT branch FROM cl_revision GROUP BY branch" )
                ->branch;
// 	    ERROR("DEBUG: Target branch %s\n", q );
        array z = glob( branch, branch_cache );
        if( !sizeof( z ) )
        {
            ERROR("No branches matching '%s'\n", branch );
            z = ({ branch });
        }
        foreach( z, string br )
        {
// 	    ERROR("DEBUG: glob-match: %s\n", br );
            foreach( modules, string module )
            {
                string rep_id = get_repository_id(module);
                array(string) list = ({""});
                if( br != "HEAD" )
                    list = ({br});
                if( !module_branches[rep_id] )
                    module_branches[rep_id]=list;
                else
                    module_branches[rep_id]|=list;
            }
        }
    }

    mapping grouped = ([]);
    foreach( module_branches; string rep_id; array(string) branches )
    {
        branches = Array.uniq( sort(branches) );
        string brid = branches*"|";
        if( grouped[brid] )
            grouped[brid][0] += ({ rep_id });
        else
            grouped[brid] = ({ ({rep_id}), branches });
    }

    array(string) lim = ({});
    rev[0] = ({});
    get_repository_id(0);
    foreach( grouped; ; array group )
    {
        lim += ({ module_branch_constraint( @group ) });
        if( !strlen( lim[-1] ) )
            return ({ rev, "" });
        if( sizeof( group[-1] - branches ) )
            rev[0] += ({String.implode_nicely( sort(map( group[0], irep_cache )) )+": "+
                        String.implode_nicely( sort(replace(group[-1]-branches,"","HEAD")) )});
    }
    if( sizeof( lim ) == 1 )
        return ({rev, lim[0]});
    if( !sizeof( lim ) )
        return ({ rev, "" });
    return ({rev, "("+lim*" OR "+")"});
}

mapping constraints( mapping arg, RequestID id )
// One querygenerator to rule them all and in the abstraction bind
// them.
{
    array(string) limit = ({});
    mapping branch_rev = ([]);
    array(string) tables = ({});
    int from, to;

    if( arg->checkins && sizeof(arg->checkins) )
    {
        array in = ({});
        foreach( replace(arg->checkins,"|",",")/",", string q )
        {
	  //            if( strlen(q) == 32 )
                catch(in += ({"'"+dbquote(q)+"'"}));
        }
      limit += ({ "revision in ("+in*","+")" });
    }
    else
    {
        if( arg->branch ) 
        {
            [branch_rev,string cl] =
                branch_constraint( (arg->module?arg->module/"|":({})),
                                   map((arg->branch||"*")/",",String.trim_whites),id );
            if( strlen( cl ) )
                limit += ({cl});
        }

        if( arg->user )
            if( (int)arg->user )
                limit += ({ "user_id in ("+(arg->user/"\0")*", "+")" });
            else if(!arg->users)
                arg->users = arg->user;
    
        if( arg->users )
        {
            array(int) get_userid( string pattern )
            {
                if( has_value( pattern, "@" ) )
                    return (array(int))
                        debug_query( "SELECT id FROM cl_user WHERE login like %s",
                                     replace(pattern,"*","%"))->id;
                return (array(int))
                    debug_query( "SELECT id FROM cl_user WHERE login like %s",
                                 replace(pattern,"*","%")+"@%")->id;
            };
        
            array(int) these = ({});
            array(int) not_these = ({});
            foreach( arg->users/",", string user )
            {
                if( !strlen( user ) ) 
                    continue;
                if( user[0] == '~' || user[0]=='!' )
                    not_these += get_userid(user[1..]);
                else
                    these += get_userid( user );
            }
            if(sizeof(these))
                limit += ({ "user_id in ("+(array(string))these*","+")" });
            if( sizeof( not_these ) )
                limit += ({ "user_id not in ("+(array(string))not_these*","+")" });
        }

        if( arg->bugglob )
        {
            array ids = 
                debug_query( "SELECT id,message FROM cl_checkin_message WHERE "+
                             "MATCH(message) AGAINST(%s IN BOOLEAN MODE)", arg->bugglob);
        
            array tmp = ({});
            string x = upper_case( arg->bugglob );
            array bugs = fix_bugno(upper_case(arg->bugglob),1)/","-({""});
            foreach( ids, mapping row )
            {
                string bb = fix_bugno( row->message, 1 );
                if( sizeof( (bb/","-({""})) & bugs ) )
                    tmp += ({ row->id });
            }
            ids = tmp;
            if( sizeof( tmp ) )
                limit += ({"message_id IN ("+tmp*","+")"});
            else
            {
                ERROR("There are no messages mentioning the bug '%s'",  arg->bugglob);
                limit += ({"message_id=-1"}); // not likely. :-)
            }
        }

        if( !arg->fromtag )
            [from,to] = get_date_bounds(arg->from, arg->to);



        if( arg->messageglob )
        {
            if( (strlen( arg->messageglob-"*" ) > 2) )
            {
                string extra;
                int negative;
                if( arg->messageglob[0] == '[' && arg->messageglob[-1] == ']' )
                {
                    // keyword mode.
                    werror("keyword mode: %O\n", arg->messageglob);
                    extra = ",message";
                }
                if( arg->messageglob[0] == '-' && (sizeof(arg->messageglob/" ")==1) || arg->messageglob[0] == '~' )
                {
                    negative = 1;
                    arg->messageglob = arg->messageglob[1..];
                }
                array ids = debug_query( "SELECT id"+(extra||"")+" FROM cl_checkin_message WHERE "+
                                         "MATCH(message) AGAINST(%s IN BOOLEAN MODE)", arg->messageglob);
	  
                if( extra )
                {
                    array tmp = ({});
                    string x =lower_case( arg->messageglob );
                    foreach( ids, mapping row )
                        if( has_value( lower_case( row->message), x ) )
                            tmp += ({ row->id });
                    ids = tmp;
                }
                else
                    ids = ids->id;
                if( sizeof( ids ) )
                    limit += ({"message_id "+(negative?"NOT ":"")+"IN ("+ids*","+")"});
                else
                {
                    if( !negative )
                    {
                        ERROR("There are no messages matching '%s'",  arg->messageglob);
                        limit += ({"message_id=-1"}); // not likely. :-)
                    }
                }
            }
        }

        string rep_limit="", time_limit="";

        if( !arg->merges )
            limit += ({"cl_revision.type!='merge'"});

        if( from||to )
        {
            limit += ({"'"+time_to_string(from)+"'<at"});
            if( to < time() )
                limit += ({"'"+time_to_string(to)+"'>=at"});
        }
        if( arg->path )
        {
            array not = ({});
            array must = ({});
            foreach( replace(arg->path,"|",",")/",", string x )
            {
                x = String.trim_all_whites(x);
                x = replace(x,(["*":"%","?":"_"]));
                if( !strlen( x) ) continue;
                if( x[-1] == '/' )
                    x+="%";
                if( x[0] == '!' )
                    not += ({x[1..]});
                else
                    must += ({ x });
            }
            string path = arg->pathmod_available || "path";
            foreach( not, string x )
                limit += ({ "("+path + " NOT LIKE '"+dbquote(x)+"')" });
            array(string) mm = ({});
            foreach( must, string y )
                mm += ({"("+path+" LIKE '"+dbquote(y)+"')"});
            if( sizeof( mm ) )
                limit += ({"("+mm* " OR "+")"});
        }
    }
    if(sizeof(tables))
        tables+=({""});

    return ([
        "limit":sizeof(limit)?(limit*" AND "):"1",
        "tables":tables,
        "from":from,
        "to":to,
        "branch_rev":branch_rev,
    ]);
}

static mapping(string:array(string)) user_info = ([]);

mapping|string try_decode_utf8( string|mapping x )
{
  if( mappingp( x ) )
  {
    foreach( x; string key; mixed val )
      if( stringp( val ) )
	x[key] = try_decode_utf8(val);
  }
  else 
    for( int i=0; i<2; i++ )
    {
      catch(x=utf8_to_string(x));
    }
  return x;
}

//! @returns
//!   @array
//!     @item string login
//!     @item string|void real_name
//!     @item string|void mailhost
//!   @endarray
array(string) get_user_info( string x )
{
    if( !x ) 
        return ({ "unknown user", "unknown", "unknown" });
    if( user_info[ x ] )
        return user_info[ x ];
    string login = x, real_name, mailhost;
    // First check if we've already got everything in @[login].
    if (sscanf(login, "%s<%s>", real_name, login) == 2) {
        real_name = String.trim_all_whites(real_name);
        login = String.trim_all_whites(login);
        if (real_name == login) {
	    real_name = 0;
        }
    } else {
        real_name = 0;
    }
    sscanf(login, "%s@%s", login, mailhost);
    if (!real_name) 
    {
        User u, tmp;
        foreach( my_configuration()->user_databases(), UserDB udb )
        {
            if( tmp = udb->find_user( login ) )
                u = tmp;
        }
        if( u )
        {
            real_name = u->real_name();
        }
    }
    return user_info[x] = ({
      login, real_name || login, mailhost
    });
}

void fix_user_info(mapping(string:string) ci)
{
  [ci->login, ci->user, string mailhost] = get_user_info( ci->login );
  // Prefer the mailhost from the login entry (if any)
  // to the one from the repository definition (if any)
  // to the default.
  ci->mailhost =
    mailhost || ci->mailhost || query("default_mailhost");
  ci->rev_email = ci->mailhost + "@" + ci->login;
  ci->email = ci->login+"@"+ci->mailhost;
}

string paragraphize( string x )
{
    return replace(x,"\n", "<br>\n");
}

string encode_high( string x )
{
    mapping repl = ([]);
    foreach( (array)x, int z )
    {
        if( z < 32 || z > '~' )
            repl[sprintf("%c",z)] = sprintf("\\u%04x",z);
    }
    if( sizeof( repl ) )
        return replace( x, repl );
    return x;
}


string encode_json(mixed msg)
{
    String.Buffer res = String.Buffer();
    low_encode_json(res,msg,2);
    return res->get();
}

#define NL() res->add( "\n"+" "*indent)
void low_encode_json(String.Buffer res, mixed msg, int indent)
{
    if (stringp(msg)) 
    {
        res->add("\"", 
                 encode_high(replace(msg,
                         ([ "\"" : "\\\"",
                            "\\" : "\\\\",
                            "\n" : "\\n",
                            "\b" : "\\b",
                            "\f" : "\\f",
                            "\r" : "\\r",
                            "\t" : "\\t" ]))), "\"");
    }
    else if (arrayp(msg)) 
    {
        res->putchar('[');
        foreach(msg; int i; mixed val) {
            if (i)
                res->putchar(',');
            NL();
            low_encode_json(res, val, indent+2);
        }
        indent-=2;
        if( sizeof( msg ) )
            NL();
        res->putchar(']');
    } 
    else if (mappingp(msg)) 
    {
        res->putchar('{');
        foreach(sort(indices(msg)); int i; string ind) {
            if (i) res->putchar(',');
            NL();
            low_encode_json(res, ind, indent+2);
            res->putchar(':');
            low_encode_json(res, msg[ind], indent+2);
        }
        indent-=2;
        if( sizeof( msg ) )
            NL();
        res->putchar('}');
    }
    else 
    {
        res->add((string)msg);
    }
}
#undef NL

class TagEmitCheckins
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "checkins";

    array(array(mapping)|mapping) process_checkin_entry(mapping checkin, mapping options)
    {
        array(mapping) raw_files = checkin->files;
        mapping xx = raw_files[0];
        int when = checkin->time;

        mapping result = ([ 
            "login":xx->login, 
            "module":(xx->module||"")-"\0",
            "user":xx->user, 
            "user_id":xx->user_id, 
            "raw_message":xx->message,
            "time":when,
            "mailhost":xx->mailhost,
            "revision":raw_files[0]->revision,
        ]);

  
        if( options->releasenotes ) 
            result->message = format_release_msg( options->releasenotes,
                                                  String.trim_all_whites(xx->message), 
                                                  xx->login );
        else
            result->message = fix_bugno(paragraphize(qh(String.trim_all_whites(xx->message))),0);

        result->bugfixes = fix_bugno( result->message, 1 );
        mapping l = localtime(when);
        result->ended = sprintf("%4d-%02d-%02d %02d:%02d:%02d", l->year+1900,
                                l->mon+1, l->mday, l->hour, l->min, l->sec );
        result->age = how_old( time(1)-when );

        m_delete( result, "files" );
        string status, path;
        sort(raw_files->path, raw_files);
        foreach(raw_files, mapping r)
        {
            if(!r->module)raw_files -=({r});
            r->time = r->at;
            r->pathraw = r->path;
            r->path = path=replace(r->path,"Attic/","");
            r->lines = r->l_total;
            status = checkin_state( r );
            r->status = status;
        }
        return ({raw_files,result});
    }

    function symbol_is_branch = Regexp("\\.0\\.[0-9]*[02468]$")->match;

    //! Associate the individual files of multi-file commits to one another
    array assoc_commits(mapping asoc, mapping branch_rev, mapping arg,
                        array res)
    {
        mapping l = localtime(time(1));
        int tz = l->timezone;
        array checkins = ({});
        foreach(res, mapping ci)
        {
            if( !ci->message )
                ci->message = "No message";
            if( has_value( ci->message, "was initially added on branch" ) )
                continue;
            if( branch_rev[ ci->branch ] )
                ci->branch = branch_rev[ ci->branch ];
            fix_user_info(ci);
            ci->module = irep_cache[ci->repository_id];
            string id= ci->revision; // ci->login + ci->message;

            // if( !arg->releasenotes )
            //     id += ci->branch;
            // if( arg->sortmodule )
            //     id += ci->module;
	    
            int t1 = (int)ci->time_t - tz;

            if(!asoc[id]) {
                // checkins += ({ asoc[id] });
                asoc[id] = ([ "time":t1, "files":({ci}), "module":ci->module]);
            }
            else 
            {
                int t2 = asoc[id]->time || t1;
                if(arg->releasenotes || abs(t1-t2) < 3600)
                {
                    asoc[id]->time = max(t2,t1);
                    asoc[id]->files += ({ ci });
                }
                else
                {
                    checkins += ({ asoc[id] });
                    // Note: Module member only realy valid if arg->sortmodule is true.
                    asoc[id] = ([ "time":t1, "files":({ ci }), "module":ci->module ]);
                }
            }
        }
        // werror("assoc_commits(..., array(%d)) ==> array(%d)\n", sizeof(res), sizeof(checkins));
        return checkins;
    }

    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        id->misc->_cvsview_error="";
        TIMER __timer = TIMER();
        NOCACHE();                    // dynamic results, for now
        array(mapping) res, checkins = ({});
        mapping branch_rev;
        string current_user = id->misc->authenticated_user ?
            id->misc->authenticated_user->name() : "anonymous" ;
        // Admin actions
        if( admin_users[current_user] )
        {
            int not;
            string what;
            if( (what=id->variables->mark_as_merge) || 
                ((what=id->variables->mark_as_not_merge)&&(not=1)) )
            {
                string module, file, rev;
                if( sscanf( what, "%s|%s|%s", module, rev, file ) == 3 )
                {
                    module = get_repository_id( module );
                    debug_query( "UPDATE cl_revision SET type=%s "
                                 "WHERE file_id=%d AND "
                                 "      repository_id=%s AND revision=%s",
                                 (not?"normal":"merge"),
                                 md5_56bit( file ), module, rev );
                }
            }
        }


        string find_next_build_tag( string tagname, int diff )
        {
            array parts = tagname /"_";
            if( (int)parts[-1] )
            {
                parts[-1] = (string)(((int)parts[-1])+diff);
                return parts*"_";
            }
            return "HEAD";
        };
        int nrevs;
        int limited;
        mapping(string:mapping(string:mixed)) asoc = ([]);
        if( arg->fromtag || arg->totag )
        {
            if( arg->fromtag == "-4 days" ) 
                arg->fromtag = "";
            if( !arg->totag || !strlen( arg->totag ) )
                arg->totag = find_next_build_tag( arg->fromtag, 1 );
            if( !arg->fromtag || !strlen( arg->fromtag ) )
                arg->fromtag = find_next_build_tag( arg->totag, -1 );

            int t1 = gethrtime();
            multiset modules;
            array(string) git_dirs = ({});
            if( arg->module ) {
                modules = (multiset)(arg->module/"|");
                foreach(modules; string mod;) {
                    string cvsroot = get_cvsroot(mod);
                    if (has_prefix(cvsroot, ":git::")) {
                        modules[mod] = 0;
                        git_dirs += ({ cvsroot[6..] });
                    }
                }
            } else {
                git_dirs =
                    debug_query("SELECT cvsroot_path FROM cl_repository "
                                " WHERE cvsroot_scheme = ':git:'")->cvsroot_path;
                // FIXME: Check if there are any other repositories, so that
                //        we won't need to scan CVS tags further below.
            }

            mapping con = constraints( arg,id );
            branch_rev = con->branch_rev;

            foreach(git_dirs, string git_dir) {
                // Git: Get the list of revisions between the two tags
                //      (including the revision for the from tag).
                Stdio.File pipe = Stdio.File();
                Process.create_process(({ query("gitbin"),
                                          "--git-dir="+git_dir,
                                          "rev-list",
                                          arg->fromtag + "^.." +
                                          arg->totag }),
                                       ([ "stdout" : pipe->pipe() ]));
                string revisions = pipe->read();
                array(string) revs = revisions/"\n" - ({""});
                // Fetch the commits corresponding to the revisions.
                foreach(reverse(revs), string rev) {
                    array res =
                        debug_query("SELECT HIGH_PRIORITY repository_id,path,state,ancestor,revision,"
                                    "branch,"
                                    "user_id,at,UNIX_TIMESTAMP(at) as time_t,l_total,l_added,l_removed,"
                                    "cl_revision.type as merge," // from cl_revision
                                    "login,message, "          // from cl_user and cl_checkin_message
                                    "mailhost\n"	// from cl_repository
                                    "FROM "+ con->tables*","
                                    +"cl_revision LEFT JOIN cl_user\n"
                                    " ON user_id=cl_user.id\n"
                                    "LEFT JOIN cl_repository\n"
                                    " ON repository_id=cl_repository.id\n"
                                    "LEFT JOIN cl_checkin_message\n"
                                    " ON message_id=cl_checkin_message.id\n"
                                    "WHERE ("+con->limit+") AND (\n"
                                    "revision = %s)", rev);
                    // Note: No need for ORDER BY above, since we're only looking
                    //       at one revision at a time.
                    checkins += assoc_commits(asoc, branch_rev, arg, res);
                }

                // The git commits are already present in asoc.
                res = ({});
            }
            if (!modules || sizeof(modules)) {
                // There are CVS repositories among the modules.
                mapping ftag = ([]);
                mapping ttag = ([]);
                string mquote( string x ) { return "'"+replace(x,"'","''")+"'"; };

                if( !sizeof(ttag) ) 
                {
                    if( lower_case(arg->totag) == "head"  )
                    {
                        mapping modfiles = ([]);
                        ttag = copy_value(ftag);
                        foreach( ftag; string file; string v )
                        {
                            string mod, fn;
                            if( sscanf( file, "%[^/]/%s,v", mod, fn ) == 2 && 
                                (!modules||modules[mod]))
                            {
			    
                                array tmp = 
                                    debug_query( "SELECT HIGH_PRIORITY "
                                                 "repository_id,revision,at,state FROM cl_revision "
                                                 "  LEFT JOIN cl_repository ON repository_id=cl_repository.id "
                                                 " WHERE "
                                                 "  (cl_repository.module="+mquote(mod)+" AND path="+
                                                 mquote(fn)+" AND branch='')"
                                                 " ORDER BY at DESC LIMIT 1");
                                if( sizeof( tmp ) )
                                    ttag[file] = tmp[0]->revision;
                                else
                                    ERROR("%O somehow not found on head",file);
                            }
                        }
                    }
                }
                if( !sizeof(ftag) )
                {
                    ERROR("No files with the tag %O found", arg->fromtag );
                    return ({});
                }
                if( !sizeof(ttag) )
                {
                    ERROR("No files with the tag %O found", arg->totag );
                    return ({});
                }
                if( !sizeof( ftag & ttag ) )
                {
                    ERROR("No files with both tags found" );
                    return ({});
                }
                array files = map( indices(ftag)|indices(ttag), 
                                   lambda(string x) { 
                                       sscanf( x, "%s,v", x );
                                       return x;
                                   });
                t1 = gethrtime();
                // // // // // First find a list of files.
                arg->pathmod_available="CONCAT(cl_repository.uriname,'/',path)";
                mapping(string:array) mylimit = ([]);
                mapping processed = ([]);

                if( strlen( arg->path||"" ) )
                    files = filter( files, lambda( string x ) {
                                               foreach( arg->path/",", string pat )
                                               {
                                                   pat = String.trim_all_whites(pat);
                                                   if( !strlen( pat ) )
                                                       continue;
                                                   if( pat[0] == '^' || pat[0] == '!')
                                                       return !glob( pat[1..], x );
                                                   return glob( pat, x );
                                               }
                                           });
                string last_error;
                mapping(string:string) minfroms = ([]);
                foreach( files, string x )
                {
                    if( processed[ x ]++ )
                        continue;
		
                    string mod, file;
                    sscanf( x, "%[^/]/%s", mod, file );
                    if( modules && !modules[mod] ) continue;
                    string to = ttag[x+",v"];
                    string from = ftag[x+",v"];
                    int from_nonexist=0;

                    if( !to && !from )
                        continue;

                    if( !to )
                    {
                        to = from+"9";
                        ERROR("Warning %s only has the start-tag (%s, %s), the end-tag (%s) is not present. Ignoring revisions after %s.\n",x,
                              arg->fromtag,from, arg->totag,arg->fromtag);
                        continue;
                    }
// 		if( !from )
// 		{
// 		    string w = to;
// 		    array z = w/".";
// 		    from=z[..sizeof(z)-2]*".";
// 		    sscanf( from, "%s.0", from );
// 		    from += ".1";
// 		    from_nonexist=1;
// 		}
                    if( from && symbol_is_branch( from ) )
                    {
                        from -= ".0";
                        from += ".1";
                    }
                    if( symbol_is_branch( to ) )
                    {
                        to -= ".0";
                        if( from && has_prefix( to, from ) ) // fromtag is closer to the trunk
                        {
                            from = to+".1";
                        }
                        // Find last checkin on branch
                        array tmp = debug_query( "SELECT HIGH_PRIORITY repository_id,revision,at,state FROM cl_revision "
                                                 "  LEFT JOIN cl_repository ON repository_id=cl_repository.id "
                                                 " WHERE "
                                                 "  (cl_repository.module='"+mod+"' AND path='"+file+"' AND branch='"+arg->totag+"')"
                                                 " ORDER BY at DESC LIMIT 1");
                        if( sizeof( tmp ) )
                        {
                            if( tmp[0]->state == "removed" && !from )
                                continue;
                            to = tmp[0]->revision;
                        }
                    }
                    if( from == to ) continue;
                    if( !from )
                    {
                        ERROR("Warning: %s only has the end-tag (%s), the start-tag (%s) is not present.\n",
                              x,arg->totag,arg->fromtag);
                        continue;
                    }
                    mapping msg = ([ "file":x ]);
                    array z=treewalk( from, to, msg );
                    if( msg->error && msg->error != last_error )
                    {
                        last_error = msg->error;
                        ERROR(msg->error);
                    }
		
                    if( sizeof( z ) )
                    {
                        if( !mylimit[mod] )
                            mylimit[mod] = ({});
                        nrevs += sizeof(z);
                        if( sizeof( z ) == 1 )
                        {
                            mylimit[mod]  += ({ "(file_id="+md5_56bit(file)+" AND revision="+mquote(z[0])+")" });
                        }
                        else
                        {
                            mylimit[mod] += ({"(file_id="+md5_56bit(file)+" AND revision IN ("+map(z,mquote)*","+"))" });
                        }
                    }
                }
                werror("finding revs done (%d files, %d revs) (%.1fs)\n", 
                       Array.sum(sizeof(values(mylimit)[*])),
                       nrevs, (gethrtime()-t1)/1000000.0);

                if( sizeof( mylimit ) )
                {
                    string query="";
                    res = ({});
                    foreach( mylimit;string m; array y )
                    {
                        if( !strlen( query ) )
                            query =
                                "SELECT HIGH_PRIORITY repository_id,path,state,ancestor,revision,"
                                "branch,"
                                "user_id,at,UNIX_TIMESTAMP(at) as time_t,l_total,l_added,l_removed,"
                                "cl_revision.type as merge," // from cl_revision
                                "login,message, "          // from cl_user and cl_checkin_message
                                "mailhost\n"
                                "FROM "
                                +(con->tables*",")+
                                "cl_revision LEFT JOIN cl_user\n"
                                " ON user_id=cl_user.id\n"
                                "LEFT JOIN cl_repository\n"
                                " ON repository_id=cl_repository.id\n"
                                "LEFT JOIN cl_checkin_message\n"
                                " ON message_id=cl_checkin_message.id\n"
                                "WHERE ("+con->limit+") AND (\n";
                        else
                            query += " OR ";
                        query += "\n   (repository_id='"+get_repository_id(m)+"' AND ("+y*"\n                     OR " + ")" + ")";
                        if( strlen( query ) > 1024*1024 )
                        {
                            res += debug_query( query+") ORDER BY at DESC" );
                            query="";
                        }
                    }

                    if( sizeof( res ) )
                        ERROR("Warning: Too large query for MYSQL, ordering might be incorrect.\n");
                    if( strlen( query ) )
                        res += debug_query( query+") ORDER BY at DESC" );
                    if( !sizeof( res ) )
                    {
                        ERROR("Warning: Could not find any relevant checkins, but some revisions did match your query\n");
                    }
                }
                else
                {
                    ERROR("Warning: No revisions found that match those tags\n");
                    ERROR("  %O on %d file%s\n", arg->fromtag, sizeof(ftag), sizeof(ftag)==1?"":"s" );
                    array a = indices(ftag);
                    foreach( sort( a )[..10], string file )
                        ERROR("    %O points to %O\n", file, ftag[file] );
                    ERROR("  %O on %d file%s\n", arg->totag, sizeof(ttag), sizeof(ttag)==1?"":"s" );
                    a = indices(ttag);
                    foreach( sort( a )[..10], string file )
                        ERROR("    %O points to %O\n", file, ftag[file] );		res = ({});
                }
            }
        }
        else
        {
            if( arg->path && strlen(arg->path) )
                arg->pathmod_available="CONCAT(cl_repository.module,'/',path)";
            mapping con = constraints( arg,id );
            branch_rev = con->branch_rev;
            //	    werror("Limit: %O\n", con->limit );
            res = debug_query("SELECT HIGH_PRIORITY repository_id,path,state,ancestor,revision,"
                              "branch,"
                              "user_id,at,UNIX_TIMESTAMP(at) as time_t,l_total,l_added,l_removed,"
                              "cl_revision.type as merge," // from cl_revision
                              "login,message, " // from cl_user and cl_checkin_message
                              "mailhost " // from cl_repository
                              "FROM "
                              +(con->tables*",")+
                              "cl_revision LEFT JOIN cl_user"
                              " ON user_id=cl_user.id "
                              "LEFT JOIN cl_repository"
                              " ON repository_id=cl_repository.id "
                              "LEFT JOIN cl_checkin_message"
                              " ON message_id=cl_checkin_message.id "
                              "WHERE "+ con->limit + " ORDER BY at DESC LIMIT "+
                              (arg->maxrows||"200000"));

            if( sizeof( res ) == 200000 )
                limited = 1;
        }

        werror("%s: Query: %d rows matched [%O/%O - %O/%O @ %O] (%.1fs)\n", 
               current_user, sizeof( res ), arg->fromtag, arg->from, arg->totag, arg->to, 
               arg->branch,  __timer->get() );
        // 	TIME(query);

        // associate the individual files of multi-file commits to one another here
        checkins += assoc_commits(asoc, branch_rev, arg, res) + values( asoc );

        sort(checkins->time, checkins);
        if( arg->sortmodule )
        {
            sort(checkins->module, checkins);
            checkins = reverse( checkins );
        }
        TIME(asoc);

        array foo=map(reverse(checkins), process_checkin_entry, arg);
	checkins = 0;

        array committed_files = column(foo,0);
        TIME(map);

        string allbugs = "";
        string today = Roxen.strftime( "%A %d %B %Y", time(1) );
        string yesterday =Roxen.strftime( "%A %d %B %Y", time(1)-3600*24 );
        string day_long( int x )
        {
            string res = Roxen.strftime( "%A %d %B %Y", x );
            if( res == today )
                return "Today; "+res;
            if( res == yesterday )
                return "Yesterday; "+res;
            return res;
        };

        string make_ci_hash( array(mapping) ci )
        {
            array res = ({});
            foreach( ci, mapping e )
                res += ({e->revision});
            res = sort(Array.uniq(res));
            return res*",";
        };


        if( arg->format == "json" )
        {
            array entries = ({});
            mapping users = ([]);
            string make_user( mapping ee, int id )
            {
                if( !users[id] )
                {
                    users[id] = ([
                        "login":ee->login,
                        "host":ee->mailhost,
                        "id":id,
                        "name":ee->user,
                    ]);
                }
                return (string)id;
            };
            string allbugs="";
            foreach( foo, [array files, mapping ci] )
            {
                if(!sizeof(files)) continue;
                
                mapping jse = ([
                    "time":ci->time*1000,
                    "user":make_user(ci,(int)ci->user_id),
                    "id":make_ci_hash(files),
                    "files":({}),
                    "message":ci->message,
                    "bugs":(ci->bugfixes/","-({""})),
                    "raw_message":ci->raw_message,
                ]);
                allbugs += ci->bugfixes;

                foreach( files, mapping ci )
                    jse->files += ({
                        ([
                            "state":ci->state,
                            "added":(int)ci->added,
                            "removed":(int)ci->removed,
                            "ancestor":ci->ancestor,
                            "revision":ci->revision,
                            "module":ci->module,
                            "path":ci->pathraw,
                            "branch":ci->branch,
                        ])
                    });
                
                entries += ({ jse });
            }
            string json = encode_json(  ([ "users":users, "log":entries ])  );
            if( arg->trim )
                json = json[1..sizeof(json)-2];
            return ({
                ([
                    "alles":json,"allbugs":allbugs
                ])
            });
        }

        if( arg->format == "default" /*&& sizeof(foo)*/ )
        {
            String.Buffer res = String.Buffer( sizeof(foo)*300 );
            function add = res->add;

            string allbugs = "";
            string today = Roxen.strftime( "%A %d %B %Y", time(1) );
            string yesterday =Roxen.strftime( "%A %d %B %Y", time(1)-3600*24 );
            string day_long( int x )
            {
                string res = Roxen.strftime( "%A %d %B %Y", x );
                if( res == today )
                    return "Today; "+res;
                if( res == yesterday )
                    return "Yesterday; "+res;
                return res;
            };

            if( limited )
                add( "<h1>Warning: Query limited to 200.000 hits</h1>\n" );

            string day = "";
#define HTTP(X) Roxen.http_encode_invalids((X)||"")
#define HTML(X) Roxen.html_encode_string((X)||"")
            string person = query("person"), viewcvs=query("viewcvs");
            string personal_link(string u, string m)
            {
                return "<a href='"+sprintf(person,u,m)+"'>";
            };
            string viewcvs_link(string m, string p, string r1, string r2)
            {
                return sprintf(viewcvs,m,p,r1,r2);
            };

            string generate_join_args( array(mapping) files )
            {
                array res = map(files,lambda(mapping q){return ({ q->module+"/"+q->path, (q->state=="added"?0:q->ancestor),
                                                                  (q->state=="removed"?0:q->revision) });});
                string q = Gz.deflate(9)->deflate(encode_value( res ));
                return "v="+Gmp.mpz(q,256)->digits(36);
            };

            string old_user;
            int old_time;
            string joinargs;
            array(mapping) last_files = ({});
            mapping(mapping:string) cids = ([]);

            // pass 1: Group commits..
            array(string) all_commits = ({});
            mapping oc;
            string x;
            foreach( foo, [array files,mapping commit] )
            {
                if( !sizeof( files ) )
                    commit->is_git = true;
                else
                    commit->is_git = has_prefix( get_cvsroot( files[0]->module )||":git:", ":git:" );
	      if(commit->login != old_user || abs(old_time-commit->time) > 120 )
	      {
// 		 old_revision = commit->revision;
                     old_time = commit->time;
                     old_user = commit->login;
                    if( sizeof( last_files ) )
                    {
                        string x = make_ci_hash( last_files );
                        cids[oc] = x;
                        all_commits += ({ x });
                        last_files = ({});
                    }
                    oc = commit;
                }
                last_files += files;
            }
            if( sizeof( last_files ) )
            {
                string x = make_ci_hash( last_files );
                cids[oc] = x;
                all_commits += ({ x });
            }
            old_user = 0;
            old_time = 0;
            foreach( foo, [array files,mapping commit] )
            {
                if(!sizeof(files))continue;
                string d;
                int total;
		array filterlines = ({});
                sscanf( commit->ended, "%s ", d );
                if( d != day )
                {
                    day = d;
                    add( "<h2><a name='",day,"'>",day_long(commit->time),"</a></h2>" );
                    old_user=0;
                }
                if( arg->showjoin=="yes" )
                    joinargs = generate_join_args( files );
                /*else if( arg->showjoin == "inline" )
                  {
                  joindata = generate_join_data( files );
                  }*/
                if( commit->login != old_user || abs(old_time-commit->time) > 120 )
                {
                    old_time = commit->time;
                    old_user = commit->login;
		    if( cids[commit] )
                    add( "<h4>",
                         "<a href='index.xml?checkins=",cids[commit],"'>",
                         commit->ended, " (",commit->age,")</a> by ",
                         (commit->login == current_user ? "<span class=me>" : ""),
                         personal_link(commit->login,commit->mailhost),
                         HTML(commit->user),
                         "</a>&nbsp;&lt;<a href='mailto:",
                         HTTP(commit->login),"@",HTTP(commit->mailhost),"'>",
                         HTML(commit->login),"@",HTML(commit->mailhost),
                         "&gt;</a>",
                         (commit->login == current_user ? "</span>" : ""),
                         "</h4>\n");
		    //		    else
		    //			werror("WARNING: No cid for commit %O\n", ""/*commit*/);
                }
                add( "<ul>" );
                string mbr;
                foreach( files, mapping ci )
                {
                    total += (int)ci->added + (int)ci->removed;

                    add( "<li><b>" );

                    string showrev="";
                    if( ci->state == "changed" )
                    {
                        string title;
                        if (ci->ancestor)
                            title = sprintf("title='From %s to %s' ", ci->ancestor, ci->revision);
                        else
                            title = sprintf("title='Created %s' ", ci->revision);
                        if( id->variables->viewcvs == "1" )
                            add( "<a ",title,"href='",
                                 viewcvs_link(HTTP(ci->module),HTTP(ci->pathraw),
                                              ci->ancestor,ci->revision),"'>");
                        else {
                            if( !arg->qa )
                                add( "<a ",title,"href='piff.xml?module=",HTTP(ci->module),
                                     "&amp;file=",HTTP(ci->pathraw));
                            else
                                add( "<a ",title,"href='log.xml?module=",HTTP(ci->module),
                                     "&amp;file=",HTTP(ci->pathraw));
                            if (ci->ancestor)
                                add( "&amp;from=",ci->ancestor );
                            add( "&amp;to=",ci->revision,"'>" );
                        }
                        if( arg->showrevs && !commit->is_git ) {
                            if (ci->ancestor)
                                showrev = " "+ci->ancestor+" -&gt; "+ci->revision;
                            else
                                showrev = " NEW ("+ci->revision+")";
                        }
			//FIXME: generalize .git hack below
			add( HTML(has_suffix(ci->module,".git")?ci->module[..<4]:ci->module),"/",HTML(ci->path) );

                        add("</a>" );
                    }
                    else
                    {
                        if( arg->showrevs && !commit->is_git )
                            if( ci->ancestor )
                                showrev = " "+ci->ancestor+" -&gt; DEAD";
                            else
                                showrev = " NEW ("+ci->revision+")";
                        add( HTML(ci->module),"/",HTML(ci->path) );
                    }
                    if( showrev )
		    {
                        add( showrev );
			//FIXME: Generalize hack to extract from Pike
			//       CVS->git coversion notes
			sscanf(commit->message, "%*s\n Rev: "+ci->path+":%s", string cvsrev);
			if( cvsrev )
                        {
			    cvsrev = (cvsrev/"\n")[0] - "<br>";
			    //NOTE: The bug module will catch these if >1000(?)
			    add( sprintf(" (cvsrev: %s)", cvsrev) );
			    filterlines += ({ sprintf("<br>\n Rev: %s:%s", ci->path, cvsrev) });
			}
		    }
                    if( admin_users[ current_user ] && id->prestate->merger )
                    {
                        string identifier = ci->module+"|"+ci->revision+"|"+ci->pathraw;
                        string x = id->raw_url;
                        sscanf( x, "%s&mark_as%*s", x );
                        if( ci->merge == "merge" )
                            add( "<a href='",HTML(x),"&mark_as_not_merge=",
                                 HTML(identifier),"'>[ not move ]</a>");
                        else
                            add( "<a href='",HTML(x),"&mark_as_merge=",
                                 HTML(identifier),"'>[ move ]</a>");
                    }
                    add( "</b> ", ci->status );
                    if( ci->state != "removed" )
                        add( "<a class=dim href='colorize.xml?module=",HTTP(ci->module),
                             "&amp;file=",HTTP(ci->pathraw),
                             "&amp;revision=",ci->revision,"'>(",
                             ci->lines, " lines)</a>\n" );
                    mbr = ci->branch;
                }
		if( arg->showrevs && commit->is_git )
		  add( "<br/>checkin: <b>",commit->revision,"</b>");
                if( strlen( mbr ) && !branch_rev[mbr] )
                    add( "<br/>branch: <b>",mbr,"</b>" );
                if( total > 20 && sizeof(files)>3 )
                    add( "<br/><i class=dim> Total ",(string)total,"</i>" );
                add( "</ul>" );
		string filtered_message;
		filtered_message = commit->message;
		foreach(filterlines, string filterline)
		    filtered_message -= filterline;
                add( "<blockquote>", filtered_message,
                     (joinargs?"<br/><a href=join.xml?"+joinargs+">[ merge this commit ]</a>":""),
                     "</blockquote>" );
                allbugs += commit->bugfixes;
            }

            if( sizeof(all_commits ) )
                add("<p><a href='/index.xml?checkins="+all_commits*","+"'>[permalink]</a></p>");

            TIME(format);
	    string q = res->get();
            return ({([ "alles":q, "allbugs":allbugs ])});
        }
        RXML_CONTEXT->set_misc( "committed_files_off", 0 );
        RXML_CONTEXT->set_misc("committed_files", committed_files );
        if( sizeof( foo ) > 5000 )
        {
            foo = foo[..4999];
            limited = 1;
        }
        if( limited )
            foo[0][1]->limited="1";

        TIME(fin);
        return column(foo,1);
    }

}

mapping uids = ([]);

//! Helper function for parsing of git commit logs.
void parse_git_commitlog(Stdio.File log,
			 function(mapping(string:string|int),
				  mixed ...: void) cb,
			 mixed ... args)
{
  mapping(string:string|int) commit;
  foreach(log->line_iterator();; string line) {
    array(string) cmd = line/" ";
    switch (cmd[0]) {
    case "":
      commit->message = (commit->message || "") + line[4..] + "\n";
      break;
    case "commit":
      if (commit) {
	cb(commit, @args);
      }
      commit = ([]);
      // FALL_THROUGH
    case "parent": case "tree":
      // Note: Only keeps track of the first of each kind.
      commit[cmd[0]] = commit[cmd[0]] || cmd[1];
      break;
    case "author": case "committer":
      // Note: Only keeps track of the first of each kind.
      if (!commit[cmd[0]]) {
	commit[cmd[0]] = cmd[1..sizeof(cmd)-3]*" ";
	commit[cmd[0]+"_ts"] = (int)cmd[-2];
	commit[cmd[0]+"_tz"] = (int)cmd[-1];
      }
      break;
    default:
      commit->other = (commit->other || "") + line + "\n";
      break;
    }
  }
  if (commit) {
    cb(commit, @args);
  }
}

class TagEmitTaglog
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "taglog";
    string get_name( int uid )
    {
        if( uids[uid] )
            return uids[uid];
        array a = getpwuid( uid );
        if( a )
            return uids[uid] = a[0];
        return "uid:"+uid;
    }

    array(mapping) get_dataset( mapping arg, RequestID id )
    {
        string z = arg->tag;

        string today =Roxen.strftime( "%A %d %B %Y", time(1) );
        string yesterday =Roxen.strftime( "%A %d %B %Y", time(1)-3600*24 );
        string day_long( int x )
        {
            string res = Roxen.strftime( "%A %d %B %Y", x );
            if( res == today )
                return "Today; "+res;
            if( res == yesterday )
                return "Yesterday; "+res;
            return res;
        };

        string person = query("person");
        string personal_link(string u, string m)
        {
            return "<a href='"+sprintf(person,u,m)+"'>";
        };
	
        mapping by_hour = ([]);

	Stdio.File dev_null = Stdio.File("/dev/null", "r");

	foreach(debug_query("SELECT cvsroot_path, module FROM cl_repository "
			    " WHERE cvsroot_scheme = ':git:'");;
		mapping(string:string) repository) {
	  string git_dir = repository->cvsroot_path;
	  string module = repository->module;

	  mapping(string:string) files = ([]);
	  Stdio.File pipe = Stdio.File();
	  Process.create_process(({ query("gitbin"),
				    "--git-dir=" + git_dir,
				    "ls-tree", "-r", "--full-tree",
				    z }),
				 ([ "stdout":pipe->pipe(),
				    "stderr":dev_null ]));
	  foreach(pipe->line_iterator();; string line) {
	    int mode;
	    string type, id, path;
	    if (sscanf(line, "%o %s %s\t%s", mode, type, id, path) == 4) {
	      if (type == "blob")
		files[path] = id;
	    }
	  }
	  if (!sizeof(files)) continue;

	  // Scan the commits linearily.
	  Stdio.File log = Stdio.File();
	  Process.create_process(({ query("gitbin"),
				    "--git-dir=" + git_dir,
				    "log", "--first-parent",
				    "--pretty=raw", "--name-only", z }),
				 ([ "stdout":log->pipe(),
				    /*"stderr":dev_null*/ ]));

	  void update_commit(mapping(string:string|int) commit,
			     mapping(string:string) files,
			     string module)
	  {
	    if (!commit->other) return;
	    foreach(commit->other/"\n", string path) {
	      if (!m_delete(files, path)) continue;

	      int timet = commit->committer_ts;
	      string uid = utf8_to_string(commit->author);

	      if( !by_hour[timet/3600] )
		by_hour[timet/3600] = ([]);
	      if( !by_hour[timet/3600][uid] )
		by_hour[timet/3600][uid] = ({});

	      by_hour[timet/3600][uid] += 
		({  ([
		  "op":"tag",
		  "file":module + "/" + utf8_to_string(path),
		  "rev":commit->commit,
		  "uid":uid,
		  "timet":timet
	      ]) });
	    }
	    if (!sizeof(files)) throw(UNDEFINED);	// Terminate.
	  };

	  catch { parse_git_commitlog(log, update_commit, files, module); };
	}
#if 0
        Stdio.File input = Stdio.File( );
        if( !input->open( rtag_log_name( z ),"r" ) )
        {
#endif
            return ({(["alles":sprintf("No support for cvs taglogs.")])});
#if 0
        } 
        else {
        foreach( input->line_iterator(); ; string line )
        {
            string op, file, rev;
            int uid, timet;
            if( sscanf( line, "%s\0%s\0%s\0%d\0%d", op, file, rev, uid, timet ) == 5 )
            {
                if( !by_hour[timet/3600] )
                    by_hour[timet/3600] = ([]);
                if( !by_hour[timet/3600][uid] )
                    by_hour[timet/3600][uid] = ({});

                by_hour[timet/3600][uid] += 
                    ({  ([
                        "op":op,
                        "file":fix_filepath(file),
                        "rev":rev,
                        "uid":get_name(uid),
                        "timet":timet
                    ]) });
            }
        }
	}
#endif

        String.Buffer res = String.Buffer( 1000000 );
        function add = res->add;
        string old_day;
        foreach( reverse(sort( indices( by_hour ) )), int hour )
        {
            foreach( by_hour[hour]; int user; array(mapping) entries ) 
            {
	        int first;
                foreach( entries, mapping entry )
                {
                    if( !first )
                    {
                        first = 1;
                        if( day_long(entry->timet) != old_day )
                        {
                            old_day = day_long(entry->timet);
                            add( "<h2>",old_day,"</h2>" );
                        }
                        mapping l = localtime(entry->timet);
                        string when = sprintf("%4d-%02d-%02d %02d:%02d:%02d", l->year+1900,
                                              l->mon+1, l->mday, l->hour, l->min, l->sec );
                        string age = how_old( time(1)-entry->timet );

			[string login, string real_name, string mailhost] =
			  get_user_info(entry->uid);

			mailhost = mailhost || query("default_mailhost");

                        add( "<h4>",when," (",age,") by ",
                             personal_link(login,mailhost),
                             HTML(real_name),"</a> &lt;<a href='mailto:",
                             HTTP(login),"@",HTTP(mailhost),"'>",
                             HTML(login),"@",HTML(mailhost),
                             "</a>&gt;</h4>\n");
                        add( "<ul>" );
                    }
                    add( "<li><b>" );
                    string module, subpath;
                    sscanf( entry->file, "%[^/]/%s", module, subpath );
                    if( entry->op == "tag" )
                        add( HTML(entry->file)," -&gt; ", entry->rev );
                    else
                        add( HTML(entry->file)," -&gt; ", "DEL" );
                    add("</b></li>");
                } 
                add( "</ul>");
            }
        }
        return ({([ "alles":res->get() ])});
    }
}

class TagEmitJoinCommands
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "join-commands";

    array(mapping) get_dataset( mapping arg, RequestID id )
    {
        string z = arg->v;
        array(array(string)) vars;
        if( catch {
                vars = decode_value(Gz.inflate()->inflate(Gmp.mpz(z,36)->digits(256)));
            } )
            RXML.parse_error( "Expected magic variable passed as argument 'v'." );
        if( arg->backout=="1" )
        {
            vars = reverse(vars);
            foreach( vars, array z )
            {
                string tmp = z[1];
                z[1]=z[2];
                z[2]=tmp;
            }
        }
        array res = ({});
        foreach( vars, array z )
        {
            string cmd = "cvs ";
            if( !z[2] )
                cmd += "rm -f";
            else if( !z[1] )
                cmd += "up -j "+z[1];
            else
                cmd += "up -j "+z[1]+" -j "+z[2];
            cmd += " "+z[0];
            res += ({ ([ "cmd":cmd ]) });
        }
        return res;
    }
}

class TagEmitCheckedInFiles
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "checked-in-files";

    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        array(array(mapping)) commits = RXML_CONTEXT->misc->committed_files;
        int off = RXML_CONTEXT->misc->committed_files_off;
        if(!commits || sizeof(commits) <= off)
            return ({ ([]) });
        RXML_CONTEXT->set_misc("committed_files_off", off+1);
        return commits[off];
    }
}

string qh(string text)
{
    return replace(text, "<>&\""/"", "&lt; &gt; &amp; &quot;" / " ");
}

string qhw(string text)
{
    return replace(text, "<>& "/"", "&lt; &gt; &amp; &nbsp;" / " ");
}

mapping|int(-1..0) find_file(string path, RequestID id)
{
    string cvsroot, mod, rev, file;
    sscanf(path, "%s/%s", mod, path); // FIXME: needs a tweak if mod contains "/"
    if(id->rest_query && sizeof(id->rest_query))
        rev = id->rest_query;
    if(mod && (cvsroot = get_cvsroot(mod)))
    {
        file = get_file_revision(cvsroot, combine_path(mod, path), rev || "HEAD");
        if(!file || !sizeof(file)) return 0;
        string mime_type = id->conf->type_from_filename(path);
        return Roxen.http_string_answer(file, mime_type);
    }
}

string get_file_revision(string cvsroot, string path, string rev)
{
    Stdio.File stdout = Stdio.File();
    path = string_to_utf8(path);
    array(string) argv = ({ query("cvsbin"), "-d", cvsroot, "-q",
			    "checkout", "-r" + rev, "-p", path });
    if (has_prefix(cvsroot, ":git::")) {
      argv = ({ query("gitbin"), "--git-dir="+cvsroot[6..], "show",
		"--encoding=UTF8",
		rev + ":" + ((path/"/")[1..]*"/") });
    }
    Process.create_process(argv, ([ "stdout":stdout->pipe() ]));
    string data = stdout->read();

    if( has_suffix( path, ".svgz" ) )
        data = gunzip( data );

    if( has_suffix( path, ".po" ) || has_suffix(path,"xml") || has_suffix(path,"html") )
    {
        string charset;
        string probe = data[..1000];
        if( sscanf( probe, "%*sContent-Type:%*scharset=%s\\n", charset ) == 3 ||
            sscanf( probe, "%*s<?xml%*scharset='%s'?>", charset ) == 3 ||
            sscanf( probe, "%*s<?xml%*scharset=\"%s\"?>", charset ) == 3 ||
            sscanf( probe, "%*s\n#%*scharset%*s%[^\n]", charset ) == 4 )
        {
            catch {
                return Locale.Charset.decoder(charset)->feed(data)->drain();
            };
        }
    }

    catch { return utf8_to_string( data ); };

    return data;
}

string get_file_annotate(string cvsroot, string path, string rev)
{
    Stdio.File stdout = Stdio.File();
    array(string) argv = ({ query("cvsbin"), "-d", cvsroot, "-q",
			    "rannotate", "-r" + rev, path });
    if (has_prefix(cvsroot, ":git::")) {
      // Note: We MUST use the -- syntax; otherwise git will
      //       complain if we don't have a checked out tree.
      argv = ({ query("gitbin"), "--git-dir="+cvsroot[6..], "blame",
                "--root",
		"-c", "--encoding=utf-8", "-l", rev, "--", (path/"/")[1..]*"/" });
    }
    Process.create_process(argv, ([ "stdout":stdout->pipe() ]));
    string data=stdout->read();


    if( has_suffix( path, ".po" ) )
    {
        string charset;
        if( sscanf( data[..10000], "%*sContent-Type:%*scharset=%s\\n", charset ) == 3 )
        {
            catch {
                data = Locale.Charset.decoder(charset)->feed(data)->drain();
            };
        }
    }

    return data;
}


string do_read( Stdio.File x  )
{
    return x->read();
}

void do_write( Stdio.File d, string x )
{
    d->write( x );
    d->close();
    destruct( d );
}

string gunzip( string x )
{
    Stdio.File out = Stdio.File();
    {
        Stdio.File in = Stdio.File();
        Process.create_process( ({"gzip", "-d" }), 
                                ([ "stdin":in->pipe(),
                                   "stdout":out->pipe() 
                                ]) );
        thread_create( do_write, in, x );
    }
    return do_read(out);
}

string is_bugid( string x, string|void prev_word )
{
    foreach( my_configuration()->get_providers( "bugs" ), object m )
      if( string res = m->is_bug( x, prev_word ) )
            return res;
    return 0;
}

mapping get_bug( string id )
{
    foreach( my_configuration()->get_providers( "bugs" ), object m )
        if( m->is_bug( id ) )
            return m->get_bug( id );
    return 0;
}

string encode_colors(string file, string filename, RequestID id)
{
    if(has_suffix(filename, ".svgz") )
        return file = gunzip(file);
    mixed err = catch {
            string header = lower_case(file[..160]);
            filename = lower_case( filename );
            string ext = (filename/".")[-1];
            foreach( my_configuration()->get_providers( "syntax-highlight" ), object m )
            {
                if( m->can_highlight( ext, filename, header ) )
                {
                    return m->highlight( file );
                }
            }
        };
    if( err ) 
        werror(describe_backtrace(err));
}

#define font(X) ("<span id=\""+X+"\">"),"</span>"
string decode_colors(string file)
{
    return replace(file,
                   ({ "\0"+1,"\1"+1,  "\0"+2, "\1"+2,
                      "\0"+3,"\1"+3,  "\0"+4, "\1"+4,
                      "\0"+5,"\1"+5,  "\0"+6, "\1"+6,
                      "\0"+7,"\1"+7
                   }),
                   ({ font("type"), font("brace"),   font("command"),
                      font("modifier"), font("cpp"), font("comment"),
                      font("string"),
                   }));
}
#undef font


class TagCvsViewErrors
{
    inherit RXML.Tag;
    constant name = "cvsview-errors";
    class Frame
    {
        inherit RXML.Frame;
        array do_return(RequestID id)
        {
            if( id->misc->_cvsview_error )
                return ({ replace(Roxen.html_encode_string(id->misc->_cvsview_error),"\n", "<br>\n")});
            return ({""});
        }
    }
}

class TagCLModuleWork
{
    inherit RXML.Tag;
    constant name = "if";
    constant plugin_name = "cl-module-work";

    string last_user;
    int last_days;
    mapping last_checkins;

    int eval( string module, RequestID id, mapping m )
    {
        string current_user = id->misc->authenticated_user ?
            id->misc->authenticated_user->name() : "anonymous" ;
        int days = ((int)m->days) || 7;

        if( current_user == last_user && m->days == last_days )
            return last_checkins[module];

        last_days = days;
        last_user = current_user;
        int bound = time(1)-86400*days + localtime(time(1))->timezone;
        array res = debug_query( "SELECT HIGH_PRIORITY cl_repository.module AS module "
                                 "FROM cl_revision "
                                 "LEFT JOIN cl_repository ON repository_id=cl_repository.id "
                                 "WHERE user_id="+md5_56bit( last_user ) +
                                 " AND at >= '"+time_to_string( bound )+"'");
        last_checkins = mkmapping( res->module, allocate( sizeof( res ), 1 ) );
        return last_checkins[module];
    }
}

class TagSyntaxHighlight
{
    inherit RXML.Tag;
    constant name="syntax-highlight";

    class Frame
    {
        inherit RXML.Frame;

        array do_return(RequestID id)
        {
            string mod = args->module, rev = args->revision, file;

            result = sprintf("Invalid file (%O,%O,%O).", mod, file, rev);
            if( rev == "" || lower_case(rev) == "head" ) rev = 0;
            string cvsroot = get_cvsroot( mod );

            if(!cvsroot)    // certifies that remote user did not supply any
                return 0; // mischievous data (which would not be in our db)

            int do_colorize = args->highlight && sizeof(args->highlight);
            string path = combine_path(mod, args->file);
            string colorful;

            if( is_image( path ) )
            {
                result="";
                return ({ do_image( cvsroot,path,rev, 0, id) });
            }
            file = get_file_revision( cvsroot, path, rev || "HEAD" );

            if( has_suffix( args->file, "html" ) || has_suffix( args->file, "htm" ))
            {
                result="";
                return ({ "<iframe width='100%' height='100%' src='" +
			  query("location") +
                          Roxen.http_encode_invalids(path) + "?" +
			  Roxen.http_encode_invalids(rev) + "'/>" });
            }

            if(do_colorize)
                colorful = encode_colors(file, args->file, id);

            int tab_width_guesstimate = 8;

            sscanf( file, "%*stab-width: %d", tab_width_guesstimate );

            if(colorful)
                file = decode_colors(qh(expand_tabs(colorful,tab_width_guesstimate)));
            else
                file = qh(expand_tabs(file,tab_width_guesstimate));

            result = "";

            array rows = file / "\n";
            result += "<table cellspacing='0' cellpadding='0'>\n";
            int ln = 1;
            result += "<tr><td valign=top><table cellspacing='0'>"
                "<col class='low' width='100'/><col class='border' width='10'/>\n";
            foreach( rows, string line )
            {
                if( (ln&1) )
                    result += "<tr><td><pre>"+ln+"</pre></td><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
                else
                    result += "<tr><td id=evenlow><pre>"+ln+"</pre></td>"
                        "<td id=evenborder>&nbsp;</td><td id=even>&nbsp;</td></tr>\n";
                ln++;
            }
            result += "</table>";
            result += "</td>";
            result += "<td valign=top><table cellspacing='0'><col class='high'/>";
            ln = 1;
            string in_tag = "";
            foreach( rows, string line )
            {
                string next_in_tag;
                if( ln&1 )
                    result += "<tr>";
                else
                    result += "<tr id=even>";

                if( String.count( line, "<span") > String.count( line, "</span>") )
                    next_in_tag = "<span"+(((line/"<span")[-1]/">")[0])+">";
                else if( String.count( line, "</span") > String.count( line, "<span") )
                    next_in_tag = "";

                if( in_tag )
                {
                    array a = line/"<span";
                    if( sizeof( a ) > 1 )
                        line = in_tag+a[0]+"</span><span"+a[1..]*"<span";
                    else
                        line = in_tag+line+"</span>";
                }
                result += "<td><a name='l"+ln+"'/><pre>"+line+" </pre></td></tr>";
                if( next_in_tag )
                    in_tag = next_in_tag;
                ln++;
            }
            result += "</table>";
            result += "</td>";
            result += "</tr>";
            result += "</table>";
            if(!has_suffix(file, "\n"))
                result += "<i>(No newline at end of file)</i><br />\n";

            if( id->misc->bugs )
            {
                result += "</tt><h2>Bugs mentioned</h2>";
                result += "<table><emit source=bugs modules='"+((array(string))id->misc->modules)*","+"' bugs='"+((array(string))id->misc->bugs)*","+"'>\n"
                    "<tr><td> &nbsp; </td><td><a href=\"&_.url;\">&_.no;</a></td><td>&_.status:none;</td><td>&_.module;</td><td>&_.summary:html;</td></tr>\n"
                    "</emit></table><tt>";
            }

            return 0;
        }
    }
}

string line_expand_tabs(string line, int|void tab_w,
                        string|void sp, string|void tb)
{
    string ws, chunk, result = "",space = sp || " ", tab = tb || " ";
    if(!has_value(line,"\t"))
    {
        if( sp != " " )
	    return replace(line," ",space);
	return line;
    }

    int col, next_tab_stop, i, tab_width = tab_w || 8;
    while(sizeof(line))
    {
        sscanf(line, "%[ \t]%[^ \t]%s", ws, chunk, line);
        for(i=0; i<sizeof(ws); i++)
            switch(ws[i])
            {
                case '\t':
                    next_tab_stop = col + tab_width - (col % tab_width);
                    result += tab * (next_tab_stop - col);
                    col = next_tab_stop;
                    break;

                case ' ':
                    result += space;
                    col++;
            }
        result += chunk;
        col += sizeof(chunk);
    }
    return result;
}

string expand_tabs(string s, int|void tab_width,
                   string|void substitute_tab,
                   string|void substitute_space,
                   string|void substitute_newline)
{
    string tab = substitute_tab || " ",
        space = substitute_space || " ",
        newline = substitute_newline || "\n";
    return map(s/"\n", line_expand_tabs, tab_width, space, tab) * newline;
}

class TagEmitDiffLines
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "diff-lines";

    array(mapping) get_dataset(mapping arg, RequestID id)
    {
        array(array(mapping)) lines = RXML_CONTEXT->misc->diff_lines;
        if(!lines || !sizeof(lines))
            return ({ ([]) });
        RXML_CONTEXT->set_misc("diff_lines", lines[1..]);
        return lines[0]; // formatting already done
    }
}

string Attic( string x )
{
    if( has_value( x, "Attic" ) )
        return ((x-"Attic/")-"/Attic")-"Attic";
    array z = x/"/";
    z = z[..sizeof(z)-2] + ({"Attic"})+z[sizeof(z)-1..];
    return z * "/";
}

mapping cvsroots = ([]);
string get_cvsroot( string module )
{
    if( cvsroots[module] )
        return cvsroots[module];
    
    array tmp=debug_query("SELECT cvsroot_scheme,cvsroot_host,cvsroot_path FROM cl_repository WHERE module=%s", module );
    if( !sizeof( tmp ) ) return 0;
    if (tmp[0]->cvsroot_scheme != ":local:")
	return cvsroots[module] = tmp[0]->cvsroot_scheme +
	  (tmp[0]->cvsroot_host||"") + ":" + tmp[0]->cvsroot_path;
    return cvsroots[module] = tmp[0]->cvsroot_path;
}

#define STEP 30
Image.Image alpha_sq;
Image.Image get_alpha_squares() 
{
    if( alpha_sq )
        return alpha_sq;
    alpha_sq = Image.Image( STEP*2, STEP*2, 120,120,120 );
    alpha_sq->setcolor( 160,160,160 );
    alpha_sq->box( 0, 0, STEP-1, STEP-1 );
    alpha_sq->box( STEP, STEP, STEP+STEP-1, STEP+STEP-1 );
    return alpha_sq;
}

Image.Image do_tile_paste( Image.Image i, Image.Image a )
{
    if( !a ) return i;
    Image.Image bi = get_alpha_squares();  
    object q = i->copy();
    for( int y = 0; y<i->ysize(); y+=STEP*2 )
        for( int x = 0; x<i->xsize(); x+=STEP*2 )
            q->paste( bi, x, y );
    return q->paste_mask( i, a );
}

Image.Image write_text( string font_data )
{
    Stdio.write_file( "/tmp/cvsview-tmp-font.ttf", font_data );
    object x = Image.TTF( "/tmp/cvsview-tmp-font.ttf" )();
    rm( "/tmp/cvsview-tmp-font.ttf" );
    x->set_height(64);
    return x->write( "The quick brown fox jumped",
                     "over the lazy dog.",
                     "1234567890, !?$# ��� ���", /// iso 8859-1
                     "ext: \x1a8\x01c8\x390\x398\x3bc\x40d\x40c\x47e", // extended latin-B, greek, cyrillic
                     "misc: \x3468\x343f\x3403 \xfb73\xfb7f\xfbaf\xfbef",
                     "japanese: \x30b0\x30b1\x30b2\x30b3\x30b4\x30b5\x3075\x3076\x3077\x3078" // japanese
                   );
}

mapping decode_image( string p, string data )
{
    if (has_prefix(data, "<metadata>")) {
	// SiteBuilder metadata block.
	sscanf(data, "<metadata>%*s</metadata>\n%s", data);
    }
    if( has_suffix(p,".ttf") )
    {
        object r = write_text( data );
        return ([ "alpha":r, "img":r->invert(),"image":r->copy()->clear(Image.Color.black) ]);
    }
    return Image._decode( data );
}

Image.Image generate_image( mapping args )
{
    string file1 = get_file_revision(args->cvsroot, args->path, args->r1), file2;
    mixed i1 = decode_image( args->path, file1 ), i2;

    if( !i1 || !i1->image )
        i1=Image.Image(1,1);
    if( args->nochecks )
        return i1;
    if( args->r2 )
        file2 = get_file_revision(args->cvsroot, args->path, args->r2);
    if( file2 )
        i2 = decode_image( args->path, file2 );

    i1 = do_tile_paste( i1->image, i1->alpha );

    if( i2 && i2->image )
        i2 = do_tile_paste( i2->image, i2->alpha );
    else
        return i1;
    i1 = i1->copy( 0,0,i2->xsize()-1, i2->ysize()-1 );
    Image.Image added   = (i2-i1);
    Image.Image removed = (i1-i2);
    Image.Image alpha   = (added->grey()+removed->grey());
    return do_tile_paste( i2, alpha );
}

string image_diff( array a1, array a2, RequestID id )
{
    string img1_url = query_internal_location() + 
        the_cache->store( ([ "cvsroot":a1[0], "format":"png", "path":a1[1], "r1":a1[2] ]), id );
    string img2_url = query_internal_location() + 
        the_cache->store( ([ "cvsroot":a1[0], "format":"png", "path":a1[1], "r1":a2[2] ]), id );
    string img3_url = query_internal_location() + 
        the_cache->store( ([ "cvsroot":a1[0], "format":"png", "path":a1[1], "r1":a1[2], "r2":a2[2] ]), id );
    return 
        "<table><tr><td>Before:</td><td> <img src='"+img1_url+"'></td></tr>"
        "<tr><td>Diff:</td><td><img src='"+img3_url+"'></td></tr>"
        "<tr><td>After:</td><td><img src='"+img2_url+"'></td></tr></table>";
}

class TagEmitAnnotate
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "annotate";
   

    string today;
    string normalize_date( string x )
    {
      if( !has_prefix(x,today))
        return (x/" ")[0];

        catch {
            Calendar.Second s = 
                Calendar.dwim_time(x)->set_timezone("UTC");
            if( s->day() == Calendar.now()->day() )
                return s->format_time();
            return s->format_ymd();
        };
        return x;
    }

    array(mapping) get_dataset(mapping args, RequestID id)
    {
      System.Timer et = System.Timer();
      today = Calendar.now()->day()->format_ymd();
        string mod = args->module, r1 = args->r;
        string cvsroot = get_cvsroot(mod);
        string path = combine_path(mod, args->file);
	string tt="";
        if(!cvsroot) return ({});
        if( is_image(path) )
            return ({});

        string annotate = get_file_annotate( cvsroot, path, r1 );

	// tt += "git: "+et->get()+"<br>";

        if( !annotate || !sizeof( annotate ) )
            return ({});
        array res = ({});
        string code = "";
	if (has_prefix(cvsroot, ":git::")) 
        {
	  foreach( annotate / "\n"-({""}), string q )
	  {
	    string revision, login, date, line;
            if( q[0] == '^' )
                q = q[1..];
            sscanf( q, "%[0-9a-fA-F]%*[ \t](%s\t%s\t%*d)%s",
                    revision, login, date, line );
            if( !date )
                date = "";
	    res += ({([ "date":normalize_date(date),"rev":revision,"srev":revision[..5], "login":login ])});
	    code += line+"\n";
	  }
	}
	else
        {
	  foreach( annotate / "\n"-({""}), string q )
	  {
	    string revision, login, date, line;
	    sscanf( q, "%[0-9.]%*[ \t](%[^ ] %s): %s", 
		    revision, login, date, line );
	    res += ({([ "date":normalize_date(date),"rev":revision,"srev":revision[..5], "login":login ])});
	    code += line+"\n";
	  }
	}


        string colorful;
        if( args->colorize )
            colorful = encode_colors(code, path, id);

	
        int tab_width_guesstimate = 8;
        sscanf( code, "%*stab-width: %d", tab_width_guesstimate );

        if(colorful)
            code = decode_colors(qh(expand_tabs(colorful, tab_width_guesstimate)));
        else
            code = qh(expand_tabs(code,tab_width_guesstimate));
        array q = code/"\n";
        for( int i = 0; i<sizeof(res); i++ )
            if( sizeof( q ) > i)
                res[i]->lines = q[i]+"\n";
            else
                res[i]->lines="";
        string old_rev;
        int old_i;

        for( int i = 0; i<sizeof(res); i++ )
        {
            if( res[i]->rev == old_rev )
            {
                res[old_i]->lines += res[i]->lines;
                res[i] = 0;
            }
            else
            {
                old_rev = res[i]->rev;
                old_i = i;
            }
        }
        res = res-({0});
	// tt += "group: "+et->get()+"<br>";

        int next_color;
        mapping colors = ([]);
        string get_color( string rev, float sat, float bright )
        {
            if( !colors[rev] ) 
                colors[rev] = ++next_color;
            return Image.Color( @Colors.hsv_to_rgb( (colors[rev]*21)%255, 
                                                    (rev?(int)(255-sat*80):0), 
                                                    (int)(255-bright*50) ) )
                ->html();
        };
        for(int i = 0; i<sizeof(res); i++ )
        {
            res[i]->revcolor = get_color( res[i]->login, 0.8, (i&1)/2.0 );
            res[i]->revcolor2 = get_color( 0, 0, (i & 1)/2.0+0.5 );
            res[i]->revcolor3 = get_color( 0, 0, (i & 1)/2.0 );
            res[i]->revcolor4 = get_color( res[i]->login, 2.0, (i & 1)/2.0 );
            res[i]->odd = (string)(i&1);
        }

	if( args->standard )
	{
	  String.Buffer out = String.Buffer();
	  function add = out->add;
	  string linkbase = "<a href='log.xml?module="+
	    Roxen.html_encode_string(mod)+ 
	    "&amp;file="+ 
	    Roxen.html_encode_string(args->file)+
	    "&amp;rev=";

	  foreach( res, mapping row )
	  {
	    add("<tr bgcolor='",row->revcolor2,"' valign=top><td >",
		linkbase,row->rev,"'>", row->srev,"</td><td>"
		  "<nobr>",row->date,"</nobr></a></td><td bgcolor='",
		  row->revcolor4,"'><nobr>", Roxen.html_encode_string(row->login), 
		  "</nobr></td>"
		  "<td bgcolor='",row->revcolor,"'>&nbsp;</td><td style='background:",
		  row->revcolor3,";;white-space: pre;'><tt>",row->lines,"</tt></td></tr>\n");
	  }
	  return ({ ([ "alles":out->get()/*+"\n<p>"+tt*/ ]) });
	}

        return res;
    }
}

class TagEmitPikeDiff
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "pike-diff";

    string parse_preproc( string x )
    {
        if( has_prefix( x, "def" ) )
            return "#if defined("+String.trim_whites(x[4..])+")";
        else if( has_prefix( x, "ndef" ) )
            return "#if undefined("+String.trim_whites(x[5..])+")";
        else
            return "#if "+x;
    }

    array(mapping) get_dataset(mapping args, RequestID id)
    {
        string mod = args->module, r1 = args->from, r2 = args->to;
        string cvsroot = get_cvsroot(mod );
        string path = combine_path(mod, args->file);
        if(!cvsroot) return ({});

        if( is_image( path ) )
        {
            return ({ ([ "alles":image_diff( ({cvsroot,path,r1}), ({cvsroot,path,r2}), id  ) ]) });
        }

        string file1 = get_file_revision(cvsroot, path, r1);
        string file2 = get_file_revision(cvsroot, path, r2);

        file1 -= "\r"; file2 -= "\r";

        int nl_at_eof1 = has_suffix(file1, "\n"), 
            nl_at_eof2 = has_suffix(file2, "\n");
        array d1, d2;
	array f1 = file1[..sizeof(file1)-1-nl_at_eof1]/"\n";
        array f2 = file2[..sizeof(file2)-1-nl_at_eof2]/"\n";
        array(string) nowhites( array(string) x )
        {
            if( args->nowhites ) 
                return String.trim_all_whites( x[*] );
            return x;
        };

        [d1, d2] = Array.diff(nowhites(f1), nowhites(f2));


        if( args->nowhites ) // Re-apply whites.
        {
            int ln;
            foreach( d1, array lines )
            {
                for( int i=0; i<sizeof(lines); i++ )
                    lines[i] = f1[ln++];
            }
	    // Note: Any lines that are equal disregarding whitespace
	    //       will get the whitespace from file2's line.
            ln = 0;
            foreach( d2, array lines )
            {
                for( int i=0; i<sizeof(lines); i++ )
                    lines[i] = f2[ln++];
            }
        }

        array diff_chunks = ({}), diff_lines = ({});
        array functions_n_defs = allocate(sizeof(f1));
    
        int is_java = has_suffix( args->file, ".java" );
        int is_cish = has_suffix( args->file, ".c" )||has_suffix( args->file, ".cpp" )||has_suffix( args->file, ".pike" )||has_suffix( args->file, ".pmod" )||has_suffix( args->file, ".h" );

        string cls;
        string func;
        array(string) defs = ({});
        string ifdef, endif, funcdef, classdef;

        classdef = "%*[ \t]class%*[ \t]%[^ \t]";
        funcdef = "%*[ \t]%[^ \t]%*[ \t]%[^ \t](%s)%*[^;]";

        if( is_java )
        {
            ifdef = "%*[ \t]//#%*[ \t]if%s";
            endif = "%*[ \t]//#%*[ \t]endif%*s";
        }
        else if( is_cish )
        {
            ifdef = "%*[ \t]#%*[ \t]if%s";
            endif = "%*[ \t]#%*[ \t]endif%*s";
        }
        if( ifdef && endif )
            foreach( f1; int l; string line )
            {
                string tmp;
                if( sscanf( line, ifdef, tmp ) && tmp )
                {
                    defs += ({ tmp });
                }
                else if( sscanf( line, endif ) > 1 )
                {
                    defs = defs[..sizeof(defs)-2];
                }
                functions_n_defs[l] = ({ (func?func:"") + (cls?" class "+cls:""), defs});
            }


        int fat, tat, ctx = ((int)args->context || 3), diff_chunk;
        if(sizeof(d1) && (d1[diff_chunk] == d2[diff_chunk]))
        {
            fat += sizeof(d1[diff_chunk]);
            tat += sizeof(d2[diff_chunk]);
            diff_chunk++;
        }
        while(diff_chunk < sizeof(d1))
        {
            diff_chunks += ({ ([ "from-line":max(fat+1-ctx, 1),
				 "to-line":max(tat+1-ctx, 1) ]) });
            if( array fd = functions_n_defs[min(max(fat+1-ctx,0),sizeof(functions_n_defs)-1)] )
            {
                diff_chunks[-1]["function"] = fd[0];
                if( sizeof( fd[1] ) )
                {
                    diff_chunks[-1]["ifdefs"] = "Inside "+String.implode_nicely(map(fd[1],parse_preproc));
                }
            }
            diff_lines += ({ ({}) });

            diff_lines[-1] += dlize("context", f1[fat-ctx..fat-1], fat-ctx, tat-ctx);
            if( sizeof( d1[diff_chunk] ) )
                diff_lines[-1] += dlize("removed", d1[diff_chunk], fat, UNDEFINED, d2[diff_chunk]);
            diff_lines[-1] += dlize("added",   d2[diff_chunk], UNDEFINED, tat, d1[diff_chunk]);
            fat += sizeof(d1[diff_chunk]);
            tat += sizeof(d2[diff_chunk]);
            while(++diff_chunk < sizeof(d1) && sizeof(d1[diff_chunk]) <= ctx * 2)
            {
                diff_lines[-1] += dlize("context", d1[diff_chunk], fat, tat);
                fat += sizeof(d1[diff_chunk]);
                tat += sizeof(d2[diff_chunk]);
                if(++diff_chunk >= sizeof(d1))
                {
                    diff_chunks[-1]["from-lines"] = fat - diff_chunks[-1]["from-line"];
                    diff_chunks[-1]["to-lines"]   = tat - diff_chunks[-1]["to-line"];
                    break;
                }
                diff_lines[-1] += dlize("removed", d1[diff_chunk], fat, UNDEFINED, d2[diff_chunk]);
                diff_lines[-1] += dlize("added",   d2[diff_chunk], UNDEFINED, tat, d1[diff_chunk]);
                fat += sizeof(d1[diff_chunk]);
                tat += sizeof(d2[diff_chunk]);
            }
            if(diff_chunk < sizeof(d1))
            {
                diff_lines[-1] += dlize("context", f1[fat..fat+ctx-1], fat, tat);
                fat += sizeof(d1[diff_chunk]);
                tat += sizeof(d2[diff_chunk]);
                diff_chunks[-1]["from-lines"] = fat - diff_chunks[-1]["from-line"];
                diff_chunks[-1]["to-lines"]   = tat - diff_chunks[-1]["to-line"];
                diff_chunk++;
            }
        }
        if(!sizeof(diff_chunks)) // files only differed in eol-at-eof style
        {
            int line_no = sizeof(d1[0]) - 1;
            diff_chunks += ({ ([ "from-line":line_no, "to-line":line_no ]) });
            diff_lines += ({ ({}) });
        }
        if(nl_at_eof1 != nl_at_eof2)
            diff_lines[-1] += dlize("info", ({ nl_at_eof1 ?
                                               "Newline at end of file removed." :
                                               "Newline at end of file added." }));
        RXML_CONTEXT->set_misc("diff_lines", diff_lines);
        return diff_chunks;
    }

    constant split_character = ([
        '#':1,'_':1,' ':1,'\t':1,'\n':1,'\r':1,'-':1,',':1,'.':1,'"':1,
        '\'':1,'<':1,'>':1,'(':1,')':1,'[':1,']':1,0xa0:1,':':1,'!':1,'?':1,
        '*':1,'%':1,'&':1,'/':1,'=':1,'~':1,'^':1,';':1,'|':1,
    ]);
    array(string) my_split( string x )
    {
        array res = ({});
        int j;
        for( int i=0; i<sizeof(x); i++ )
        {
            if( split_character[x[i]] ) {
	        res += ({ x[j..i-1] });
		for(;(i < sizeof(x)) && split_character[x[i]]; i++) {
		    res += ({ x[i..i] });
		}
		j = i;
            }
        }
        if( j < strlen(x) )
            res += ({x[j..]});
        return res;
    }

    array find_best_match( array in, string f, mapping matching )
    {
        if( matching[f] ) 
            return Array.diff( f/"", matching[f]/"" );
        int best = strlen(f);
        array res = ({ ({ f/"" }), ({ ({}) }) });
        foreach( in-({""}), string q )
        {
            if( !matching[q] )
            {
                int dlen;
                [array d1, array d2] = Array.diff( f/"", q/"" );
                for( int i=0; i<sizeof( d1 ); i++ )
                {
                    if( !equal( d1[i], d2[i] ) )
                    {
                        dlen += sizeof(d1[i]) + sizeof(d2[i]);
                    }
                }
                if( dlen < best )
                {
                    best = dlen;
                    matching[q]=f;
                    res = ({ d1, d2 });
                }
            }
        }
        return res;
    }

    array(mapping) dlize(string type, array(string) lines,
                         int|void fat, int|void tat, void|array(string) other )
    {
        int row;
        if( other )
            other=map( other, line_expand_tabs );
        array(mapping) result = allocate(sizeof(lines), ([ "type":type ]));
        int q = 0;
        if( other )
        {
            string res = "";
            string line = map(lines,line_expand_tabs)*"\n";
            string oth = other*"\n";
            array d1, d2;
            [d1,d2]=Array.diff( my_split(line), my_split(oth) );
		
            if( sizeof( d1 ) < 2 && (sizeof( line ) > 4 || sizeof(oth)>4))
            {
                res += Roxen.html_encode_string( line );
            }
            else for( int i = 0; i<sizeof( d1 ); i++ )
                 {
                     if( equal( d1[i], d2[i] ) )
                         res += Roxen.html_encode_string( d1[i]*"" );
                     else if( sizeof( d1[i] ) /*&& sizeof(String.trim_all_whites(d2[i]*""))*/ )
                     {
                         string x = Roxen.html_encode_string( d1[i]*"" );
                         string p;
                         while( sscanf( x, "%s\n%s", p, x ) == 2)
                         {
                             string w="";
                             sscanf( p, "%[ \xa0\t]%s", w, p );
                             res += w+"<span class='change_"+type+"'>"+p+"</span>\n";
                         }
                         string w="";
                         sscanf( x, "%[ \xa0\t]%s", w, p );
                         res += w+"<span class='change_"+type+"'>"+p+"</span>";
                     }
                     else
                         res += Roxen.html_encode_string( d1[i]*"" );
                 }
            return map( res/"\n", lambda( string x ) { return (["type":type, "line":x ]); });
        }
        for( int i = 0; i<sizeof(lines); i++ )
        {
            string line = line_expand_tabs(lines[i]);
            string res = Roxen.html_encode_string(line);
            result[row]->line = res;
            row++;
        }
        return result;
    }
}

//! Used mostly for hashing data names to persistent unique id:s for various
//! cl_* tables - the idea is to get a unique id that will regenerate itself
//! even in the event of a restart from scratch, one or more tables wiped.
int md5_56bit(string data)
{
#if constant(Crypto.md5)
    // Compat with Pike 7.4 (Roxen 4.5 and earlier).
    return data && array_sscanf(Crypto.md5()->update( data )->digest(), "%7c")[0];
#else
    return data && array_sscanf(Crypto.MD5()->update( data )->digest(), "%7c")[0];
#endif
}

string time_to_string(int time_t)
{
    return Calendar.ISO.Second(time_t)->format_time();
}

class TagCommitGraph
{
    inherit RXML.Tag;
    constant name = "commit-graph";
    RXML.TagSet internal = RXML.TagSet(this_module(), "commit-graph", ({ TagBar() }));

    class TagBar
    {
        inherit RXML.Tag;
        constant name = "bar";

        class Frame
        {
            inherit RXML.Frame;

            array do_return(RequestID id)
            {
                if(content && (m_delete(args, "normalise-ws") || m_delete(args, "normalize-ws")))
                {
                    content = replace(content, "\t\n\r" / 1, "   " / 1);
                    content = (content / " " - ({ "" })) * " ";
                }

                args->title = args->title || content;
                args->alt   = args->alt   || content;
                args->total = (string)((int)args->added + (int)args->removed);
                array(mapping) bars = RXML_CONTEXT->misc->commits_per_day || ({});
                RXML_CONTEXT->set_misc("commits_per_day", bars + ({ args }));
            }
        }
    }

    array(string) trim_titles( array(string) titles )
    {
        string current_year = (string)(localtime(time())->year+1900), current_month;
        string actual;
        int step = max((sizeof(titles) / 5), 1 );
        int adjust;
        array res = allocate( sizeof( titles ), "VOID" );
        for( int i = 0; i<sizeof( titles ); i++ )
        {
            actual="";
            string year, month, day;
            if( String.count( titles[i], ":") > 1 )
            {
                sscanf( titles[i]||"", "%s:%s:%s", year, month, day );
                if( year != current_year ) 
                {
                    adjust=i;
                    actual = year+"-";
                    current_year = year;
                }
                if( (step < 30) && (month != current_month) )
                {
                    adjust=i;
                    current_month=month;
                }
                actual += month+" "+day;
                if( ((i-adjust) % step)==0 )
                    res[i] = actual;
            }
            else if( titles[i] != titles[i-1] || !i)
                res[i] = titles[i]||"";
        }
        return res;
    }

    class Frame
    {
        inherit RXML.Frame;
        RXML.TagSet additional_tags = internal;
        constant title = "Lines added/removed recently";
        constant default_diagram_args =
        ([ "type":"sumbars", "font":"franklin gothic demi", "fontsize":"12",
           "bgcolor":"#dddddd", "textcolor":"#000", "gridcolor":"#BBBBBB",
           "vertgrid":"vertgrid", "horgrid":"horgrid", "linewidth":"2.2",
           "neng":"neng", "height":"220", "width":"220", "border":"0",
           "alt":title, "title":title ]);
	
        int max_value;
        void adjust_scale( array x )
        {
            if( !sizeof( x ) )
                return;
            x = (array(int))x;
            int max, min=0x7ffffff, avg;
            foreach( x, int q )
            {
                avg += q;
                if( q < min ) min = q;
                if( q > max ) max = q;
            }
            avg /= sizeof( x );
            if(  (max-avg)/3 > (avg-min) )
            {
                for( int i = predef::max(10,min); i<max; i=i*5 )
                {
                    if( sizeof( filter( x, `>, i ) )*20 < sizeof( x ) )
                    {
                        if( (i*2) < max )
                            max_value = predef::max( max_value, i );
                        return;
                    }
                }
            }
        }

        mapping get_color_from_style( string style )
        {
            switch( style )
            {
                case "style/orange.css":
                    return ([ "bgcolor":"#ffaa00",
                              "linewidth":"2.0",
                              "gridcolor":"#ffff00",
                              "colors":"#ee0000\t#000000"
                           ]);
                default: return ([]);
            }
        }

        array do_return(RequestID id)
        {
            if( args->style )
            {
                args |= get_color_from_style( args->style );
            }

            string imagemap_name = args->usemap || "#graph",
                href = m_delete(args, "date-href");
            array(mapping) bars = RXML_CONTEXT->misc->commits_per_day || ({});
            if( !sizeof( bars ) )
                bars = ({
                    ([ "name":"No matches", "total":"0", "added":"0", "removed":0])
                });
            string data = ({ trim_titles(bars->name) * "\t", // e g "May 12", "2001: Dec 30"
                             bars->added * "\t", bars->removed * "\t" }) 
                * "\n";

            args = default_diagram_args | ([ "usemap" : imagemap_name ]) | args;
            int hour = (sscanf( bars[0]->name||"", "%*s:%*s:%*s" ) != 3);
            if( sizeof( bars ) > 40 )
            {
                args->type = "lines";
// 		data += "\n"+bars->total * "\t";
            }
            adjust_scale( bars->total );
            if( max_value )
            {
                args->ystop = (string)max_value;
            }
            string contents = sprintf("<colors>"+(args->colors?args->colors:"#103060\t#3040B0")+"</colors>\n"
                                      "<xaxis quantity='Time'/>\n"
                                      "<yaxis quantity='%s'/>\n"
                                      "<legend>Added\tRemoved</legend>"
                                      "<data form='row' xnames xnamesvert>%s</data>",
                                      "Lines/"+(hour?"Hour":"Day"), data);

            RXML.Frame diagram = RXML.make_tag("diagram", args, contents);
            // DEBUG("commit-graph diagram content: %O", contents);
      

            // generate a client-side imagemap for the bars
            int max_x = (int)args->width - 41, max_y = (int)args->height - 3;
            int n = sizeof(bars);
            string areas = "";
            float w = (max_x-30) / (float)(n+0.001);
            foreach(bars, mapping bar)
            {
	        // We loop backwards, since the first bar
	        // is the farthest from the right edge.
	        int x0 = max_x - (int)(n * w)+1;
                n--;
                int x1 = max_x - (int)(n * w);
		if (x1 < x0) x1 = x0;	// Paranoia in case w is less than 1.
                bar->shape = "rect";
                bar->coords= sprintf("%d,%d,%d,%d", x0, 27, x1, max_y);
                foreach("removed,added,name"/",", string not_area_tag_arg)
                    m_delete(bar, not_area_tag_arg);
                areas += Roxen.make_tag("area", bar, 1) + "\n";
            }
            result = sprintf("<map name='%s'>%s</map>", imagemap_name - "#", areas);
            return ({ diagram });
        }
    }
}

mapping ancestor_arg(string ancestor_tag_name, string arg_name)
{
    RXML.Frame frame = RXML.get_context()->frame;
    while(frame = frame->up)
        if(frame->tag->name == ancestor_tag_name)
            return frame->args[arg_name];
}


class TagEmitBugs
{
    inherit RXML.Tag;
    constant name="emit";
    constant plugin_name = "bugs";
    constant result_members =({ "no","summary", "url", "os"});
    array(mapping) get_dataset( mapping args, RequestID id )
    {
        if( !args->bugs )
            return ({});
        if( stringp( args->bugs ) )
            args->bugs/=",";
        if( stringp( args->modules ) )
            args->modules=mkmapping((array)args->bugs, args->modules/",");
        return filter(map( sort(Array.uniq( args->bugs ))-({0}), get_bug ),
                      lambda(mapping x){ return x && x["summary"] ? 1 : 0; });
    }
}

class TagEmitCommitGraph
{
    inherit RXML.Tag;
    constant name = "emit";
    constant plugin_name = "commit-graph";
    constant result_members = "plus,minus,commits,login,authors" / ",";

    string branch_version( string x )
    {
        array q= x/".";
        return q[..sizeof(q)-2]*".";
    }

    int minor_version( string x )
    {
        return (int)(x/".")[-1];
    }

    array(mapping) get_dataset(mapping args, RequestID id)
    {
        NOCACHE();

	args->pathmod_available="CONCAT(cl_repository.module,'/',path)";

	string fetch;
	mapping con = constraints( args,id );
	int hours;
	string unit = "TO_DAYS(at)", divisor="";

        if( abs(con->to-con->from)/3600 < 100 )
        {
            unit = "(TO_DAYS(at)*(24*6)+HOUR(at)*6+MINUTE(at)/10)";
            divisor = "*6";
            hours=1;
        }

        fetch = "SELECT HIGH_PRIORITY at,"+unit+" as unit,SUM(l_added)"+
            divisor+" AS plus, SUM(l_removed)"+divisor+" AS minus,"
            "COUNT(at) AS commits,login,"+
            "COUNT(DISTINCT user_id) AS authors FROM "
            +(con->tables*",")+
            "cl_revision"
            " LEFT JOIN cl_user ON user_id=cl_user.id"
            " LEFT JOIN cl_repository ON repository_id=cl_repository.id"
            " LEFT JOIN cl_checkin_message ON message_id=cl_checkin_message.id "
            " WHERE " +con->limit+" GROUP BY "+unit+" ORDER BY at DESC";

        array(mapping) tmp = debug_query(fetch);

        array(int) x = (array(int))tmp->unit;
	
        int maxt = Array.reduce(max,x);
        int mint = Array.reduce(min,x);

        mapping dd = mkmapping( x, tmp );
        array res = allocate( maxt-mint+1 );
        array bars = allocate( sizeof(res) );


        Calendar.Second at;
        Calendar.TimeRange range = (hours?Calendar.Minute()*10 : Calendar.Day()*((int)divisor[1..]||1) );

        if( sizeof( tmp ) )
        {
            at = Calendar.ISO.parse("%y-%M-%D %h:%m:%s", tmp[-1]->at /*+ " UTC"*/);
            for( int i=0; i<sizeof(res); i++, at+=range )
            {
                int z = i+mint;
                if( dd[z] )
                    res[i] = dd[z];
                else
                    res[i] = ([]);
                res[i]->at = at->format_time();
            }

            for(int i = 0; i<sizeof(res); i++)
            {
                at = Calendar.ISO.parse("%y-%M-%D %h:%m:%s", res[i]->at /*+ " UTC"*/);//->set_timezone("CET");
                bars[i] = mkmapping(result_members, rows(res[i], result_members));

                if( hours )
                    bars[i]->name = sprintf("%02d:00", at->hour_no());
                else
                {
                    if( divisor )
                        bars[i]->name = sprintf("%d:%s:%d", at->year_no(),at->month_shortname(), at->month_day());
                    else
                        bars[i]->name = sprintf("%d:%s:%d %02d:%02d", 
                                                at->year_no(),at->month_shortname(), 
                                                at->month_day(), at->hour_no(), at->minute_no());
                }
                bars[i]->date = at->format_ymd();
            }
            return bars;
        }
        return ({});
    }
}
