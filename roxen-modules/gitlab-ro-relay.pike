#include <module.h>

inherit "roxen-module://relay2";

constant module_name = "Proxies: GitLab read-only issue access";

constant module_doc = "HTTP relay module for creating a read-only "
  "access method to a GitLab issue database.";

class Relay
{
  inherit ::this_program;

  mapping make_headers(RequestID from, int trim)
  {
    mapping res = ::make_headers(from, trim);

    res["PRIVATE-TOKEN"] = query("gitlab_private_token");

    return res;
  }
}

class Relayer
{
  inherit ::this_program;

  int(0..1) relay(RequestID id)
  {
    if (!(< "GET", "HEAD" >)[id->method]) return 0;

    return ::relay(id);
  }
}

void create(Configuration c)
{
  ::create(c);

  killvar("patterns");
  killvar("pre-rxml");
  killvar("post-rxml");
  killvar("additional-headers");

  defvar("gitlab_url", "https://git.lysator.liu.se",
	 "GitLab URL", TYPE_URL|VAR_INITIAL,
	 "The URL for the GitLab server.");
  defvar("project_id", 636, "GitLab project id", TYPE_INT|VAR_INITIAL,
	 "The project identifier at the GitLab server.");
  defvar("gitlab_private_token", "", "GitLab API token",
	 TYPE_STRING|VAR_INITIAL,
	 "<p>GitLab private access token.</p>\n"
	 "\n"
	 "<p>Note that this token must have the <tt>api</tt> "
	 "privilege for this module to be meaningful.</p>\n");
}

void start(int i, Configuration c)
{
  ::start(i, 0);

  if (c) {
    relays = ({
      // Only relay issues for a single project and relay them verbatim.
      Relayer(sprintf("(/api/v4/projects/%d/issues/[0-9]*)",
		      query("project_id")),
	      sprintf("%s\\1", query("gitlab_url")), 1, (< "trimheaders" >)),
    });
  }
}